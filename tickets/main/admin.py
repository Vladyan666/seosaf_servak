# -*- coding: utf-8 -*-
from django.contrib import admin
from main.models import TextPage, Team, Player, Match, Stadium, City, News, Champ, Teamchamp, Group, Fed, Zayavka, Pevent, Event, Place, Scene

# Register your models here.
#общие поля
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#ОБЩИЕ СТРАНИЦЫ

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name',]}),] + page_fields
#
admin.site.register(TextPage,TextPageAdmin)


#Города
class CityAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'image']}),] + page_fields
#
admin.site.register(City,CityAdmin)

#Новости
class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("seo_h1",)}
    fieldsets = [(u'Новость',  {'fields':['name','date','content','source','tags','part']})] + page_fields
#
admin.site.register(News,NewsAdmin)

#ФУТБОЛ

#Стадионы
class StadiumAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'city','traffic', 'image',]}),] + page_fields
    list_display = ('name','city','traffic') + page_list
    search_fields = ['name']
    list_filter = ['city']
    
#
admin.site.register(Stadium,StadiumAdmin)

#Федерации
class FedAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','acronym') + page_list
    fieldsets = [
        (u'Информация о федерации',  {'fields':['name','acronym','image']}),
    ] + page_fields
#
admin.site.register(Fed,FedAdmin)

#Игроки
class PlayerAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [
        (u'Личная информация',  {'fields':['name', 'eng_name', 'fioname', 'birth','image','weight','height']}),
        (u'Профессиональные данные',   {'fields':['number','position','team','country']}),
    ] + page_fields
    list_display = ('name','image','alias') + page_list
    search_fields = ['name']
    list_filter = ['team__name','position','club']
#добавление игрока на странице клуба
class PlayerInline(admin.StackedInline):
    model = Player
    extra = 1
    fieldsets = [
        (u'Личная информация',  {'fields':['name','birth','']}),
        (u'Профессиональные данные',   {'fields':['number','position','team']}),
    ]
#
admin.site.register(Player,PlayerAdmin)

#Группы
class GroupAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name',) + page_list
    fieldsets = [
        (u'Информация о группе',  {'fields':['name','champ','teams','qualifying']}),
    ] + page_fields
    filter_vertical = ('teams',)
#
admin.site.register(Group,GroupAdmin)

#Заявки
class ZayavkaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("champ","team")}
    list_display = ('team', 'champ') + page_list
    fieldsets = [
        (u'Информация о заявке',  {'fields':['champ','team','players',]}),
    ] + page_fields
    search_fields = ['team__name']
    list_filter = ['champ','team']
    filter_vertical = ('players',)
#
admin.site.register(Zayavka,ZayavkaAdmin)

#Чемпионаты
class ChampAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','year') + page_list
    fieldsets = [
        (u'Информация о чемпионате',  {'fields':['name','fed','image','year','teams','web','status']}),
    ] + page_fields
    search_fields = ['name']
    list_filter = ['year']
    filter_vertical = ('teams',)
#
admin.site.register(Champ,ChampAdmin)

#Команды
class TeamAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    #inlines = [PlayerInline] #добавлять игрока на странице клуба
    list_display = ('name','city','country','stadium')
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','shortname','founded','offsite','city','country','stadium','logo','flag','photo','forma','techteam']}),
    ] + page_fields
    list_filter = ['country']
    search_fields = ['name']
#
admin.site.register(Team,TeamAdmin)

class TeamchampAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    #inlines = [PlayerInline] #добавлять игрока на странице клуба
    list_display = ('name','champ','points','win','draw','lose','goals','vgoals','mesto')
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','shortname','stadium','champ','logo','photo','forma','techteam','points','goals','vgoals','win','lose','draw','mesto']}),
    ] + page_fields
    list_filter = ['champ']
    search_fields = ['name']
#
admin.site.register(Teamchamp,TeamchampAdmin)

#Матчи
class MatchAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("datetime","stadium")}
    list_display = ('name','id','parse','score','tour','datetime',"stadium","status") + page_list
    
    def name(self, obj):
        return ("%s - %s" % (obj.command_first, obj.command_second)).upper()
    name.short_description = u'Матч'
    
    fieldsets = [
        (u'Информация о матче',  {'fields':['command_first','command_second','datetime','parse','score','status','stadium','champ','tour']}),
    ] + page_fields
    search_fields = ['command_first__name','command_second__name']
    list_filter = ['champ','stadium','datetime']
    
admin.site.register(Match,MatchAdmin)

#ТЕАТРЫ


#Площадки
class PlaceAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("place",)}
    fieldsets = [(u'Основные',  {'fields':['place', 'city','traffic', 'image',]}),] + page_fields
#
admin.site.register(Place, PlaceAdmin)

#Сцены проведения мероприятий
class SceneAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("scene",)}
    fieldsets = [(u'Информация о сцене',  {'fields':['scene','scene_place',]})] + page_fields    
#
admin.site.register(Scene,SceneAdmin)

#Группа мероприятий 
class PeventAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("pevent",)}
    fieldsets = [(u'Поля мероприятия',  {'fields':['pevent','content','libretto','genre','image','video','bannerimage','afishaimage','popularimage','repertoireimage','shortinfo','fullinfo','compositor','actors','duration','popular','afisha']})] + page_fields
    search_fields = ['pevent']
    list_filter = ['genre','compositor']
#
admin.site.register(Pevent,PeventAdmin)

#Одно событие
class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("event",)}
    fieldsets = [(u'Поля события',  {'fields':['name','event','parse','event_date','event_place','event_scene','event_program','content']})] + page_fields
    list_filter = ['event_scene','event_date']
    search_fields = ['event__pevent']
#
admin.site.register(Event,EventAdmin)
    
#Сцена места проведения
class SceneInline(admin.TabularInline):
    model = Scene
    extra = 3
    list_display = ('scene','scene_place')
    






