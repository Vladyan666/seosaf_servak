# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField
# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True

    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"

    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню" )

    def get_absolute_url(self):
        if self.alias == "index" :
            return reverse('base:index', kwargs={})
        else :
            return reverse('base:textpage', kwargs={'textpage_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Fed(Page):
    class Meta:
        verbose_name = u"Федерация (Футбол)"
        verbose_name_plural = u"Федерации (Футбол)"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название(Футбол)")
    acronym = models.CharField(max_length=10 , verbose_name=u"Аббревиатура(Футбол)")
    image = models.ImageField(verbose_name=u"Лого" , null=True , blank=True)
    
    def get_absolute_url(self):
        return reverse('portal:fed', kwargs={'fed_alias': self.alias})
    
    def __unicode__(self):
        return self.name

#Города        
class City(Page):
    class Meta:
        verbose_name = u"Город (Общая)"
        verbose_name_plural = u"Города (Общая)"

    name = models.CharField( max_length=200 , verbose_name=u"Название города")
    image = models.CharField(max_length=200 , verbose_name=u"Фото" , null=True , blank=True)

    def get_absolute_url(self):
        return reverse('main:city', kwargs={'city_alias': self.alias})

    def __unicode__(self):
        return self.name

#Стадион        
class Stadium(Page):
    class Meta:
        verbose_name = u"Стадион (Футбол)"
        verbose_name_plural = u"Стадионы (Футбол)"

    name = models.CharField( max_length=200 , verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Схема" , null=True , blank=True)
    city = models.ForeignKey(City, verbose_name=u"Город", null=True , blank=True)
    traffic = models.IntegerField( verbose_name=u"Вместимость" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('main:stadium', kwargs={'stadium_alias': self.alias})

    def __unicode__(self):
        return self.name
        
#Места проведения мероприятий
class Place(Page):
    class Meta:
        verbose_name = u"Площадка под мероприятие (Театры)"
        verbose_name_plural = u"Площадки (Театры)"
        
    place = models.CharField(max_length=255 , verbose_name=u"Место проведения (Площадка)")
    city = models.ForeignKey(City, verbose_name=u"Город площадки", null=True , blank=True)
    image = models.ImageField(verbose_name=u"Фото", null=True , blank=True)
    traffic = models.IntegerField( verbose_name=u"Вместимость" , null=True , blank=True )
    
    def get_absolute_url(self):
        return reverse('main:place', kwargs={'place_alias': self.alias})
    def __unicode__(self):
        return self.place
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

#Сцены проведения для площадок (Например у большого театр 3 сцены: историческая, новая сцена и бетховенский зал
class Scene(Page):
    class Meta:
        verbose_name = u"Сцена(Зал) (Театры)"
        verbose_name_plural = u"Сцены(Залы) (Театры)"
    
    scene = models.CharField(max_length=255 , verbose_name=u"Название сцены")
    scene_place = models.ForeignKey(Place, verbose_name=u"Место проведения (Площадка)")
    
    def get_absolute_url(self):
        return reverse('main:scene', kwargs={'scene_alias': self.alias})
    
    def __unicode__(self):
        return self.scene
                
#Постоянные мероприятия (те что содержат в себе список дат) 
class Pevent(Page):
    class Meta:
        verbose_name = u"Мероприятие(без даты) (Театры)"
        verbose_name_plural = u"Мероприятия(без даты) (Театры)"
        
    pevent = models.CharField( max_length=255 , verbose_name=u"Название мероприятия" )
    libretto = models.TextField( verbose_name=u"Краткое содержание", null=True , blank=True )
    GENRE = (
        ('Балет','Балет'),
        ('Опера','Опера'),
        ('Концерт','Концерт'),
    )
    genre = models.CharField( max_length=25 ,verbose_name=u"Жанр мероприятия",choices=GENRE, blank=True )
    image = models.ImageField(verbose_name=u"Фото мероприятия", null=True , blank=True)
    bannerimage = models.CharField( max_length=255 ,verbose_name=u"Фото на баннер главной", null=True , blank=True)
    afishaimage = models.CharField( max_length=255 ,verbose_name=u"Фото на афишу", null=True , blank=True)
    popularimage = models.CharField( max_length=255 ,verbose_name=u"Фото для популярные", null=True , blank=True)
    repertoireimage = models.CharField( max_length=255 ,verbose_name=u"Фото для раздела Репертуар", null=True , blank=True)
    video = models.CharField( max_length=2555 ,verbose_name=u"Видео", null=True , blank=True)
    actors = models.TextField(verbose_name=u"Действующие лица", null=True , blank=True)
    fullinfo = models.TextField(verbose_name=u"Организаторы постановки", null=True , blank=True)
    shortinfo = models.TextField(verbose_name=u"Краткая информация", null=True , blank=True)
    compositor = models.CharField(max_length=255, verbose_name=u'Композитор', null=True , blank=True)
    duration = models.CharField( max_length=255 , verbose_name=u"Продолжительность", null=True , blank=True )
    afisha = models.BooleanField( verbose_name=u"показать в Афише", default=False )
    popular = models.BooleanField( verbose_name=u"показать в Ближайшие", default=False )
    banner = models.BooleanField( verbose_name=u"показать на главном баннере", default=False )
    bannertext = models.TextField(verbose_name=u"Текст на баннер на главной", null=True , blank=True)

    def get_fullinfo(self):
        full_info = self.fullinfo.split('</li><li>')
        result = []
        for item in full_info :
            item.split('-')
            temp = []
            for i in item :
                temp.append(i)
            result.append(temp)
            
        return result
    
    def get_absolute_url(self):
        return reverse('main:pevent', kwargs={'pevent_alias': self.alias})
    
    def __unicode__(self):
        return self.pevent
        
#Одиночное мероприятия (которое привязано к одной дате)     
class Event(Page):
    class Meta:
        verbose_name = u"Событие(с датой) (Театры)"
        verbose_name_plural = u"События(с датой) (Театры)"
    
    name = models.CharField(max_length=500 , verbose_name=u"Название мероприятия")
    event = models.ForeignKey( Pevent, verbose_name=u"Событие" )
    parse = models.CharField(max_length=5000 , verbose_name=u"Значение парсера", null=True , blank=True)
    event_date = models.DateTimeField( verbose_name=u"Время начала события" )
    event_place = models.ForeignKey( Place, verbose_name=u"Место проведения события" )
    event_scene = models.ForeignKey( Scene , verbose_name=u"Сцена проведения события" )
    event_program = models.TextField( verbose_name=u"Программка мероприятия", null=True , blank=True )
    
    def get_absolute_url(self):
        return reverse('event', kwargs={'event_alias': self.alias})
    
    def __unicode__(self):
        return self.name


#Команда        
class Team(Page):
    class Meta:
        verbose_name = u"Команда (Футбол)"
        verbose_name_plural = u"Команды (Футбол)"

    name = models.CharField( max_length=200 , verbose_name=u"Название")
    shortname = models.CharField( max_length=3 , verbose_name=u"Сокращенное название", null=True , blank=True)
    city = models.CharField(max_length=200, verbose_name=u"Город" , null=True , blank=True)
    country = models.CharField( max_length=200 , verbose_name=u"Страна" , null=True , blank=True )
    stadium = models.ForeignKey(Stadium, verbose_name=u"Домашний стадион" , null=True , blank=True)
    offsite = models.CharField( max_length=200 , verbose_name=u"Официальный сайт" , null=True , blank=True )
    logo = models.CharField( max_length=3000 ,verbose_name=u"Логотип" , null=True , blank=True )
    flag = models.CharField( max_length=3000 ,verbose_name=u"Флаг" , null=True , blank=True )
    photo = models.CharField( max_length=3000 ,verbose_name=u"Фотография команды" , null=True , blank=True )
    forma = models.CharField( max_length=3000 ,verbose_name=u"Фотография формы" , null=True , blank=True )
    president = models.CharField( max_length=100 ,verbose_name=u"Президент клуба" , null=True , blank=True )
    founded = models.IntegerField( verbose_name=u"Год основания" , null=True , blank=True )

    techteam = models.BooleanField( default=False, verbose_name=u"Техническая команда" )

    def get_logo_list(self):
        return self.logo.split(',')

    def get_flag_list(self):
        return self.flag.split(',')

    def get_photo_list(self):
        return self.photo.split(',')

    def get_absolute_url(self):
        return reverse('base:team', kwargs={'team_alias': self.alias})

    def __unicode__(self):
        return self.name
        
#Игрок
class Player(Page):
    class Meta:
        verbose_name = u"Игрок"
        verbose_name_plural = u"Футболисты"

    name = models.CharField(max_length=200, verbose_name=u"Фамилия Имя")
    eng_name = models.CharField(max_length=200, verbose_name=u"Английское имя", null=True , blank=True)
    fioname = models.CharField(max_length=200, verbose_name=u"Полное Имя", null=True , blank=True)
    position = models.CharField(max_length=100, verbose_name=u"Позиция" , null=True , blank=True )
    number = models.CharField(max_length=3, verbose_name=u"Номер в команде" , null=True , blank=True )
    birth = models.DateField(verbose_name=u"День рождения" , null=True , blank=True )
    team = models.ForeignKey(Team, verbose_name=u"Команда" )
    country = models.CharField(max_length=100, verbose_name=u"Гражданство" , null=True , blank=True )
    club = models.CharField(max_length=100, verbose_name=u"Клуб" , null=True , blank=True )
    height = models.CharField(max_length=100, verbose_name=u"Рост" , null=True , blank=True )
    weight = models.CharField(max_length=100, verbose_name=u"Вес" , null=True , blank=True )
    image = models.CharField(max_length=100,verbose_name=u"Фотография" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('base:player', kwargs={'player_alias': self.alias})

    def __unicode__(self):
        return self.name
        
        
#Чемпионат        
class Champ(Page):
    class Meta:
        verbose_name = u"Чемпионат"
        verbose_name_plural = u"Чемпионаты"

    name = models.CharField( max_length=200 , verbose_name=u"Название")
    image = models.CharField(max_length=3000 ,verbose_name=u"Лого"  , null=True , blank=True )
    year = models.IntegerField( verbose_name=u"Год проведения" , null=True , blank=True )
    status = models.ImageField(verbose_name=u"Статус"  , null=True , blank=True )
    fed = models.ForeignKey(Fed, verbose_name=u'Федерация')
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в чемпионате" , blank=True )

    web = models.TextField(verbose_name=u"Турнирная сетка" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('base:champ', kwargs={'champ_alias': self.alias})

    def __unicode__(self):
        return self.name 
        
class Statplayer(Page):
	class Meta:
		verbose_name = u"Стата игрока"
		verbose_name_plural = u"Стата игроков"
		
	name = models.ForeignKey(Player, verbose_name=u'Игрок')
	champ = models.ForeignKey(Champ, verbose_name=u'Чемпионат')
	team = models.ForeignKey(Team, verbose_name=u'Команда')
	game = models.IntegerField(verbose_name=u'Игр')

class Teamchamp(Page):
    class Meta:
        verbose_name = u"Команда чемпионата (Футбол)"
        verbose_name_plural = u"Команды чемпионата (Футбол)"

    name = models.ForeignKey(Team, verbose_name=u"Название")
    shortname = models.CharField( max_length=3, verbose_name=u"Сокращенное название", null=True , blank=True)
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат")
    stadium = models.ForeignKey(Stadium, verbose_name=u"Домашний стадион" , null=True , blank=True)
    logo = models.CharField( max_length=3000 ,verbose_name=u"Логотип" , null=True , blank=True )
    photo = models.CharField( max_length=3000 ,verbose_name=u"Фотография команды" , null=True , blank=True )
    forma = models.CharField( max_length=3000 ,verbose_name=u"Фотография формы" , null=True , blank=True )
    techteam = models.BooleanField( default=False, verbose_name=u"Техническая команда" )
    #Результаты сезона
    points = models.IntegerField( verbose_name=u"Очков в текущем турнире" , null=True , blank=True )
    goals = models.IntegerField( verbose_name=u"Голов забито" , null=True , blank=True )
    vgoals = models.IntegerField( verbose_name=u"Голов пропущено" , null=True , blank=True )
    win = models.IntegerField( verbose_name=u"Выигрышей" , null=True , blank=True )
    lose = models.IntegerField( verbose_name=u"Поражений" , null=True , blank=True )
    draw = models.IntegerField( verbose_name=u"Ничьи" , null=True , blank=True )
    mesto = models.IntegerField( verbose_name=u"Место в чемпионате" , null=True , blank=True )

    def get_logo_list(self):
        return self.logo.split(',')

    def get_photo_list(self):
        return self.photo.split(',')

    def get_absolute_url(self):
        return reverse('base:teamchamp', kwargs={'team_alias': self.alias})

    def __unicode__(self):
        return self.name.name  
        
class Group(Page):
    class Meta:
        verbose_name = u"Группа (Футбол)"
        verbose_name_plural = u"Группы (Футбол)"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат" , null=True , blank=True )
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в группе" , blank=True )
    qualifying = models.BooleanField( default=False , verbose_name=u"Отброчная группа" )
    
    def get_absolute_url(self):
        return reverse('portal:group', kwargs={'group_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        

        
class Zayavka(Page):
    class Meta:
        verbose_name = u"Заявка на чемпионат (Футбол)"
        verbose_name_plural = u"Заявки на чемпионаты (Футбол)"
      
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат")
    team = models.ForeignKey(Team, verbose_name=u"Сборная")
    players = models.ManyToManyField(Player, verbose_name=u"Заявленные игроки")
    
    def get_absolute_url(self):
        return reverse('portal:zayavka', kwargs={'zayavka_alias': self.alias})
    
    def __unicode__(self):
        return self.champ.name+' - '+self.team.name
        
class Match(Page):
    class Meta:
        verbose_name = u"Матч"
        verbose_name_plural = u"Матчи"

    matchid = models
    datetime = models.DateTimeField(verbose_name=u"Время начала матча")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат" , null=True , blank=True )
    group = models.ForeignKey(Group, verbose_name=u"Группа" , null=True , blank=True )
    stadium = models.ForeignKey(Stadium, verbose_name=u"Стадион" , null=True , blank=True )

    tour = models.IntegerField(verbose_name=u"Тур" , null=True , blank=True )
    command_first = models.ForeignKey(Team, verbose_name=u"Первая команда", related_name="command_first")
    command_second = models.ForeignKey(Team, verbose_name=u"Вторая команда", related_name="command_second")

    #f_goals = models.IntegerField( verbose_name=u"Голы" , null=True , blank=True )

    #command_data = JSONField()
    #history = JSONField()
    #actions_data = JSONField()
    #offace_data = JSONField()
    parse = models.CharField(max_length=1000, verbose_name=u"Откуда парсим" , null=True , blank=True )
    score = models.CharField(max_length=100, verbose_name=u"Счёт, (например: 2 : 1)" , null=True , blank=True )
    status = models.CharField(max_length=100, verbose_name=u"Статус матча" , null=True , blank=True )
    minute = models.CharField(max_length=10, verbose_name=u"Текущее время матча" , null=True , blank=True )    
    goals = JSONField( verbose_name=u"Голы" , null=True , blank=True )
    nogoalpenalty = JSONField( verbose_name=u"Незабитые пенальти" , null=True , blank=True )
    foals = JSONField( verbose_name=u"Нарушения" , null=True , blank=True )
    people = models.IntegerField( verbose_name=u"Посещаемость" , null=True , blank=True )
    penalty = JSONField( verbose_name=u"Наказания" , null=True , blank=True )
    osnova1 = JSONField( verbose_name=u"Основной состав 1 команды" , null=True , blank=True )
    osnova2 = JSONField( verbose_name=u"Основной состав 2 команды" , null=True , blank=True )
    zamena1 = JSONField( verbose_name=u"Запасной состав 1 команды" , null=True , blank=True )
    zamena2 = JSONField( verbose_name=u"Запасной состав 2 команды" , null=True , blank=True )
    trener1 = JSONField( verbose_name=u"Тренер 1 команды" , null=True , blank=True )
    trener2 = JSONField( verbose_name=u"Тренер 2 команды" , null=True , blank=True )
    referee = JSONField( verbose_name=u"Главный судья" , null=True , blank=True )
    assistent1 = JSONField( verbose_name=u"1 Боковой судья" , null=True , blank=True )
    assistent2 = JSONField( verbose_name=u"2 Боковой судья" , null=True , blank=True )
    reserve = JSONField( verbose_name=u"Резрвный судья" , null=True , blank=True )
    text = JSONField( verbose_name=u"Текстовая трансляция" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('base:match', kwargs={'match_alias': self.alias})

    def __unicode__(self):
       return self.command_first.name + ' - ' + self.command_second.name + ' ' + str(self.datetime)  
          
    def get_next(self):
        flag = False
        delta = timezone.now() - self.datetime
        if delta < 0 :
            flag = True
        return flag    

    def get_win(self):
    
        team = self.score.split(':')
        if int(team[0]) > int(team[1]) :
            win = self.command_first.name
        
        elif int(team[0]) < int(team[1]) :
            win = self.command_second.name
        
        else :
            win = u'ничья'
            
        return win
    
    def get_lifematches(self):
        
        status = self.status
        if status == u'1-ый тайм' :
            livematch = True
        elif  status == u'Перерыв':
            livematch = True
        elif status == u'2-ой тайм':
            livematch = True
        else :
            livematch = False

        return livematch       
class News(Page):
    class Meta:
        verbose_name = u"Новость (Общая)"
        verbose_name_plural = u"Новости (Общая)"
        
    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    date = models.DateField( default=datetime.date.today , verbose_name=u"Время публикации" )
    
    def get_absolute_url(self):
        return reverse('portal:news', kwargs={'news_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
