# -*- coding: utf-8 -*-
import datetime
import re, sys, os
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.contrib.sitemaps import Sitemap
from django.core.mail import send_mail, BadHeaderError
from django.template import RequestContext, loader
from django.db.models import Q

# импортируем модели
from .models import TextPage, Team, Champ, Group, City, Stadium, Zayavka, Match, Player, Teamchamp, News, Scripts

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias

def default_context(request,alias,object):
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    data = get_object_or_404(object, alias=alias)
    scripts = Scripts.objects.all()
    active_url = active_menu(request)
    datee = datetime.datetime.now()
    zone = timezone.now()
    
    context_object = {
        'menu' : menu,
        'data' : data,
        'active_url' : active_url,
        'scripts' : scripts,
        'datee' : datee,
        'zone' : zone,
    }
    
    return context_object

@csrf_exempt 
def postupdater(request): 
    try :
        object_name_m = re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',request.POST['type']).strip()
    except KeyError :
        return HttpResponse(u'Модель не определена!')
     
    sys.path.append(os.path.join(settings.BASE_DIR, 'scripts'))
    import updater
    result = ''
    
    object_name_arr = object_name_m.split(',')
    write_arr = request.POST['write'].encode('utf-8').split('@@')
    
    i = 0
    for object_name in object_name_arr :
    
        write = 'break\n'
        
        for wr in write_arr[i].split('::') :
            write += wr+'\n'
            
        data = open(os.path.join(settings.BASE_DIR, 'scripts')+'/pr/'+object_name+'.pr' , 'w')
        data.write(write)
        data.close()
        
        result += updater.init( object_name )
        
        i += 1
    return HttpResponse(result)    
    
  
def get_field_value( object , field ):
    from django.db.models.fields import IntegerField, DateField, DateTimeField
    from django.db.models.fields.files import ImageField
    from django.contrib.postgres.fields.jsonb import JSONField
    import json

    value = eval("object."+field)
    field_type = "other"
    
    if type(value) == bool :
        field_type = "bool"
        if value :
            value = "1"
        else :
            value = "0"
        
    elif isinstance( object._meta.get_field(field) , IntegerField ) : 
        field_type = "int"
        if value :
            value = str( value )
        else :
            value = "0"
            
    elif isinstance( object._meta.get_field(field) , DateField ) : 
        field_type = "date"
        if value :
            value = str( value.strftime("%d.%m.%Y,%H:%M") )
        else :
            value = "01.01.1900"
            
    elif isinstance( object._meta.get_field(field) , DateTimeField ) : 
        field_type = "datetime"
        if value :
            value = str( value.strftime("%d.%m.%Y,%H:%M") )
        else :
            value = "01.01.1900,00:00"
            
    elif isinstance( object._meta.get_field(field) , ImageField ) : 
        field_type = "image"
        if value :
            value = str( value ).split("/")[-1]
        else :
            value = ""
    
    elif isinstance( object._meta.get_field(field) , JSONField ) :
        field_type = "json"
        value = json.dumps(value , ensure_ascii = False , encoding='utf8')
    
    elif type(value) == unicode or type(value) == str :
        field_type = "str"
        value = value.encode('utf8').replace('"', '\\"')
    
    else :
        if value :
            value = str( value ).replace('\n', ' ')
        else :
            value = ""
        
    return [field , value , field_type]

def export(request,export_type):
    template = loader.get_template('export.html')
    if export_type == 'team' :
        object = Team
    elif export_type == 'match' :
        object = Match
    elif export_type == 'player' :
        object = Player
    elif export_type == 'stadium' :
        object = Stadium
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'fed' :
        object = Fed
    elif export_type == 'group' :
        object = Group
    elif export_type == 'textpage' :
        object = TextPage
    else :
        object = Team
    fields = [field.name for field in object._meta.fields]
    
    objects = object.objects.all()

    data = []
    for object in objects :
        item = []
        for field in fields :
            if field != 'id' and field != 'content' :
                field_param = get_field_value( object , field ) # [field , value , type]
                item.append(field_param)
        data.append(item)
            
    context = RequestContext(request, {
        'objects' : objects,
        'fields' : fields,
        'data' : data,
    })
    
    return HttpResponse(template.render(context) , content_type="text/plain;charset=utf-8")

def error_404(request):
    template = loader.get_template('404.html')
    context_data = default_context( request , match_alias , TextPage )
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    
    context_data.update({
        'menu' : menu,
    })
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")

def yandex_verification(request,yandex_verification_number):
    template = loader.get_template('yandex_verification.html')
    context = RequestContext(request,{
        'yandex_verification_number' : yandex_verification_number,
    })
    return HttpResponse(template.render(context))
    
def google_verification(request,google_verification_number):
    template = loader.get_template('google_verification.html')
    context = RequestContext(request,{
        'google_verification_number' : google_verification_number,
    })
    return HttpResponse(template.render(context))

def mail_verification(request,mail_verification_number):
    template = loader.get_template('mail_verification.html')
    context = RequestContext(request,{
        'mail_verification_number' : mail_verification_number,
    })
    return HttpResponse(template.render(context))
    
def ajaxcontroller(request):
    if request.is_ajax():
        new_min = 'ajax:no-method'
        if request.POST['type'] == 'mins':
            #match = Match.objects.get(alias='match1')
            #this_url = 'match1'
            #this_url = request.POST['id']
            #request.POST['id'] = 'match1'
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.minute
            
        elif request.POST['type'] == 'score':
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.score
            
        elif request.POST['type'] == 'content':
            if request.user.is_authenticated():
                match = Match.objects.get(alias=request.POST['id'])
                match.content = request.POST['this_cont']
                match.save()
                MyValue = match.content
            else:
                MyValue = u'Неавторизован'
                
        return HttpResponse(MyValue)
    else:
        raise Http404
