# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-19 18:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('club', '0002_auto_20160919_1853'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='city',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='club.City', verbose_name='\u0413\u043e\u0440\u043e\u0434'),
        ),
    ]
