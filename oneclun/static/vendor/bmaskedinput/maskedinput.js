$.fn.maskinput = function(finale, initiale) {
    /*
    A very basic masked input implementation
    Accepts two arguments:
        - `finale`: regexp for complete input
        - `initiale` (optional): regexp for incomplete input, it is against this regexp that
            all user input will be matched; if this argument is not passed, maskedinput will
            try to come up with its own version of `initiale` using `finale`, however this
            algorithm does not support non-basic constructs such as back and forward references,
            grouping, and "|".
    Input tag with valid, complete input value is decorated with `bmi-complete` css class.
    Valid, but incomplete input receives `bmi-incomplete` css class.
    The algorithm assumes that the input tag will never contain invalid input, however
        it will not be always the case, e.g. with inputs populated with copy&paste.
    */
    var chop = function(string, boundaries) {
        var seq = [];
        var i = 0;
        while(i < boundaries.length - 1) {
            seq.push(string.slice(boundaries[i], boundaries[i + 1]));
            i ++;
        }
        return seq;
    }
    var autoinitiale = function(regexp) {
        var source = regexp.source;
        if(source.slice(0, 1) === '^') {
            source = source.slice(1, source.length);
        }
        if(source.slice(source.length - 1, source.length) === '$') {
            source = source.slice(0, source.length - 1);
        }
        var i = 0;
        var atomIsOpen = false;
        var closeAtomOn = undefined;
        var closeAtomIn = undefined;
        var atomBoundaries = [];
        while(i < source.length) {
            var ch = source[i];
            if(atomIsOpen) {
                if(closeAtomOn === ch) {
                    atomIsOpen = false;
                    closeAtomOn = undefined
                    // atomBoundaries.push(i);
                }else if(closeAtomIn === 0) {
                    atomIsOpen = false;
                    closeAtomIn = undefined
                    // atomBoundaries.push(i);
                }
            }else {if(ch === '[') {
                    atomIsOpen = true;
                    closeAtomOn = ']'
                }else if(ch === '\\') {
                    atomIsOpen = true;
                    closeAtomIn = 1;
                }
                if(ch === '{') {
                    atomIsOpen = true;
                    closeAtomOn = '}'
                }else if(ch === '+' || ch === '*' || ch === '?') {
                    // pass
                }else {
                    atomBoundaries.push(i);
                }
            }
            if(closeAtomIn !== undefined) {
                closeAtomIn --;
            }
            i ++;
        }
        atomBoundaries.push(source.length);
        var chunks = chop(source, atomBoundaries);
        var transformedRegexp = '^' + chunks[0];
        i = 0;
        while(i < chunks.length) {
            var chunk = chunks[i];
            if(chunk.slice(chunk.length - 1, chunk.length) === '}') {
                chunk = chunk.split('{');
                var prefix = chunk[0];
                chunk = chunk[1];
                chunk = chunk.slice(0, chunk.length - 1).replace(' ', '').split(',')
                if(chunk.length > 1) {
                    chunk = prefix + '{0,' + chunk[1] + '}';
                }else {
                    chunk = prefix + '{0,}';
                }
            }
            chunks[i] = chunk;
            if(i > 0) {
                transformedRegexp = transformedRegexp + '(' + chunk;
            }
            i ++;
        }
        i = 1;
        while(i < chunks.length) {
            transformedRegexp = transformedRegexp + ')?';
            i ++;
        }
        return RegExp(transformedRegexp + '$');
    }
    if(initiale === undefined) {
        initiale = autoinitiale(finale);
    }
    var applyWithTimeout = function() {
        return function() {
            var args = arguments;
            setTimeout(function() {
                onchange.apply(null, args)
            }, 0);   
        }
    }
    var onchange = function(event) {
        var body = function(input) {
            var canBeMatched = input.val().match(initiale) !== null;
            var isMatched = input.val().match(finale) !== null;
            input.removeClass('bmi-incomplete');
            input.removeClass('bmi-complete');
            if(!isMatched && canBeMatched) {
                input.addClass('bmi-incomplete');
            }else if(isMatched) {
                input.addClass('bmi-complete');
            }else if(!canBeMatched) {
                input.val(input.val().slice(0, input.val().length - 1));
                if(input.val().length > 0) {
                    body(input);
                }
            }
        }
        body($(event.target));
    }
    this.on('keydown', applyWithTimeout(onchange));
}