ymaps.ready(function() {
    var displayMap = function($element) {
        var oid = $element.data("oid");
        var coordsRaw = $element.data("coords").split(",");
        var address = $element.data("address");
        var coords = [parseFloat(coordsRaw[0]), parseFloat(coordsRaw[1])];
        var map = new ymaps.Map("map_" + oid, {center: coords, zoom: 13});
        var placemark = new ymaps.Placemark(coords, { 
            hintContent: address, 
            balloonContent: address 
        });
        map.geoObjects.add(placemark);
    };
    var hideMap = function($element) {
        var oid = $element.data("oid");
        $("#map_" + oid).children().remove();
    };
    $('a[data-toggle="tab"].map-toggle').on('shown.bs.tab', function(event) {
        displayMap($(event.target));
    });
    $('a[data-toggle="tab"].map-toggle').on('hidden.bs.tab', function(event) {
        hideMap($(event.target));
    });
    displayMap($($('a[data-toggle="tab"].map-toggle').eq(0)));
});