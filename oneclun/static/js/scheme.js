angular.module('SchemeApp', [], function() { });

angular.module('SchemeApp').directive('scheme', function() {
  return {
    template: "<div class='scheme'><ng-transclude></ng-transclude></div>",
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: {'eventId': '='},
    controller: function($scope, $element) {
      this.eventId = $scope.eventId;
      this.element = $element;
    },
    link: function(scope, element, attrs) {
    }
  }
});

angular.module('SchemeApp').directive('clickable', function() {
  var touch = !!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i);
  if (touch) template = "<div class='clickable mobile'><ng-transclude></ng-transclude></div>";
  else template = "<div class='clickable desktop'><ng-transclude></ng-transclude></div>";
  return {
    template: template,
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: false,
    require: ['^scheme'],
    controller: function($scope, $element) {
      this.bindSelection = function($el, selection) {
        $el.on("mouseenter", function(event) {
          $scope.$broadcast('showTooltip', {
            event: event,
            selection: selection
          });
        });
        $el.on("mouseleave", function() {
          $scope.$broadcast('hideTooltip', {});
          $scope.$emit('hideTooltip', {});
        });
      }
      var that = this;
      this.bindAction = function($el, callback, undoCallback) {
        $el.on("click", function() {
          $el.off("click");
          $el.attr("class", "active selected");
          $el.on('click', function() {
            $el.off("click");
            $el.attr("class", "active");
            that.bindAction($el, callback, undoCallback);
            if(undoCallback !== undefined) {
              undoCallback();
            }
          });
          callback();
        });
      }
      this.unbindSelection = function($el) {
        $el.off("mouseenter");
        $el.off("mouseleave");
      }
      this.unbindAction = function($el) {
        $el.off("click");
      }
      $(window).trigger('wvideo');
    },
    link: function(scope, element, attrs) {

    }
  }
});

angular.module('SchemeApp').directive('touchable', function($log) {
  return {
    template: "<div class='touchable'><ng-transclude></ng-transclude></div>",
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: false,
    require: ['^scheme'],
    controller: function($scope, $element) {
      var that = this;
      this.bindSelection = function($el, selection) {
        $el.on("touchstart click", function(event) {
          $($element).find('.active').each(function(_, el) {
            if(!$(el).is('.selected')) {
              $(el).attr("class", "active");
            }
          });
          $el.off("touchstart click");
          $el.attr("class", "active touch-hover");
          $scope.$broadcast('showTooltip', {
            event: event,
            selection: selection
          });
          event.stopPropagation();
          var handler = function(event) {
            var notInTooltip = $(event.target).closest('#tooltip').length === 0;
            if(notInTooltip) {
              if($el.is('.selected')) {
                return;
              }
              $el.off("touchstart click");
              $el.attr("class", "active");
              $scope.$apply(function() {
                $scope.$broadcast('hideTooltip', {});
              });
              $('body').off("touchstart click", handler);
              that.bindSelection($el, selection);
            }
          }
          $('body').on("touchstart", handler);
        });
      }
      this.bindAction = function($el, callback, undoCallback) {
        $scope.$on('tooltipSubmitted', function() {
          if(!!$el.attr('class') && $el.attr('class').indexOf('touch-hover') !== -1) {
            $el.attr('class', 'active selected');
            $el.off("touchstart click");
            $scope.$broadcast('hideTooltip', {});
            callback();
            $el.on('touchstart click', function() {
              $el.attr('class', 'active');
              $el.off("touchstart click");
              // that.bindAction($el, callback, undoCallback);
              undoCallback()
            });
          }
        });
      }
      this.unbindSelection = function($el) {
        $el.off("touchstart click");
      }
      this.unbindAction = function($el) {
        $el.off("touchstart click");
      }
    },
    link: function(scope, element, attrs) {
    }
  }
});

angular.module('SchemeApp').directive('tooltip', function($timeout) {
  return {
    template: '<div style="position: static !important;" class="hide"><div id="tooltip" ng-style="getCoordinates()">'
      + '<div class="tooltip-inner"><ng-transclude></ng-transclude></div></div></div></div>',
    transclude: true,
    replace: true,
    restrict: 'E',
    scope: false,
    require: ['^scheme', '?^touchable', '?^clickable'],
    link: function(scope, element, attrs, ctrls) {
      var schemeController = ctrls[0];
      var interactionController = ctrls[1] || ctrls[2];
      scope.active = false;
      scope.selection = undefined;
      scope.mouseCoordinates = [100, 0];
      scope.moving = element.moving === "true";
      scope.submit = function() {
        // console.log("submit");
        scope.$emit('tooltipSubmitted');
      }
      scope.cancel = function() {
        scope.active = false;
        scope.selection = undefined;
      }
      scope.getCoordinates = function() {
        return {
          "top": scope.mouseCoordinates[0],
          "left": scope.mouseCoordinates[1]
          // "top": 0,
          // "left": 0
        };
      }
      var updateMouseCoordinates = function(event) {
        var $svg = schemeController.element.find('svg');
        var svgOffset = $svg.offset();

        // if($(event.target).closest("text").length > 0) {
        //   return;
        // }

        var $tooltip = $(element).find("#tooltip");

        scope.$apply(function() {
          if(!!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
            console.log(scope.seats);
            if(!event.pageX){
              var left = event.originalEvent.touches[0].pageX - $tooltip.width()/2 - 25;
              left = left < 0 ? 0 : left;
              if (!!$('svg').closest('.seats').length) {
                  scope.mouseCoordinates = [
                    event.originalEvent.touches[0].pageY - svgOffset.top - $tooltip.height() + 20,
                    left
                  ]
              } else {
                  scope.mouseCoordinates = [
                    event.originalEvent.touches[0].pageY - svgOffset.top - $tooltip.height() - 25,
                    left
                  ]
              }
            } else {
                if (!!$('svg').closest('.seats').length) {
                  console.log('click');
                  scope.mouseCoordinates = [
                    event.pageY - svgOffset.top - $tooltip.height(),
                    event.pageX - $tooltip.width()/2 - 25
                  ]
                } else {
                  console.log('click');
                  scope.mouseCoordinates = [
                    event.pageY - svgOffset.top - $tooltip.height() - 25,
                    event.pageX - $tooltip.width()/2 - 25
                  ]
                }
            }
          } else {
            scope.mouseCoordinates = [
              event.pageY - svgOffset.top - $tooltip.height() - 20,
              event.pageX - $tooltip.width() - 20
            ]
          }
        });
      }
      if(scope.moving) {
        $(schemeController.element).on('mousemove', 'svg', function(event) {
          updateMouseCoordinates(event);
        });
      }
      scope.$on("showTooltip", function(event, payload) {
        element.removeClass('hide');
        window.selection = payload.selection;
        scope.selection = payload.selection;
        scope.active = true;
        $(window).trigger('tooltip-show');
        updateMouseCoordinates(payload.event);
      });
      scope.$on("hideTooltip", function(event) {
        element.addClass('hide');
        scope.active = false;
        scope.selection = undefined;
        if(!!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){$(window).trigger('tooltip-hide');}
      });
    }
  }
});

angular.module('SchemeApp').directive('sectors', function($log) {
  return {
    template: "<div class='sectors svg-container'><ng-transclude></ng-transclude></div>",
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: {onSelect: "&"},
    require: ['^scheme', '?^touchable', '?^clickable'],
    link: function(scope, element, attrs, ctrls) {
      var interactionController = ctrls[1] || ctrls[2];
      scope.$on('updateTickets', function(event, tickets) {
        var activeSectors = {};
        tickets.forEach(function(ticket) {
          if(activeSectors[ticket.sys_name] === undefined) {
            activeSectors[ticket.sys_name] = [ticket.sector_id, 0, ticket.title];
          }
          activeSectors[ticket.sys_name][1] += ticket.count || 1;
        });
        // $(element).find('g').off('click');
        $(element).find('g').removeAttr('class');
        Object.keys(activeSectors).forEach(function(sector) {
          var $sectorGroup = $(element).find('g#' + sector);
          if($sectorGroup === undefined) {
            $log.log('failed to find sector', sector);
          }
          $sectorGroup.attr('class', 'active');
          interactionController.bindSelection($sectorGroup, {
            name: activeSectors[sector][2],
            count: activeSectors[sector][1],
            sysName: sector
          });
          interactionController.bindAction($sectorGroup, function() {
            scope.onSelect()(activeSectors[sector][0]);
          });
        });
      });
      scope.$emit('sectorsRendered');
      scope.$broadcast('sectorsRendered');
      $(window).trigger('angLoad');
    }
  }
});

angular.module('SchemeApp').directive('seats', function($log, $cart) {
  return {
    template: "<div class='seats' ng-transclude></div>",
    replace: true,
    transclude: true,
    restrict: 'E',
    controller: 'SeatsController',
    scope: {'sector': '@', 'tickets': '@'},
    require: ['^scheme', '?^touchable', '?^clickable'],
    link: function(scope, element, attrs, ctrls) {
      var schemeController = ctrls[0];
      var interactionController = ctrls[1] || ctrls[2];
      $(element).find('path').attr('class', '');
      scope.tickets.forEach(function(ticket, i) {
        var offset = 1;
        if(scope.sector !== undefined) {
          if(scope.sector !== ticket.sys_name) {
            return;
          }
          var $sector = $(element).find('g#places');
        } else {
          var $sector = $(element).find('g#' + ticket.sys_name);
        }
        if(!!$sector.attr('data-offset')) {
          offset = parseInt($sector.attr('data-offset'), 10);
        }
        if($sector.children("title").length > 0) {
          offset = parseInt($sector.children("title").text(), 10);
        }
        var row = $sector.children()[ticket.row - offset];
        if(row === undefined) {
          $log.log('failed to find row', 'sys_name:' + ticket.sys_name, 'row:' + row);
          return;
        }
        var $row = $(row);
        var rowLength = $row.children().length;
        var seatOffset = 1;
        if($row.children("title").length > 0) {
          seatOffset = parseInt($row.children("title").text(), 10);
        }
        var $seat_list;
        var $seat;
        //if ($('.arena').text().indexOf('Химки') != -1) {
        //  $seat_list = $row.children();
        //  $seat = $($seat_list[$seat_list.length - ticket.seat + seatOffset - 1]);
        //} else {
        $seat = $($row.children()[ticket.seat - seatOffset]);
        //}
        $seat.attr('class', 'active');
        interactionController.bindSelection($seat, {
          name: ticket.full_title,
          price: ticket.price
        });
        interactionController.bindAction($seat, function() {
          $cart.parserAddToCart(schemeController.eventId, ticket);
        }, function() {
          $cart.parserRemoveFromCart(schemeController.eventId, ticket);
        });
      });
      $(window).trigger('angLoad');

    }
  }
});

angular.module('SchemeApp').directive('noseats', function($log, $cart) {
  return {
    template: "<div class='seats noseats'><ng-transclude></ng-transclude></div>",
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: {'sector': '@'},
    require: ['^scheme', '?^touchable', '?^clickable'],
    link: function(scope, element, attrs, ctrls) {
      var schemeController = ctrls[0];
      var interactionController = ctrls[1] || ctrls[2];
      scope.$on('updateTickets', function(event, tickets) {
        // console.log(tickets);
        $(element).find('path').attr('class', '');
        tickets.forEach(function(ticket, i) {
          if(scope.sector !== undefined) {
            if(scope.sector !== ticket.sys_name) {
              return;
            }
            var $sector = $(element).find('g#places');
          }else {
            var $sector = $(element).find('g#' + ticket.sys_name);
          }
          var $gs = $sector.children('g');
          var selectedTicketsCount = 0;
          $gs.each(function(j, g) {
            if(ticket.count > j) {
              var $g = $(g);
              $g.attr('class', 'active');
              interactionController.bindSelection($g, {
                name: ticket.full_title,
                price: ticket.price
              });
              interactionController.bindAction($g, function() {
                selectedTicketsCount += 1;
                $cart.fallbackUpdateCart(schemeController.eventId, ticket, selectedTicketsCount);
              }, function() {
                selectedTicketsCount -= 1;
                $cart.fallbackUpdateCart(schemeController.eventId, ticket, selectedTicketsCount);
              });
            }
          });
        });
      });
      scope.$emit('sectorsRendered');
      scope.$broadcast('sectorsRendered');
      $(window).trigger('angLoad');
    }
  }
});