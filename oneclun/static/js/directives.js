angular.module('MainApp').directive('sliSticky', function () {
    function link(scope, element, attrs) {
        try {
            element.stick_in_parent({offset_top: 70, parent: '.section-3'});
            setTimeout(function () {
                $(document.body).trigger("sticky_kit:recalc")
            }, 2000)
        } catch (e) {
        }
    }

    return {
        link: link
    }
});