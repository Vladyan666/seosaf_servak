window.onload = function () {
  var video = '<div class="overlay-video-black"></div>' +
    '<video class="bgvideo" preload="auto" autoplay="true" muted>' +
    '<source   src="/static/layout/media/cska.mp4" type="video/mp4">' +
    '<source   src="/static/layout/media/cska.webm" type="video/webm">' +
    '<source   src="/static/layout/media/cska.ogv" type="video/ogg">' +
    '</video>';
  if (!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
    if(!(window.location.href.indexOf('match')+1)) {
      document.querySelector('.bgvideo').innerHTML = video;
    }
    if(document.querySelectorAll('.list-countries').length > 0) {
      var shareElem = document.createElement('script');
      shareElem.type = "text/javascript";
      shareElem.src = "//yastatic.net/share/share.js";
      shareElem.charset="utf-8";
      var shareParent = document.querySelector('.list-countries');
      shareParent.appendChild(shareElem);
    }
  }
  setTimeout(function() {
    $('.bgvideo').animate({opacity: "hide"}, 3000);
  }, 36000);
  var element = document.getElementById('video') || undefined;
  if (typeof element != 'undefined') element.muted = "muted";
}

var requestAnimFrame = (function(){
  return window.requestAnimationFrame  ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(callback){
      window.setTimeout(callback, 1000 / 60);
    };
})();

(function($) {
  if (navigator.userAgent.indexOf('Trident') > -1) {
    $(window).on('angLoad', function() {
      var $svg = $('svg'),
          $containerWidth = $('.clickable').width(),
          height = $svg.get(0).viewBox.baseVal.height,
          width = $svg.get(0).viewBox.baseVal.width,
          ratio = height/width;
      $svg.css({'width': $containerWidth + 'px', 'height': $containerWidth*ratio + 'px'});
    });
  }
})(jQuery);

$(document).ready(function() {
  $('.match-hidden').closest('.col-xs-12').hide();
  $(window).on('proceed', function() {
    $('#loading').addClass('proceed');
    $('#loading').parent().addClass('block');
    $('#loading').find('.text').addClass('pulse');
  });

  $('.nonscrollable-top').on('click', function() {
    $('.float-mobile-top').toggleClass('float-hidden');
  });

  $('.menu-link').bigSlide();

  $(window).on('tooltip-show', function() {
      $tooltip = $('#tooltip');
      if (selection.name.indexOf('Silve') + 1 == 1) {
        selection.name = 'A5 Silver' + selection.name.substring(5);
      }
      if (selection.name.indexOf('S1') + 1) {
        selection.name = 'A5 Silver' + selection.name.substring(2);
      }
      $tooltip.find('h5').text(selection.name);
      if(selection.count) {
        $tooltip.find('span').text(selection.count);
      } else {
        $tooltip.find('span').text(selection.price);
      }
  });

  $(window).on('changeTotalTickets', function() {
    $('.tickets-count').addClass('mod-flip');
    setTimeout(function(){
      $('.tickets-count').removeClass('mod-flip');
    }, 1000);
  });

  var navU = navigator.userAgent,
      isTouchscreen = !!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i),
      is_iOS = !!navigator.userAgent.match(/iPhone|iPad|iPod/i)
      isAndroidMobile = navU.indexOf('Android') > -1 && navU.indexOf('Mozilla/5.0') > -1 && navU.indexOf('AppleWebKit') > -1,
      regExAppleWebKit = new RegExp(/AppleWebKit\/([\d.]+)/),
      resultAppleWebKitRegEx = regExAppleWebKit.exec(navU),
      appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat(regExAppleWebKit.exec(navU)[1])),
      regExChrome = new RegExp(/Chrome\/([\d.]+)/),
      resultChromeRegEx = regExChrome.exec(navU),
      chromeVersion = (resultChromeRegEx === null ? null : parseFloat(regExChrome.exec(navU)[1])),
      isAndroidBrowser = isAndroidMobile && (appleWebKitVersion !== null && appleWebKitVersion < 537) || (chromeVersion !== null && chromeVersion < 20),
      isMobileChrome = isAndroidMobile && !isAndroidBrowser;
  window.isMobileChrome = isMobileChrome;

  if (is_iOS) {
    $('body').addClass('ios-device');
  }

  if (isTouchscreen) {
    var n_scale = screen.width/1265;
    document.getElementById("view").setAttribute('content', 'width=1265, initial-scale='+ n_scale +', minimum-scale='+ n_scale +', maximum-scale='+ n_scale);
    document.getElementById("view").setAttribute('content', 'width=1265, initial-scale='+ n_scale +', minimum-scale='+ n_scale);
    $('.main-container').css('min-height', $(window).height());
    //setTimeout(function() {$('.section-1-3').css('height', $('.section-1-3').height());}, 2000);
    if (!isMobileChrome) {
      //$('body').css('overflow', 'hidden');
    }
    //$('.main-container').css('overflow', 'hidden');
  }

  $('a.ancLinks[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash,
    $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    return $(this).ekkoLightbox();
  });
  if(!isTouchscreen) {
    $('#quote-carousel').carousel();
    $("#sticker-menu").sticky({topSpacing: 0});
    //$("#sticker").stick_in_parent({offset_top: 70, parent: '.pay-content'});
    setTimeout(function () {
      $(document.body).trigger("sticky_kit:recalc")
    }, 10000);
  }
  setInterval(function() {
    $('.slab').fitText();
    totalFs = $('.total-tickets').css('font-size');
    $('.total-tickets').parent().css('font-size', totalFs);
  }, 1000);

  $(".sound-toggle").click(function(){$(this).toggleClass("sound-toggle--on sound-toggle--off");var a=$("#audio");
    0==a[0].paused?a.animate({volume:0},1e3,"swing",function(){a[0].pause()}):(a[0].play(),a.animate({volume:.2},1e3))
  });

  //Floatlabel
  $('.group-order-block input').floatlabel();
  $('.group-order-block a, .group-order-block button').click(function(e){
    e.preventDefault();
  });
  if(!isTouchscreen) {
    jQuery('.post').addClass("hidden2").viewportChecker({
      classToAdd: 'visible2 animated fadeInDown', // Class to add to the elements when they are visible
      offset: 200
    });

    jQuery('.post2').addClass("hidden2").viewportChecker({
      classToAdd: 'visible2 animated bounceInLeft',
      offset: 140
    });
    jQuery('.post3').addClass("hidden2").viewportChecker({
      classToAdd: 'visible2 animated bounceInRight',
      offset: 100
    });
    jQuery('.post4').addClass("hidden2").viewportChecker({
      classToAdd: 'visible2 animated bounceInUp',
      offset: 100
    });
    jQuery('.post5').addClass("hidden2").viewportChecker({
      classToAdd: 'visible2 animated fadeIn',
      offset: 300
    });
  }

  if(isTouchscreen) {
    $('#sticker-menu').addClass('mobile');
    window.showNav = false;
    $('.phone').addClass('slab');
    $('.mobile-menu-icon').on('touchstart scroll', function(e) {
      e.stopPropagation();
      if (window.showNav == false) {
        console.info('Show menu');
        var $navLeft = $('.navbar-left');
        var count;
        $navLeft.addClass('nh');
        $(this).find('.hamburger').removeClass('show');
        $(this).find('.close').addClass('show');
        count = 100;
        $navLeft.find('li:not(.hidden-mobile)').each(function(i, e) {
          setTimeout(function() {
            $(e).addClass('anim')
          }, count*(i-1))
        });
        window.showNav = true;
        return 'Show menu'
      } else {
        hideMobileMenu(e);
      }
    });
    $('body').on('touchstart scroll', hideMobileMenu);
    $('.navbar-left').on('click touchstart', function(e) {
      e.stopPropagation();
    });
    $('.navbar-left').find('a:not(.callme)').on('click touchstart',function(e){
      e.stopPropagation();
      $('#callme').modal('hide');
      setTimeout(hideMobileMenu, 1000)
    });
    $('.navbar-left').find('a.callme').on('click touchstart',function(e) {
      e.stopPropagation();
      e.preventDefault();
      $('#callme').modal();
      setTimeout(hideMobileMenu, 300)
    });
    calculatePosition();
  }

  /* calculate position of fixed elems on mobile devices with zoom */
  function calculatePosition() {
    if (!el || !el.length) {var el = $(".position-fix");}
    var lowerleft = [window.pageXOffset,(window.pageYOffset+window.innerHeight)];
    var lowerright = [(lowerleft[0] + window.innerWidth),lowerleft[1]];
    var zoomFactor = window.innerWidth/document.documentElement.clientWidth;
    if (zoomFactor < 1) {
      if (!!el && !!el.length) {
        el.each(function (indx, elem) {
          if (!$(elem).hasClass('navbar')) {
            if ($(elem).hasClass('with-parser')) {
              $(elem).css('width', lowerright[0] - lowerleft[0] + 'px');
            } else {
              $(elem).css('width', lowerright[0] - lowerleft[0] - 15 + 'px');
            }
            $(elem).css({
              'position': 'absolute',
              'height': parseInt(window.innerHeight / 10) + 'px',
              'left': lowerleft[0] + 'px',
              'top': lowerleft[1] - $(elem).outerHeight() + 'px',
              'fontSize': parseInt(zoomFactor * 60) + 'px',
              'bottom': 'initial'
            });
          } else {
            $(elem).css('height', (window.innerHeight/100)*8);
            $(elem).css({
              'position': 'absolute',
              'top': $(window).scrollTop() + 'px',
              'fontSize': parseInt(zoomFactor * 60) + 'px'
            });
            $(elem).find('.navbar-inner').css('width', window.innerWidth + 'px')
            if(lowerleft[0] > 0) {
              $(elem).find('.navbar-inner').css({
                'left': window.pageXOffset + window.innerWidth < document.body.clientWidth ? lowerleft[0] : document.body.clientWidth - window.innerWidth
              })
            } else {
              $(elem).find('.navbar-inner').css({
                'left': 0
              });
            }
            $(elem).find('.nav-pills').css('font-size', zoomFactor * 44 + 'px');
            //$('.show-mobile').find('.ancLinks').css('font-size', '25px');
            //$('.phone').css('font-size', '20px');
          }
        });
      }
    } else {
      el.removeAttr('style');
      $('.nav-pills').css('font-size', '44px');
      $('.navbar-inner').removeAttr('style');
    }
    if (screen.orientation) {
      screen.orientation.type.indexOf('portrait') > -1 ? window.c_orientation = 'portrait' : window.c_orientation = 'landscape'
    } else if (window.orientation) {
      window.orientation == 0 ? window.c_orientation = 'portrait' : window.c_orientation = 'landscape'
    }
    if(window.c_orientation != window.orientationCache) {
      $(window).trigger('orientation-change')
    }
    window.orientationCache = window.c_orientation;
    requestAnimFrame(calculatePosition);
  }

});

function adjustModalMaxHeightAndPosition(){
  $('.modal').each(function(){
    if($(this).hasClass("modal-adjust-opt-out")) {
      return;
    }
    if($(this).hasClass('in') == false){
      $(this).show();
    };
    var contentHeight = $(window).height() - 60;
    var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
    var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;

    $(this).find('.modal-content').css({
      'max-height': function () {
        return contentHeight;
      }
    });

    $(this).find('.modal-body').css({
      'max-height': function () {
        return (contentHeight - (headerHeight + footerHeight));
      }
    });

    $(this).find('.modal-dialog').css({
      'margin-top': function () {
        return -($(this).outerHeight() / 2);
      },
      'margin-left': function () {
        return -($(this).outerWidth() / 2);
      }
    });
    if($(this).hasClass('in') == false){
      $(this).hide();
    }
  });
}

function hideMobileMenu(ev) {
  if (window.showNav == true) {
    console.info('Hide menu');
    var $navLeft = $('.navbar-left'),
        $mmi = $('.mobile-menu-icon');
    $navLeft.removeClass('nh');
    $mmi.find('.close').removeClass('show');
    $mmi.find('.hamburger').addClass('show');
    $navLeft.find('li:not(.hidden-mobile)').removeClass('anim');
    window.showNav = false;
  }
};

$(window).resize(adjustModalMaxHeightAndPosition).trigger("resize");

(function(e,t,n,r){function o(t,n){this.$element=e(t);this.settings=e.extend({},s,n);this.init()}var i="floatlabel",s={slideInput:true,labelStartTop:"20px",labelEndTop:"10px",transitionDuration:.3,transitionEasing:"ease-in-out",labelClass:"",typeMatches:/text|password|email|number|search|url/};o.prototype={init:function(){var e=this;var n={"-webkit-transition":"all "+this.settings.transitionDuration+"s "+this.settings.transitionEasing,"-moz-transition":"all "+this.settings.transitionDuration+"s "+this.settings.transitionEasing,"-o-transition":"all "+this.settings.transitionDuration+"s "+this.settings.transitionEasing,"-ms-transition":"all "+this.settings.transitionDuration+"s "+this.settings.transitionEasing,transition:"all "+this.settings.transitionDuration+"s "+this.settings.transitionEasing};if(this.$element.prop("tagName").toUpperCase()!=="INPUT"){return}if(!this.settings.typeMatches.test(this.$element.attr("type"))){return}var r=this.$element.attr("id");if(!r){r=Math.floor(Math.random()*100)+1;this.$element.attr("id",r)}var i=this.$element.attr("placeholder");var s=this.$element.data("label");var o=this.$element.data("class");if(!o){o=""}if(!i||i===""){i="You forgot to add placeholder attribute!"}if(!s||s===""){s=i}this.inputPaddingTop=parseFloat(this.$element.css("padding-top"))+10;this.$element.wrap('<div class="floatlabel-wrapper" style="position:relative"></div>');this.$element.before('<label for="'+r+'" class="label-floatlabel '+this.settings.labelClass+" "+o+'">'+s+"</label>");this.$label=this.$element.prev("label");this.$label.css({position:"absolute",top:this.settings.labelStartTop,left:this.$element.css("padding-left"),display:"none","-moz-opacity":"0","-khtml-opacity":"0","-webkit-opacity":"0",opacity:"0"});if(!this.settings.slideInput){this.$element.css({"padding-top":this.inputPaddingTop})}this.$element.on("keyup blur change",function(t){e.checkValue(t)});t.setTimeout(function(){e.$label.css(n);e.$element.css(n)},100);this.checkValue()},checkValue:function(e){if(e){var t=e.keyCode||e.which;if(t===9){return}}var n=this.$element.data("flout");if(this.$element.val()!==""){this.$element.data("flout","1")}if(this.$element.val()===""){this.$element.data("flout","0")}if(this.$element.data("flout")==="1"&&n!=="1"){this.showLabel()}if(this.$element.data("flout")==="0"&&n!=="0"){this.hideLabel()}},showLabel:function(){var e=this;e.$label.css({display:"block"});t.setTimeout(function(){e.$label.css({top:e.settings.labelEndTop,"-moz-opacity":"1","-khtml-opacity":"1","-webkit-opacity":"1",opacity:"1"});if(e.settings.slideInput){e.$element.css({"padding-top":e.inputPaddingTop})}},50)},hideLabel:function(){var e=this;e.$label.css({top:e.settings.labelStartTop,"-moz-opacity":"0","-khtml-opacity":"0","-webkit-opacity":"0",opacity:"0"});if(e.settings.slideInput){e.$element.css({"padding-top":parseFloat(e.inputPaddingTop)-10})}t.setTimeout(function(){e.$label.css({display:"none"})},e.settings.transitionDuration*1e3)}};e.fn[i]=function(t){return this.each(function(){if(!e.data(this,"plugin_"+i)){e.data(this,"plugin_"+i,new o(this,t))}})}})(jQuery,window,document);

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT / GPLv2 License.
 */
(function(w){

  // This fix addresses an iOS bug, so return early if the UA claims it's something else.
  var ua = navigator.userAgent;
  if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && /OS [1-5]_[0-9_]* like Mac OS X/i.test(ua) && ua.indexOf( "AppleWebKit" ) > -1 ) ){
    return;
  }

  var doc = w.document;

  if( !doc.querySelector ){ return; }

  var meta = doc.querySelector( "meta[name=viewport]" ),
      initialContent = meta && meta.getAttribute( "content" ),
      disabledZoom = initialContent + ",maximum-scale=1",
      enabledZoom = initialContent + ",maximum-scale=10",
      enabled = true,
      x, y, z, aig;

  if( !meta ){ return; }

  function restoreZoom(){
    meta.setAttribute( "content", enabledZoom );
    enabled = true;
  }

  function disableZoom(){
    meta.setAttribute( "content", disabledZoom );
    enabled = false;
  }

  function checkTilt( e ){
    aig = e.accelerationIncludingGravity;
    x = Math.abs( aig.x );
    y = Math.abs( aig.y );
    z = Math.abs( aig.z );

    // If portrait orientation and in one of the danger zones
    if( (!w.orientation || w.orientation === 180) && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
      if( enabled ){
        disableZoom();
      }
    }
    else if( !enabled ){
      restoreZoom();
    }
  }

  w.addEventListener( "orientationchange", restoreZoom, false );
  w.addEventListener( "devicemotion", checkTilt, false );

})( this );