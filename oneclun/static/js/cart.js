angular.module('MainApp').factory('$cart', function($http, $timeout) {
  var Cart = function() {
    // selected tickets aka cart
    var tickets = {
      fallback: {}, // mapping date id -> ticket specs
      parser: {}
    };
    var ticketsAvailable = {
      fallback: {}, // mapping date id -> ticket specs
      parser: {}
    };

    var callbacks = {};
    this.onQueryFinished = function(callback) {
      if(callbacks.onQueryFinished === undefined) {
        callbacks.onQueryFinished = [];
      }
      callbacks.onQueryFinished.push(callback);
    }
    this.onSelectionChanged = function(callback) {
      if(callbacks.onSelectionChanged === undefined) {
        callbacks.onSelectionChanged = [];
      }
      callbacks.onSelectionChanged.push(callback);
    }
    var callCallbacks = function(key) {
      return function() {
        if(callbacks[key] === undefined) {
          return;
        }
        var args = arguments;
        callbacks[key].forEach(function(callback) {
          callback.apply(null, args);
        });
      }
    }

    var equals = {
      parser: function(otherTicket) {
        return this.full_title === otherTicket.full_title;
      }
    }

    var that = this;

    this.parserAddToCart = function(dateId, ticketSpec) {
      if(tickets.parser[dateId] === undefined) {
        tickets.parser[dateId] = [];
      }
      ticketSpec.equals = equals.parser;
      tickets.parser[dateId].push(ticketSpec);
      callCallbacks('onSelectionChanged')();
    }
    this.parserRemoveFromCart = function(dateId, ticketSpec) {
      toBeSpliced = undefined;
      tickets.parser[dateId].reduce(function(cont, ticket, i) {
        if(cont === false) {
          return false;
        }
        if(ticket.equals(ticketSpec)) {
          toBeSpliced = i;
          return false;
        }
        return true;
      }, true);
      if(toBeSpliced !== undefined) {
        tickets.parser[dateId].splice(toBeSpliced, 1);
      }
      callCallbacks('onSelectionChanged')();
    }
    this.fallbackUpdateCart = function(dateId, ticketSpec, desiredCount) {
      var override = function(ticketSpec) {
        var toBeSpliced = undefined;
        if(tickets.fallback[dateId] !== undefined) {
          tickets.fallback[dateId].forEach(function(otherTicketSpec, i) {
            if(otherTicketSpec.name === ticketSpec.name) {
              toBeSpliced = i;
            }
          });
        }
        if(toBeSpliced !== undefined) {
          tickets.fallback[dateId].splice(toBeSpliced, 1);
          if(tickets.fallback[dateId].length === 0) {
            delete tickets.fallback[dateId];
          }
        }
      }
      if(ticketSpec.price === undefined) {
        ticketSpec.price = 0;
      }
      if(desiredCount === 0) {
        if(tickets.fallback[dateId] !== undefined) {
          override(ticketSpec);
        }
      }else {
        override(ticketSpec);
        if(tickets.fallback[dateId] === undefined) {
          tickets.fallback[dateId] = [];
        }
        ticketSpec.desired_count = desiredCount
        tickets.fallback[dateId].push(ticketSpec);
      }
      callCallbacks('onSelectionChanged')();
    }

    this.getTotals = function(dateId) {
      // getting price for all selected tickets
      var totals = 0,
          abnormal;
      Object.keys(tickets.parser).forEach(function(key) {
        tickets.parser[key].forEach(function(ticketSpec) {
          totals += parseInt(ticketSpec.price, 10);
        });
      });
      Object.keys(tickets.fallback).forEach(function(key) {
        tickets.fallback[key].forEach(function(ticketSpec) {
          var tmp = parseInt(ticketSpec.price * ticketSpec.desired_count, 10);
          abnormal = isNaN(tmp) ? ticketSpec.price : undefined;
          totals += tmp || 0;
        });
      });
      return totals <= 0 ? abnormal : totals;
    }
    this.fallbackForEach = function(callback) {
      tickets.fallback.forEach(callback);
    }
    this.parserForEach = function(callback) {
      tickets.parser.forEach(callback);
    }
    this.getSelectedTickets = function(dateId) {
      return {
        parser: tickets.parser[dateId],
        fallback: tickets.fallback[dateId]
      };
    }
    var loadingTickets = undefined;
    this.queryTickets = function(dateId, date, venueId) {
      loadingTickets = true;
      var queryParser = function(sectorsSeq, sectorsByName) {
        var url = window.PARSER_API_ENDPOINT + "get_tickets/" + dateId + "/?date=" + date + "&margin=[]";
        $http.get(url).success(function(data, status, headers, config) {
          var status = headers()["x-status"];
          if(status === 'pending') {
            $timeout(function() { return queryParser(sectorsSeq, sectorsByName); }, 1000);
          }else if(status === 'ok') {
            loadingTickets = false;
            ticketsAvailable.parser[dateId] = [];
            data.forEach(function(group) {
              var gTitle = transliterate(group.title);
              if (gTitle == 'S1' || gTitle == 'Silve') {gTitle = 'A5'} 
              var sector = sectorsByName[gTitle];
              if(sector === undefined) {console.log(sector, gTitle, sectorsByName); return;}
              if(group.subcats === undefined) {
                ticketsAvailable.parser[dateId].push({
                  id: group.id,
                  sys_name: transliterate(sector.sys_name),
                  sector_id: sector._id,
                  title: transliterate(group.title),
                  full_title: group.title,
                  price: parseInt(group.offer_id, 10) != 0 ? margin(parseInt(group.price.min, 10)) : parseInt(group.price.min, 10),
                  count: parseInt(group.count, 10),
                  offer_id: group.hasOwnProperty('offer_id') ? group.offer_id : NaN
                });
              }else {
                group.subcats.forEach(function(subcat) {
                  subcat.places.forEach(function(place) {
                    if (group.title.indexOf('Silve') + 1 == 1) {
                      group.title = 'A5 Silver' + group.title.substring(5);
                    }
                    if (group.title.indexOf('S1') + 1) {
                      group.title = 'A5 Silver' + group.title.substring(2);
                    }
                    var row = parseInt(subcat.title.split(' ')[1], 10);
                    var seat = parseInt(place.title.split(' ')[1], 10);
                    ticketsAvailable.parser[dateId].push({
                      id: place.id,
                      row: row,
                      seat: seat,
                      sys_name: transliterate(sector.sys_name),
                      sector_id: sector._id,
                      title: transliterate(group.title),
                      full_title: group.title + ', ' + subcat.title + ', ' + place.title,
                      price: parseInt(place.offer_id, 10) != 0 ? margin(parseInt(place.price, 10)) : parseInt(place.price, 10),
                      offer_id: place.hasOwnProperty('offer_id') ? place.offer_id : NaN
                    });
                  });
                });
              }
            });
            callCallbacks('onQueryFinished')(sectorsSeq);
          }else if(status === "error") {
            callCallbacks('onQueryFinished')(sectorsSeq);
          }
        }).error(function(data, status, headers, config) {
          loadingTickets = false;
          callCallbacks('onQueryFinished')(sectorsSeq);
        });
      }
      var query = function() {
        var sectorsSeq = JSON.parse($("#sectors").html());
        // $http.get(window.EVENTS_API_ENDPOINT + "sectors/?venue=" + venueId + "&mark_as=" + window.MARK_AS).success(function(sectorsSeq) {
          var sectorsByName = {};
          sectorsSeq.forEach(function(sector) {
            sector.name = transliterate(sector.name);
            sector.sys_name = transliterate(sector.sys_name);
            sectorsByName[sector.name] = sector;
          });
          queryParser(sectorsSeq, sectorsByName);
        // });
      }
      query();
    }
    this.ticketsStillLoading = function() {
      if(that.loadingTickets === false) {
        return false;
      }else {
        return true;
      }
    }
    this.getAvailableTickets = function(dateId) {
      if(dateId === undefined) {
        return ticketsAvailable;
      }else {
        return {
          fallback: ticketsAvailable.fallback[dateId] || [],
          parser: ticketsAvailable.parser[dateId] || []
        }
      }
    }
    // this.serialize = function() {
    //   Object.keys(tickets.parser).forEach(function(key) {
    //     var i = 0;
    //     while(i < tickets.parser[key].length) {
    //       delete tickets.parser[key][i].id;
    //       delete tickets.parser[key][i].sys_name;
    //       i += 1;
    //     }
    //   });
    //   Object.keys(tickets.fallback).forEach(function(key) {
    //     var i = 0;
    //     while(i < tickets.fallback[key].length) {
    //       delete tickets.fallback[key][i]["$$hashKey"];
    //       i += 1;
    //     }
    //   });
    //   return JSON.stringify(tickets);
    // }
    this.hasSelection = function(dateId) {
      if(dateId === undefined) {
        if(Object.keys(tickets.fallback) || Object.keys(tickets.parser)) {
          return true;
        }else {
          return false;
        }
      }else {
        var fallback = tickets.fallback[dateId] && tickets.fallback[dateId].length;
        var parser = tickets.parser[dateId] && tickets.parser[dateId].length;
        if(fallback || parser) {
          return true;
        }else {
          return false;
        }
      }
    }
  }
  return new Cart();
});