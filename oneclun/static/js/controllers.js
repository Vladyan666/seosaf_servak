angular.module('MainApp').controller('ChooseTicketsController', function($scope, $http, $timeout,
 $cart, $state, $window) {
    $scope.venueId = $('body').data('venue-id');    // always a string
    $scope.date = $('body').data('date');    // %Y-%m-%d-%H:%M
    $scope.eventId = $('body').data('event-id');
    $scope.fallbackPricesOn = $('body').data('fallback-prices-on') === "true";
    if($scope.eventId === "" || $scope.eventId === undefined) {
      $scope.fallbackPricesOn = true;
    }
    $scope.totalPrice = 0;
    $scope.totalTickets = 0;
    $scope.totalTicketsFormat = 0;
    $scope.ticketsDoneLoading = false;
    $scope.someTicketsSelected = false;
    $scope.tickets = [];
    $scope.serializedSelection = '[]';
    $scope.sectors = [];
    $scope.selectedSector = undefined;
    $scope.touchscreen = !!navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i);
    window.isTouchscreen = $scope.touchscreen;
    var update_tickets = function(){ return $scope.$broadcast('updateTickets', $scope.tickets); };
    var preloadTrigger = function(){ return $(window).trigger('proceed')}
    $scope.formatTicketsPrice = function (price){ return isNaN(parseInt(price)) ? price : price + ' Р'; };
    if($scope.fallbackPricesOn === true) {
      // $state.go("noparser");         // doesnt work, god knows why
      $window.location.href = $window.location.href.split("#")[0] + "#/select-noparser";
    }else {
      $cart.onQueryFinished(function(sectors) {
        $timeout(function() {
          $scope.$apply(function() {
            $('#preloader').remove();
            $scope.ticketsDoneLoading = true;
            $scope.tickets = $cart.getAvailableTickets($scope.eventId).parser;
            $scope.sectors = sectors;
            $scope.$broadcast('updateSectors', $scope.sectors);
            $scope.$on('sectorsRendered', function(e){
                update_tickets();
            });
            update_tickets();
          });
        }, 10);
      });
      $cart.queryTickets($scope.eventId, $scope.date, $scope.venueId);
    }
    $scope.proceed = function() {
      return function(sectorId) {
        preloadTrigger();
        $scope.sectors.forEach(function(sector) {
          if(sector._id === sectorId) {
            $scope.selectedSector = sector;
          }
        });
        $scope.selectSeatPass = true;         // telling 'select-seat' not to reload the page
        $state.go("select-seat", {sector_oid: $scope.selectedSector._id});
        $scope.$on('sectorsRendered', function(e){
            update_tickets();
        });
        update_tickets();
      }
    }
    $scope.getSchema = function() {
      return $scope.selectedSector.schema;
    }
});

angular.module('MainApp').controller('SectorController', function($scope, $cart, $state, $timeout, $window) {
});

angular.module('MainApp').controller('SeatsController', function($scope, $cart, $state, $timeout, $window) {
  $scope.eventId = $('body').data('event-id');
  $scope.goBack = function() {
    $state.go("select-sector");
  }
  if(!$scope.$parent.selectSeatPass) {          // the user has just reloaded the page; no data to show
    $scope.selectedSector = {schema: ""};
    $scope.goBack();
  }
  $scope.tickets = $cart.getAvailableTickets($scope.eventId).parser;
  $cart.onSelectionChanged(function() {
    $timeout(function() {
      $scope.$apply(function() {
        $scope.totalPrice = $cart.getTotals($scope.eventId);
        var totalTickets = 0;
        if ($cart.getSelectedTickets($scope.eventId).parser) {
          for (var i=0; i<$cart.getSelectedTickets($scope.eventId).parser.length; i++) {
            totalTickets += 1;
          }
        }
        // console.log($cart.getSelectedTickets($scope.eventId), totalTickets);
        $scope.totalTickets = totalTickets;
        if (totalTickets == 1) {totalTickets += " билет"}
        else if (totalTickets < 5) {totalTickets += " билета"}
        else {totalTickets += " билетов"}
        $scope.totalTicketsFormat = totalTickets;
        $scope.someTicketsSelected = $cart.hasSelection($scope.eventId);
        if ($cart.hasSelection($scope.eventId)) {
          $(window).trigger('ticketsSelected');
        } else {
          $(window).trigger('ticketsNotSelected');
        }
        if(parseInt(totalTickets) > 1) {
          $(window).trigger('changeTotalTickets');
        }
      });
      $('.slab').fitText();
    }, 0);
  });
  $scope.proceedToPersonalData = function() {
    $state.go("personal-data");
  }
});

angular.module('MainApp').controller('PersonalDataController', function($scope, $rootScope, $filter, $cart, $http, $state, $timeout) {
    maskInput();
    $scope.eventId = $('body').data('event-id');
    $scope.eventRepr = $('body').data('event-repr');
    $scope.date = $('body').data('date-iso');
    console.log($filter('date')(new Date($scope.date), 'yyyy-MM-dd-HH:mm'));
    $scope.totalPrice = $cart.getTotals($scope.eventId);
    $scope.tickets = $cart.getSelectedTickets($scope.eventId);
    $scope.model = {
        name: "",
        phone: "",
        email: "",
        address: "",
        notes: "",
        paymentMethod: "1"
    };
    $scope.paymentMethods = [];
    $http.get(window.EVENTS_API_ENDPOINT + "payment_methods/?mark_as=global").success(function(paymentMethods) {
        $timeout(function() {
          $scope.$apply(function() {
            $scope.paymentMethods = paymentMethods;
          });
        });
    });
    $scope.errors = {};
    $scope.clientValidateData = {};
    $scope.submit = function () {
        $scope.validateName();
        $scope.validatePhone();
        //$scope.validateEmail();
        if (Object.keys($scope.clientValidateData).length > 0) {
            $scope.errors = $scope.clientValidateData;
            return;
        }
        var tickets = [],
            reservation = [],
            info = [];
        if ($scope.tickets.parser !== undefined) {
            $scope.tickets.parser.forEach(function (ticket) {
                var offer_id = ticket.hasOwnProperty('offer_id') ? ticket.offer_id : undefined;
                if (offer_id){
                    reservation.push('1:' + ticket.offer_id);
                    info.push(
                        ticket.offer_id.split(":")[0]
                        + ':'
                        + [
                            ticket.title,
                            'Ряд ' + ticket.row,
                            'Место ' + ticket.seat
                        ].join(', ')
                    );
                }
                tickets.push({
                    event: $scope.eventRepr,
                    title: ticket.full_title,
                    date: $scope.date,
                    tickets_count: 1,
                    sum: parseInt(ticket.price, 10),
                    category: ticket.title
                });
            });
        }
        if ($scope.tickets.fallback !== undefined) {
            $scope.tickets.fallback.forEach(function (ticket) {
                tickets.push({
                    event: $scope.eventRepr,
                    title: ticket.name,
                    date: $scope.date,
                    tickets_count: ticket.desired_count,
                    sum: parseInt(ticket.desired_count, 10) * parseInt(ticket.price, 10) || 0,
                    category: ticket.name
                });
            });
        }
        $http.post(window.CRM_API_ENDPOINT + "place_order.php", {
            name: $scope.model.name,
            phone: $scope.model.phone,
            email: $scope.model.email,
            address: $scope.model.address,
            notes: $scope.model.notes,
            payment_method: $scope.model.paymentMethod,
            delivery_method: "1",
            language: "0",
            tickets: tickets
        }).success(function (data) {
            $rootScope.orderId = data.order_id;
            $rootScope.orderBy = $scope.model.name;
            var new_reservation = [],
                new_info = [];
            angular.forEach(reservation, function(val){this.push(val.split(',').join(""))}, new_reservation);
            angular.forEach(info, function(val){this.push(val.split(',').join(""))}, new_info);
            if (new_reservation && new_info){
                $http({
                    method: 'POST',
                    url: window.PARSER_API_ENDPOINT.replace('/fe/v01/', '/'),
                    data: $.param({
                        'event': $scope.eventId,
                        'date': $filter('date')(new Date($scope.date), 'yyyy-MM-dd-HH:mm'),
                        'reservations': new_reservation.join(),
                        'add_info': new_info.join(),
                        'order_id': $rootScope.orderId
                    }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    $state.go("finish");
                });
            } else { $state.go("finish"); }
        });
    };
    $scope.validatePhone = function () {
        if($scope.model.phone == ""){
            $scope.clientValidateData['phone'] = "Телефон обязателен для заполнения";
            return false;
        }

        angular.lowercase($scope.model.phone);
        var phoneNumber = [];
        var digitsCount = 0;
        angular.forEach($scope.model.phone, function(value, key){
            if(!isNaN(parseInt(value, 10)) || value == "-" || value == "+" || value == " ")
                this.push(value);
            if(!isNaN(parseInt(value, 10)))
                digitsCount++;
        }, phoneNumber);

        if( (phoneNumber.length === $scope.model.phone.length) && (digitsCount >= 5 && digitsCount <= 18) ){
            delete $scope.clientValidateData['phone'];
            return true;
        } else {
            $scope.clientValidateData['phone'] = "Введен неверный телефон";
            return false;
        }
    };
    $scope.validateName = function () {
        if($scope.model.name == ""){
            $scope.clientValidateData['name'] = "Имя обязательно для заполнения";
            return false;
        } else {
            delete $scope.clientValidateData['name'];
            return true;
        }
    };
    $scope.validateEmail = function () {
        if($scope.model.email !== ""){
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if(!re.test($scope.model.email)){
                $scope.clientValidateData['email'] = "Введен неверный email";
                return false;
            } else {
                delete $scope.clientValidateData['email'];
                return true;
            }
        } else {
            return true; //not required field
        }
    };
});

angular.module('MainApp').controller('FinishController', function($scope, $rootScope) {
  $scope.orderId = $rootScope.orderId;
  $scope.orderBy = $rootScope.orderBy;
});

angular.module('MainApp').controller('CanSelectSectorController', function($scope) {
    $scope.selectedSector = undefined;
    $scope.selectSector = function(sectorId) {
      $scope.$apply(function() {
        $scope.selectedSector = sectorId;
      });
    }
});

angular.module('MainApp').controller('NoParserController', function($scope, $timeout, $http, $cart, $state, $rootScope) {
  $scope.eventId = $('body').data('event-id');
  $scope.eventOId = $('body').data('event');
  $scope.ticketSpecsBySector = {};
  $rootScope.throughNoparser = true;
  $http.get(window.EVENTS_API_ENDPOINT + "prices/?event=" + $scope.eventOId + "&mark_as=" + window.MARK_AS).success(function(prices) {
    $timeout(function() {
      $scope.$apply(function() {
        $scope.prices = prices;
        prices.sort(function(a, b) {
            return parseInt(a.price, 10) - parseInt(b.price, 10);
        })
        prices.forEach(function(price) {
          $scope.ticketSpecsBySector[price.sector] = { // this interface is not described anywhere, which is a pity
            name: price.sector,
            price: price.price,
            availability: price.availability
          };
        });
      });
    });
  });
  $scope.orders = {};
  $cart.onSelectionChanged(function() {
    $timeout(function() {
      $scope.$apply(function() {
        $scope.totalPrice = $cart.getTotals($scope.eventId);
        var totalTickets = 0;
        if ($cart.getSelectedTickets($scope.eventId).fallback) {
          for (var i=0; i<$cart.getSelectedTickets($scope.eventId).fallback.length; i++) {
            totalTickets += $cart.getSelectedTickets($scope.eventId).fallback[i].desired_count;
          }
        }
        //console.log($cart.getSelectedTickets($scope.eventId));
        $scope.totalTickets = totalTickets;
        if (totalTickets == 1) {totalTickets += " билет"}
        else if (totalTickets < 5) {totalTickets += " билета"}
        else {totalTickets += " билетов"}
        $scope.totalTicketsFormat = totalTickets;
        $scope.someTicketsSelected = $cart.hasSelection($scope.eventId);
        if ($cart.hasSelection($scope.eventId)) {
          $(window).trigger('ticketsSelected');
        } else {
          $(window).trigger('ticketsNotSelected');
        }
        if(parseInt(totalTickets) > 1) {
          $(window).trigger('changeTotalTickets');
        }
      });
      $('.slab').fitText();
    }, 0);
  });
  // the inputs (values in $scope.orders) came to be explicitly stored as strings somehow
  // maybe for the better (we still need empty input ie "", right?)
  $scope.increment = function(sector) {
    if($scope.ticketSpecsBySector[sector] !== undefined) {
      if($scope.orders[sector] === undefined) {
        $scope.orders[sector] = "1";
      }else if($scope.ticketSpecsBySector[sector].availability > parseInt($scope.orders[sector], 10)) {
        $scope.orders[sector] = (parseInt($scope.orders[sector], 10) + 1).toString();
      }
    }
  }
  $scope.decrement = function(sector) {
    if($scope.orders[sector] !== undefined && parseInt($scope.orders[sector], 10) > 0) {
      $scope.orders[sector] = (parseInt($scope.orders[sector], 10) - 1).toString();
    }
  }
  $scope.$watchCollection("orders", function() {
    Object.keys($scope.orders).forEach(function(sector) {
      if($scope.orders[sector] && isNaN(parseInt($scope.orders[sector], 10))) {
        delete $scope.orders[sector];
      }
      if($scope.orders[sector] && parseInt($scope.orders[sector], 10).toString() !== $scope.orders[sector]) {
        delete $scope.orders[sector];
      }
      if(parseInt($scope.orders[sector], 10) > $scope.ticketSpecsBySector[sector].availability) {
        $scope.orders[sector] = $scope.ticketSpecsBySector[sector].availability;
      }
      if(parseInt($scope.orders[sector], 10) < 0) {
        $scope.orders[sector] = 0;
      }
      if($scope.orders[sector] !== undefined) {
        if($scope.orders[sector] === "") {
          $cart.fallbackUpdateCart($scope.eventId, $scope.ticketSpecsBySector[sector], 0);
        }else {
          $cart.fallbackUpdateCart($scope.eventId, $scope.ticketSpecsBySector[sector], parseInt($scope.orders[sector], 10));
        }
      }
    });
  });
  $scope.proceedToPersonalData = function() {
    $state.go("personal-data");
  }
});

angular.module('MainApp').controller('GroupOrderController', function($scope, $http) {
  $scope.model = {
    name: '',
    email: '',
    phone: '',
    // count: '',
    description: ''
  };
  $scope.clientValidateData = {};
  $scope.errors = {};
  $scope.order = undefined;
    $scope.submitOrder = function () {
        if($scope.validateInput()){
            $http.post(window.CRM_API_ENDPOINT + 'place_call_order.php', {
                name: $scope.model.name,
                email: $scope.model.email,
                phone: $scope.model.phone,
                address: "",
                notes: $scope.model.description,
                language: "0",
                tickets_count: "0",//$scope.model.count,
                date: ""
            }).success(function (data, status, headers, config) {
                $scope.errors = {};
                $scope.order = {name: $scope.model.name, bin_id: data.order_id};
                $('#grouporder-modal').modal('show');
            });
        } else {
            $scope.errors = $scope.clientValidateData;
        }
    };
    $scope.validatePhone = function () {
        if($scope.model.phone == ""){
            $scope.clientValidateData['phone'] = "Телефон обязателен для заполнения";
            return false;
        }

        angular.lowercase($scope.model.phone);
        var phoneNumber = [];
        var digitsCount = 0;
        angular.forEach($scope.model.phone, function(value, key){
            if(!isNaN(parseInt(value, 10)) || value == "-" || value == "+" || value == " ")
                this.push(value);
            if(!isNaN(parseInt(value, 10)))
                digitsCount++;
        }, phoneNumber);

        if( (phoneNumber.length === $scope.model.phone.length) && (digitsCount >= 5 && digitsCount <= 18) ){
            delete $scope.clientValidateData['phone'];
            return true;
        } else {
            $scope.clientValidateData['phone'] = "Введен неверный телефон";
            return false;
        }
    };
    $scope.validateName = function () {
        if($scope.model.name == ""){
            $scope.clientValidateData['name'] = "Имя обязательно для заполнения";
            return false;
        } else {
            delete $scope.clientValidateData['name'];
            return true;
        }
    };
    $scope.validateEmail = function () {
        if($scope.model.email !== ""){
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if(!re.test($scope.model.email)){
                $scope.clientValidateData['email'] = "Введен неверный email";
                return false;
            } else {
                delete $scope.clientValidateData['email'];
                return true;
            }
        } else {
            return true; //not required field
        }
    };
    $scope.validateInput = function () {
        var nameValidationResult = $scope.validateName();
        var phoneValidationResult = $scope.validatePhone();
        var emailValidationResult = $scope.validateEmail();
        return (nameValidationResult && phoneValidationResult && emailValidationResult);
    };
    $scope.dismiss = function () {
        $('#grouporder-modal').modal('hide');
        $scope.model = {
            name: '',
            email: '',
            phone: '',
            // count: '',
            description: ''
        };
        $scope.errors = {};
        $scope.order = undefined;
    };
});

angular.module('MainApp').controller('OrderCallController', function($scope, $http) {
  $scope.model = {
    name: '',
    phone: '',
    description: ''
  };
  $scope.clientValidateData = {};
  $scope.errors = {};
  $scope.order = undefined;
    $scope.submitOrder = function () {
        if($scope.validateInput()){
            $http.post(window.CRM_API_ENDPOINT + 'place_call_order.php', {
                name: $scope.model.name,
                phone: $scope.model.phone,
                notes: $scope.model.description,
                language: "0"
            }).success(function (data, status, headers, config) {
                $scope.errors = {};
                $scope.order = data.order;
                $scope.dismiss();
            });
        } else {
            $scope.errors = $scope.clientValidateData;
        }
    };
    $scope.validatePhone = function () {
        if($scope.model.phone == ""){
            $scope.clientValidateData['phone'] = "Телефон обязателен для заполнения";
            return false;
        }

        angular.lowercase($scope.model.phone);
        var phoneNumber = [];
        var digitsCount = 0;
        angular.forEach($scope.model.phone, function(value, key){
            if(!isNaN(parseInt(value, 10)) || value == "-" || value == "+" || value == " ")
                this.push(value);
            if(!isNaN(parseInt(value, 10)))
                digitsCount++;
        }, phoneNumber);

        if( (phoneNumber.length === $scope.model.phone.length) && (digitsCount >= 5 && digitsCount <= 18) ){
            delete $scope.clientValidateData['phone'];
            return true;
        } else {
            $scope.clientValidateData['phone'] = "Введен неверный телефон";
            return false;
        }
    };
    $scope.validateName = function () {
        if($scope.model.name == ""){
            $scope.clientValidateData['name'] = "Имя обязательно для заполнения";
            return false;
        } else {
            delete $scope.clientValidateData['name'];
            return true;
        }
    };
    $scope.validateInput = function () {
        var nameValidationResult = $scope.validateName();
        var phoneValidationResult = $scope.validatePhone();
        return (nameValidationResult && phoneValidationResult);
    };
    $scope.dismiss = function () {
        $('#callme').modal('hide');
        $scope.order = undefined;
        $scope.errors = {};
        $scope.model = {
            name: '',
            phone: '',
            description: ''
        };
    };
});