angular.module('MainApp', ['SchemeApp', 'ui.router'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

angular.module('MainApp').run(function($rootScope) {
  $rootScope.touchscreen = 'ontouchstart' in document.documentElement;
  // $rootScope.touchscreen = true;
  $rootScope.touchscreen = false;
  if($rootScope.touchscreen) {
    FastClick.attach(document.body);
  }
});

angular.module('MainApp').config(function($locationProvider) {
  $locationProvider.html5Mode(false);     // hashes in urls
});

function maskInput() {
  $(".js-phone").maskinput(/^\+?[0-9]{6,16}$/gi);
  $(".js-name").maskinput(/^[^0-9!@#$%^&)(+=_]+$/gi);
  //$(".js-email").maskinput(/^[A-z0-9.@_-]+$/gi);
}

angular.module('MainApp').run(function() {
  maskInput();
});

angular.module('MainApp').config(function($stateProvider, $urlRouterProvider) {  
  $urlRouterProvider.otherwise("/select-sector");

  var oid = location.pathname.split("/")[2];

  $stateProvider.state('select-sector', {
    url: "/select-sector",
    templateUrl: function() {
      return "/partials/" + oid + "/select_sector.html";
    },
    contolller: "SectorController"
  });

  $stateProvider.state('select-seat', {
    url: "/select-seat/:sector_oid",
    templateUrl: function(params) {
      return "/partials/" + oid + "/" + params.sector_oid + "/select_seat.html";
    },
    controller: "SeatsController"
  });

  $stateProvider.state('personal-data', {
    url: "/personal-data",
    templateUrl: function() {
      return "/partials/" + oid + "/personal_data.html";
    },
    controller: "PersonalDataController"
  });

  $stateProvider.state('finish', {
    url: "/finish",
    templateUrl: function() {
      return "/partials/finish.html";
    },
    controller: "FinishController"
  });

  $stateProvider.state('noparser', {
    url: "/select-noparser",
    templateUrl: function() {
      return "/partials/" + oid + "/noparser.html";
    },
    controller: "NoParserController"
  });
});

angular.module('MainApp').run(function($rootScope, $window){
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    // force-reloads
    var fromSeatsToSectors = toState.name === "select-sector" && fromState.name === "select-seat";
    var fromPersonalDataToSeats = toState.name === "select-seat" && fromState.name === "personal-data";
    var fromPersonalDataToNoparser = toState.name === "noparser" && fromState.name === "personal-data";
    var fromFinishToPersonalData = toState.name === "personal-data" && fromState.name === "finish";
    var uncomfortableCombination = fromSeatsToSectors || fromPersonalDataToSeats || fromPersonalDataToNoparser || fromFinishToPersonalData;
    if(uncomfortableCombination) {
      // generating random string to force the page to reload
      // there has to be a less hacky way to do this
      var randomSeed = "";
      var i = 0;
      while(i < 5) {
        randomSeed += Math.floor(Math.random() * 24).toString(16);
        i ++;
      }
      window.location.href = $window.location.href.split("#")[0] + "?" + randomSeed + "#/select-sector";
    }
  });
});

// $(window).on('resize', function(){
//     // console.log('resize');
//     var self = $(this),
//         scheme = $('#scheme svg');
//     if (self.width() > 1000 && self.width() < start_scheme_width) {
//        var propotion = self.width()/start_scheme_width;
//        scheme.attr('width', self.width()+'px');
//        scheme.attr('height', start_scheme_height*propotion+'px');
//     }
//     // получаем номинальный размер схемы
//     var viewbox = $('#scheme svg')[0].getAttribute('viewBox').split(' ');       // jquery .attr почему-то не работает
//     var x = parseInt(viewbox[0]),
//         y = parseInt(viewbox[1]),
//         width = parseInt(viewbox[2]),
//         height = parseInt(viewbox[3]);
//     // width = width - x;
//     // height = height - y;
//     // вычисляем пропорцию
//     scheme.css('height', self.width() * height / width + 'px');
//     if($('#scheme svg').width() > $(window).width()) {       // некоторые браузеры выпростывают схему за пределы вьюпорта
//         scheme.css('width', $(window).width() - 20 + 'px');
//     }
// }).trigger('resize');


function transliterate(word) {
    var A = {};
    var result = '';
    A['А'] = 'A';
    A['С'] = 'C';
    A['В'] = 'B';
    for(var i = 0; i < word.length; i++) {
        var c = word.charAt(i);
        result += A[c] || c;
    }
    return result;
}

function margin(price){
    var margins = window['margins'] ? window['margins'] : [], _tmp_price;
    price = parseInt(price);
    for (var idx in margins){
        if (price >= margins[idx][0]) { _tmp_price = price + margins[idx][1]; }
    }
    return _tmp_price ? _tmp_price : price;
}
