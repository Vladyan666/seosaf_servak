# -*- coding: utf8 -*-

# определение окружения
import os, sys, re, locale, codecs , argparse , json , django
sys.path.append('/home/pweb/seo/fifa2018/')
sys.path.append('/home/pweb/seo/fifa2018/fifa2018/')
os.environ["DJANGO_SETTINGS_MODULE"] = "fifa2018.settings"
app_name = "football"
django.setup()

import umodels, settings

# логирование
def result_log( test ):
    print test
    return test+"<br>"

# создание парсера командной строки
def create_parser ():
    parser = argparse.ArgumentParser()
    parser.add_argument('type', nargs='?')
    parser.add_argument ('-export', action='store_const', const=True)
 
    return parser
    
# убираем спецсимволы и числа из названия поля   
def get_field_without_somechars( field ):
    return re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',field).strip()

# задаем объект
def set_object( object_name , json_data ):
    object_name = get_field_without_somechars(object_name) # убираем спецсимволы и числа из имени объекта 
    object_name_U = object_name.capitalize() # и делаем первую букву заглавной
    # импортируем объект
    if object_name_U == 'Textpage':
        object_name_U = 'TextPage'
    exec("""from """+app_name+""".models import """+object_name_U)

    object = eval("umodels.set_"+object_name+"(json_data,"+object_name_U+")")
    print object
    return object
    
# получаем объект
def get_object( object_name , json_data ):
    object_name = get_field_without_somechars(object_name) # убираем спецсимволы и числа из имени объекта 
    object_name_U = object_name.capitalize() # и делаем первую букву заглавной
    # импортируем объект
    if object_name_U == 'Textpage':
        object_name_U = 'TextPage'
    exec("""from football.models import """+object_name_U)

    object = eval("umodels.get_"+object_name+"(json_data,"+object_name_U+")")
    print object
    return object
    
# перезаписываем файл   
def rewrite_file( rewrite , object_name ):
    data = open(os.path.join(settings.BASE_DIR, 'scripts')+'/pr/'+object_name+'-export.pr' , 'w')
    data.write(rewrite)
    data.close()
        
#основные операции
def drive( object_name , export = False ):

    log = ""
    
    data = open(os.path.join(settings.BASE_DIR, 'scripts')+'/pr/'+object_name+'.pr' , 'r')  # открытие файла
    go = False                            # начало записи в базу
    change = False                        # наличие изменений в файле
    rewrite = ''                          # перезаписанный файл
    num_all = 0                           # счетчик
    num_add = 0                           # счетчик
    num_up = 0                            # счетчик
    num_error = 0                         # счетчик
    
    for line in data :  
        # записываем в базу
        if go :
            if line.strip() == '': # пропуск пустых строк
                continue
              
            json_object = json.loads(line) # декодирование строки
            
            if object_name == 'other':
                object_name = json_object['type']
            
            #выбираем модель записи данных
            if export :
                object = get_object( object_name , json_object )
            else :
                object = set_object( object_name , json_object )
            
            # статистика
            num_all += 1
            if object :
                log += str(object).replace('<', '&lt;').replace('<', '&gt;')+"<br>"
                if object[1] :
                    num_add += 1
                else :
                    num_up += 1
            else :
                num_error += 1
                log += 'Error line: ' + line + '<br>'
            
            if export :
                rewrite += object + '\n'
            else :
                rewrite += line  
            change = True 
        # не записываем
        else :
            # если встречаем break начинаем запись
            if line.strip() == 'break' :
                go = True
                
            else :
                rewrite += line     

    # если были изменения в файле делаем перенос строки        
    if change :
        rewrite += '\nbreak'
    else :
        rewrite += 'break'
        
    data.close()

    # перезаписываем файл
    if export :
        rewrite_file( rewrite , object_name )
    
    log += result_log("End") 
    log += result_log("All objects: "+str(num_all)) 
    log += result_log("Add objects: "+str(num_add)) 
    log += result_log("Update objects: "+str(num_up)) 
    log += result_log("Error objects: "+str(num_error)) 
    
    return log
    
def init( object_name ):
    log = ""
    
    log += result_log("Start") 
    
    # разбор аргументов коммандной строки
    parser = create_parser()
    namespace = parser.parse_args()
    
    # если передан аргумент переопределяем тип данных
    if namespace.type :
        object_name = namespace.type
    
    # export
    export = False
    if namespace.export :
        export = True    
    
    log += drive( object_name , export )  
    
    return log
    

if __name__ == '__main__':
    object_name = 'textpage'
    init( object_name )  