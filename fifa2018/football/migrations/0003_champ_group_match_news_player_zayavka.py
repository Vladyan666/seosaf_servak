# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-16 14:22
from __future__ import unicode_literals

import datetime
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('football', '0002_fed_stadium_team'),
    ]

    operations = [
        migrations.CreateModel(
            name='Champ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(blank=True, null=True, upload_to=b'', verbose_name='\u041b\u043e\u0433\u043e')),
                ('year', models.IntegerField(blank=True, null=True, verbose_name='\u0413\u043e\u0434 \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u044f')),
                ('web', models.TextField(blank=True, null=True, verbose_name='\u0422\u0443\u0440\u043d\u0438\u0440\u043d\u0430\u044f \u0441\u0435\u0442\u043a\u0430')),
                ('fed', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Fed', verbose_name='\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u0430')),
                ('teams', models.ManyToManyField(blank=True, to='football.Team', verbose_name='\u041a\u043e\u043c\u0430\u043d\u0434\u044b \u0432 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u0435')),
            ],
            options={
                'verbose_name': '\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442',
                'verbose_name_plural': '\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('qualifying', models.BooleanField(default=False, verbose_name='\u041e\u0442\u0431\u0440\u043e\u0447\u043d\u0430\u044f \u0433\u0440\u0443\u043f\u043f\u0430')),
                ('champ', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Champ', verbose_name='\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442')),
                ('teams', models.ManyToManyField(blank=True, to='football.Team', verbose_name='\u041a\u043e\u043c\u0430\u043d\u0434\u044b \u0432 \u0433\u0440\u0443\u043f\u043f\u0435')),
            ],
            options={
                'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u044b',
            },
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('datetime', models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u043d\u0430\u0447\u0430\u043b\u0430 \u043c\u0430\u0442\u0447\u0430')),
                ('tour', models.CharField(blank=True, max_length=100, null=True, verbose_name='\u0422\u0443\u0440')),
                ('status', models.CharField(blank=True, max_length=100, null=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043c\u0430\u0442\u0447\u0430')),
                ('goals', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0413\u043e\u043b\u044b')),
                ('foals', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u041d\u0430\u0440\u0443\u0448\u0435\u043d\u0438\u044f')),
                ('people', models.IntegerField(blank=True, null=True, verbose_name='\u041f\u043e\u0441\u0435\u0449\u0430\u0435\u043c\u043e\u0441\u0442\u044c')),
                ('penalty', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u041d\u0430\u043a\u0430\u0437\u0430\u043d\u0438\u044f')),
                ('osnova1', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u041e\u0441\u043d\u043e\u0432\u043d\u043e\u0439 \u0441\u043e\u0441\u0442\u0430\u0432 1 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('osnova2', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u041e\u0441\u043d\u043e\u0432\u043d\u043e\u0439 \u0441\u043e\u0441\u0442\u0430\u0432 2 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('zamena1', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0417\u0430\u043f\u0430\u0441\u043d\u043e\u0439 \u0441\u043e\u0441\u0442\u0430\u0432 1 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('zamena2', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0417\u0430\u043f\u0430\u0441\u043d\u043e\u0439 \u0441\u043e\u0441\u0442\u0430\u0432 2 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('trener1', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0422\u0440\u0435\u043d\u0435\u0440 1 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('trener2', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0422\u0440\u0435\u043d\u0435\u0440 2 \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('referee', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0413\u043b\u0430\u0432\u043d\u044b\u0439 \u0441\u0443\u0434\u044c\u044f')),
                ('assistent1', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='1 \u0411\u043e\u043a\u043e\u0432\u043e\u0439 \u0441\u0443\u0434\u044c\u044f')),
                ('assistent2', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='2 \u0411\u043e\u043a\u043e\u0432\u043e\u0439 \u0441\u0443\u0434\u044c\u044f')),
                ('reserve', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0420\u0435\u0437\u0440\u0432\u043d\u044b\u0439 \u0441\u0443\u0434\u044c\u044f')),
                ('text', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442\u043e\u0432\u0430\u044f \u0442\u0440\u0430\u043d\u0441\u043b\u044f\u0446\u0438\u044f')),
                ('champ', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Champ', verbose_name='\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442')),
                ('command_first', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_first', to='football.Team', verbose_name='\u041f\u0435\u0440\u0432\u0430\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u0430')),
                ('command_second', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_second', to='football.Team', verbose_name='\u0412\u0442\u043e\u0440\u0430\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u0430')),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Group', verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430')),
                ('stadium', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Stadium', verbose_name='\u0421\u0442\u0430\u0434\u0438\u043e\u043d')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0442\u0447',
                'verbose_name_plural': '\u041c\u0430\u0442\u0447\u0438',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f')),
                ('position', models.CharField(max_length=100, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('number', models.CharField(max_length=100, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432 \u043a\u043e\u043c\u0430\u043d\u0434\u0435')),
                ('birth', models.DateField(verbose_name='\u0414\u0435\u043d\u044c \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f')),
                ('club', models.CharField(max_length=100, verbose_name='\u041a\u043b\u0443\u0431')),
                ('height', models.CharField(max_length=100, verbose_name='\u0420\u043e\u0441\u0442')),
                ('weight', models.CharField(max_length=100, verbose_name='\u0412\u0435\u0441')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='football.Team', verbose_name='\u0421\u0431\u043e\u0440\u043d\u0430\u044f')),
            ],
            options={
                'verbose_name': '\u0418\u0433\u0440\u043e\u043a',
                'verbose_name_plural': '\u0424\u0443\u0442\u0431\u043e\u043b\u0438\u0441\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Zayavka',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('champ', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='football.Champ', verbose_name='\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442')),
                ('players', models.ManyToManyField(to='football.Player', verbose_name='\u0417\u0430\u044f\u0432\u043b\u0435\u043d\u043d\u044b\u0435 \u0438\u0433\u0440\u043e\u043a\u0438')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='football.Team', verbose_name='\u0421\u0431\u043e\u0440\u043d\u0430\u044f')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430 \u043d\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u044b',
            },
        ),
    ]
