# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-16 13:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('football', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('acronym', models.CharField(max_length=10, verbose_name='\u0410\u0431\u0431\u0440\u0435\u0432\u0438\u0430\u0442\u0443\u0440\u0430')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u041b\u043e\u0433\u043e')),
            ],
            options={
                'verbose_name': '\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f',
                'verbose_name_plural': '\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Stadium',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(blank=True, null=True, upload_to=b'', verbose_name='\u0421\u0445\u0435\u043c\u0430')),
                ('city', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('country', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430')),
                ('traffic', models.IntegerField(blank=True, null=True, verbose_name='\u0412\u043c\u0435\u0441\u0442\u0438\u043c\u043e\u0441\u0442\u044c')),
            ],
            options={
                'verbose_name': '\u0421\u0442\u0430\u0434\u0438\u043e\u043d',
                'verbose_name_plural': '\u0421\u0442\u0430\u0434\u0438\u043e\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('eng_name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043d\u0430 \u0430\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u043e\u043c')),
                ('code', models.CharField(blank=True, max_length=5, null=True, verbose_name='\u041a\u043e\u0434 \u0441\u0442\u0440\u0430\u043d\u044b')),
                ('offsite', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041e\u0444\u0438\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0439 \u0441\u0430\u0439\u0442')),
                ('logo', models.CharField(blank=True, max_length=3000, null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f')),
                ('flag', models.CharField(blank=True, max_length=3000, null=True, verbose_name='\u0424\u043b\u0430\u0433')),
                ('photo', models.CharField(blank=True, max_length=3000, null=True, verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('forma', models.CharField(blank=True, max_length=3000, null=True, verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u0444\u043e\u0440\u043c\u044b')),
                ('widget', models.TextField(blank=True, null=True, verbose_name='\u0412\u0438\u0434\u0436\u0435\u0442')),
                ('address', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('email', models.CharField(blank=True, max_length=200, null=True, verbose_name='Email')),
                ('phone', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('fax', models.CharField(blank=True, max_length=200, null=True, verbose_name='Fax')),
                ('fansite', models.CharField(blank=True, max_length=2000, null=True, verbose_name='\u041d\u0435\u043e\u0444\u0438\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0435 \u0441\u0430\u0439\u0442\u044b')),
                ('founded', models.IntegerField(blank=True, null=True, verbose_name='\u0413\u043e\u0434 \u043e\u0441\u043d\u043e\u0432\u0430\u043d\u0438\u044f')),
                ('country_fed', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u0444\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438')),
                ('techteam', models.BooleanField(default=False, verbose_name='\u0422\u0435\u0445\u043d\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u0430')),
                ('fed', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Fed', verbose_name='\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f')),
                ('stadium', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='football.Stadium', verbose_name='\u0414\u043e\u043c\u0430\u0448\u043d\u0438\u0439 \u0441\u0442\u0430\u0434\u0438\u043e\u043d')),
            ],
            options={
                'verbose_name': '\u0421\u0431\u043e\u0440\u043d\u0430\u044f',
                'verbose_name_plural': '\u0421\u0431\u043e\u0440\u043d\u044b\u0435',
            },
        ),
    ]
