from django.conf.urls import url
import django

from . import views
from views import TeamSitemap, NewsSitemap, TextPageSitemap, MatchSitemap, StadiumSitemap, PlayerSitemap

sitemaps = {
	'team': TeamSitemap,
	'news': NewsSitemap,
	'textpage': TextPageSitemap,
	'match': MatchSitemap,
	'stadium': StadiumSitemap,
	'player': PlayerSitemap,
}

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^teams/$', views.teams, name='teams'),
    url(r'^matches/$', views.matches, name='matches'),
    url(r'^stadiums/$', views.stadiums, name='stadiums'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap' , {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^yandex_(?P<yandex_verification_number>[0-9a-z]+)\.html$', views.yandex_verification, name='yandex_verification'),
    url(r'^google(?P<google_verification_number>[0-9a-z]+)\.html$', views.google_verification, name='google_verification'),
    url(r'^wmail_(?P<mail_verification_number>[0-9a-z]+)\.html$', views.mail_verification, name='mail_verification'),      
    url(r'^(?P<textpage_alias>[0-9a-z\-]+)/$', views.textpage, name='textpage'),
    url(r'^news/(?P<news_alias>[0-9a-z\-]+)/$', views.news, name="news"),
    url(r'^teams/(?P<team_alias>[0-9a-z\-]+)/$', views.team, name="team"),
    url(r'^matches/(?P<match_alias>[0-9a-z\-]+)/$', views.match, name="match"),
    url(r'^players/(?P<player_alias>[0-9a-z\-]+)/$', views.player, name="player"),
    url(r'^stadiums/(?P<stadium_alias>[0-9a-z\-]+)/$', views.stadium, name="stadium"),
]

handler404 = 'portal.views.error_404'