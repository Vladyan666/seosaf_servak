# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField

# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True
        
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню" )
    
    def get_absolute_url(self):
        return reverse('portal:textpage', kwargs={'textpage_alias': self.alias})
    
    def __unicode__(self):
        return self.name
    
class Stadium(Page):
    class Meta:
        verbose_name = u"Стадион"
        verbose_name_plural = u"Стадионы"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Схема")
    
    def get_absolute_url(self):
        return reverse('portal:stadium', kwargs={'stadium_alias': self.alias})
    
    def __unicode__(self):
        return self.name
    
class Team(Page):
    class Meta:
        verbose_name = u"Сборная"
        verbose_name_plural = u"Сборные"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    offsite = models.CharField( max_length=200 , verbose_name=u"Официальный сайт")
    group = models.CharField(max_length=100, verbose_name=u"Группа")
    image = models.ImageField(verbose_name=u"Фотография")
    
    def get_absolute_url(self):
        return reverse('portal:team', kwargs={'team_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Player(Page):
    class Meta:
        verbose_name = u"Игрок"
        verbose_name_plural = u"Хоккеисты"
        
    name = models.CharField(max_length=200, verbose_name=u"Имя")
    position = models.CharField(max_length=100, verbose_name=u"Позиция")
    number = models.CharField(max_length=100, verbose_name=u"Номер в команде")
    birth = models.DateField(verbose_name=u"День рождения")
    team = models.ForeignKey(Team, verbose_name=u"Сборная")
    height = models.CharField(max_length=100, verbose_name=u"Рост")
    weight = models.CharField(max_length=100, verbose_name=u"Вес")
    image = models.ImageField(verbose_name=u"Фотография")
    
    def get_absolute_url(self):
        return reverse('portal:player', kwargs={'player_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Match(Page):
    class Meta:
        verbose_name = u"Матч"
        verbose_name_plural = u"Матчи"
         
    datetime = models.DateTimeField(verbose_name=u"Время начала матча")
    stadium = models.ForeignKey(Stadium, verbose_name=u"Стадион")
    group = models.CharField(max_length=100, verbose_name=u"Группа")
    tour = models.CharField(max_length=100, verbose_name=u"Тур")
    command_first = models.ForeignKey(Team, verbose_name=u"Первая команда", related_name="command_first")
    command_second = models.ForeignKey(Team, verbose_name=u"Вторая команда", related_name="command_second") 
    command_data = JSONField()
    history = JSONField()
    actions_data = JSONField()
    offace_data = JSONField()
    
    def get_absolute_url(self):
        return reverse('portal:match', kwargs={'match_alias': self.alias})
    
    def __unicode__(self):
       return self.command_first.name + ' - ' + self.command_second.name + ' ' + str(self.datetime)
       
class News(Page):
    class Meta:
        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"
        
    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    date = models.DateField( default=datetime.date.today , verbose_name=u"Время публикации" )
    
    def get_absolute_url(self):
        return reverse('portal:news', kwargs={'news_alias': self.alias})
    
    def __unicode__(self):
        return self.name