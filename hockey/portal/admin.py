# -*- coding: utf-8 -*-
from django.contrib import admin
from portal.models import TextPage, Team, Player, Match, Stadium, News

# Register your models here.
#общие поля
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
#
admin.site.register(TextPage,TextPageAdmin)

#Игроки
class PlayerAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [
        (u'Личная информация',  {'fields':['name','birth','image']}),
        (u'Профессиональные данные',   {'fields':['number','position','team']}),
    ] + page_fields
#добавление игрока на странице клуба
class PlayerInline(admin.StackedInline):
    model = Player
    extra = 1
    fieldsets = [
        (u'Личная информация',  {'fields':['name','birth']}),
        (u'Профессиональные данные',   {'fields':['number','position','team']}),
    ]
#
admin.site.register(Player,PlayerAdmin)

#Стадионы
class StadiumAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name',) + page_list
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','image']}),
    ] + page_fields
#
admin.site.register(Stadium,StadiumAdmin)

#Команды
class TeamAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    #inlines = [PlayerInline] #добавлять игрока на странице клуба
    list_display = ('name',) + page_list
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','offsite','group','image']}),
    ] + page_fields
#
admin.site.register(Team,TeamAdmin)

#Матчи
class MatchAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("datetime","stadium")}
    list_display = ('name','datetime',"stadium") + page_list
    
    def name(self, obj):
        return ("%s - %s" % (obj.command_first, obj.command_second)).upper()
    name.short_description = u'Матч'
    
    fieldsets = [
        (u'Информация о матче',  {'fields':['command_first','command_second','datetime',"stadium"]}),
    ] + page_fields
admin.site.register(Match,MatchAdmin)


#Новости
class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Новость',  {'fields':['name','date',]}),] + page_fields
#
admin.site.register(News,NewsAdmin)