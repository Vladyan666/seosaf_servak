# -*- coding: utf-8 -*-
import datetime
from django.utils import timezone
from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.http.response import Http404
from django.contrib.sitemaps import Sitemap
from django.template import RequestContext, loader
from django.db.models import Q
from .models import TextPage, News, Team, Player, Match, Stadium

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias

def default_context(request,alias,object):
    host = active_menu(request)
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"))
    russia = Team.objects.get(name="Россия")
    ru_matches = Match.objects.filter(Q(command_first=russia) | Q(command_second=russia)).order_by('datetime')
    last_news = News.objects.order_by('date')[:4]
    teamsA = Team.objects.filter(group="A").order_by('name')
    teamsB = Team.objects.filter(group="B").order_by('name')
    teamsAB = [teamsA,teamsB]
    try:
        data = object.objects.get(alias=alias)
    except object.DoesNotExist:
        raise Http404
    
    context_object = {
        'host' : host,
        'menu' : menu,
        'ru_matches' : ru_matches,
        'last_news' : last_news,
        'teamsA' : teamsA,
        'teamsB' : teamsB,
        'teamsAB' : teamsAB,
        'data' : data,
    }
    
    return context_object

# Create your views here.
def index(request):
    template = loader.get_template('main/index.html')
    
    context_data = default_context( request , "index" , TextPage )    
    news = News.objects.order_by('date')[:5]
    
    context_data.update({
        'news' : news,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    

def textpage(request,textpage_alias):
    template = loader.get_template('main/textpage.html')
    
    context_data = default_context( request , textpage_alias , TextPage )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def news(request,news_alias):
    template = loader.get_template('main/news_item.html')
    
    context_data = default_context( request , news_alias , News )
    try:
        next = context_data['data'].get_next_by_date()
    except News.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_date()
    except News.DoesNotExist:
        prev = None
    
    context_data.update({
        'next' : next,
        'prev' : prev,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def stadiums(request):
    template = loader.get_template('main/stadiums.html')
    
    context_data = default_context( request , "stadiums" , TextPage )
    stadiums = Stadium.objects.all()
    
    context_data.update({
        'stadiums' : stadiums,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def teams(request):
    template = loader.get_template('main/teams.html')
    
    context_data = default_context( request , "teams" , TextPage )
    teams = Team.objects.order_by('name')
    
    context_data.update({
        'teams' : teams,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def matches(request):
    template = loader.get_template('main/matches.html')
    
    context_data = default_context( request , "matches" , TextPage )
    matches = Match.objects.order_by('datetime')
    stadiums = Stadium.objects.all()
    
    context_data.update({
        'matches' : matches,
        'stadiums' : stadiums,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def team(request,team_alias):
    template = loader.get_template('main/team.html')
    
    context_data = default_context( request , team_alias , Team )

    breadcrumbs = [TextPage.objects.get(alias='teams')]
    matches = Match.objects.filter(Q(command_first=context_data['data']) | Q(command_second=context_data['data']))
    coaches = Player.objects.filter(position__contains="тренер", team=context_data['data'])
    goalkeepers = Player.objects.filter(position__contains="Вратарь", team=context_data['data'])
    defenders = Player.objects.filter(position__contains="Защитник", team=context_data['data'])
    attacks = Player.objects.filter(position__contains="Нападающий", team=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'matches' : matches,
        'coaches' : coaches,
        'goalkeepers' : goalkeepers,
        'defenders' : defenders,
        'attacks' : attacks,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def get_birth( data ):
    if data.birth.year == 1 :
        return u"Нет данных"
    else :
        return data.birth
    
def player(request, player_alias):
    template = loader.get_template('main/player.html')
    
    context_data = default_context( request , player_alias , Player )
    breadcrumbs = [TextPage.objects.get(alias='teams'),context_data['data'].team]
    birth = get_birth( context_data['data'] )
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'birth' : birth,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def match(request, match_alias):
    template = loader.get_template('main/match.html')

    context_data = default_context( request , match_alias , Match )
    breadcrumbs = [TextPage.objects.get(alias='matches')]    
    try:
        next = context_data['data'].get_next_by_datetime()
    except Match.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_datetime()
    except Match.DoesNotExist:
        prev = None
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'next' : next,
        'prev' : prev,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def stadium(request, stadium_alias):
    template = loader.get_template('main/stadium.html')

    context_data = default_context( request , stadium_alias , Stadium )
    breadcrumbs = [TextPage.objects.get(alias='stadiums')]
    matches = Match.objects.filter(stadium=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'matches' : matches
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

# не работает(    
def error_404(request):
    template = loader.get_template('main/404.html')
    
    context_data = default_context( request , match_alias , TextPage )

    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
# Карта сайта
class StadiumSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Stadium.objects.all()
    
class PlayerSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Player.objects.all()
    
class MatchSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Match.objects.all()
    
class TextPageSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return TextPage.objects.all()    
    
class TeamSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Team.objects.all()
        
class NewsSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return News.objects.all()
        
def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")
    
def yandex_verification(request,yandex_verification_number):
    template = loader.get_template('yandex_verification.html')
    context = RequestContext(request,{
        'yandex_verification_number' : yandex_verification_number,
    })
    return HttpResponse(template.render(context))
    
def google_verification(request,google_verification_number):
    template = loader.get_template('google_verification.html')
    context = RequestContext(request,{
        'google_verification_number' : google_verification_number,
    })
    return HttpResponse(template.render(context))
    
def mail_verification(request,mail_verification_number):
    template = loader.get_template('mail_verification.html')
    context = RequestContext(request,{
        'mail_verification_number' : mail_verification_number,
    })
    return HttpResponse(template.render(context))