$(document).ready(function () {
    $('.nav-tabs').on('click' , 'a' , function () {
        var target = $(this).parent().attr('data-target');
        
        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');
        
        $('.tab-pane').removeClass('active in');
        $('.tab-pane[data-name='+target+']').addClass('active in');
        
        window.location.hash = target;
    });
    var hash = window.location.href.split('#')[1];
    $('.nav-tabs li[data-target='+hash+'] a').trigger('click');
    
    var matchesFilter = function () {
        var matches = $('.element_size_100').find('.match-result').parents('article');
        var filters = $('.matches-filter select');
    
        filters.on('change' , function () {
            var type = $(this).attr('data-type');
            var value = $(this).val();
       
            matches.hide();

            var filters_value = {
                'stadium' : filters[0].value,
                'team' : filters[1].value
            }

            matches.has('.pix-post-title-nohover a:contains(' + filters_value.team + ')')
                   .has('.calendar-date:contains(' + filters_value.stadium + ')')
                   .show();
        });
        $('.matches-filter button').on('click' , function () {
            matches.show();
            filters.val('');
        });
    }
    matchesFilter();
    
    
    var svg_id = '#svg-scheme';
    var svg_field = Snap(svg_id);
    svg_field.attr({
        viewBox: [0, 0, 830, 650]
    });
    var svg_src = $(svg_id).attr('data-src');
    Snap.load(svg_src , function ( svg ) {
    
        var g = svg.selectAll("g").attr({
                'style' : "cursor:pointer;"
            }),
            figure = svg.selectAll("polygon,rect,path").attr({
                    'stroke' : "#489DD0",
                    'stroke-width' : "2px",
                    'fill' : '#fff'
                }),
            text = svg.selectAll("text").attr({
                    'font' : "12px Source Sans Pro, sans-serif",
                    'stroke-width' : "3px"
                })
                
        
        for (var i = 0; i < text.length; i++) {
            text[i].node.innerHTML = text[i].node.innerHTML.replace("Ложа ","Л.")
        }
        
        
        var tooltip =  function ( name , count , price ) {
            return  '<div id="svg-scheme-tooltip">' +
                        '<p>' + name + '</p>' +
                        '<p>' + count + ' билетов в наличии</p>' +
                        '<p> Цена от ' + price + ' р.</p>' +
                    '</div>';
        }
        
        svg_field.append(g);
        
        $(svg_id).parent().on('mousemove' , function (event) {
            var pos = $(this).offset();
            var elem_left = pos.left;
            var elem_top = pos.top;
            // положение курсора внутри элемента
            var tt_x = event.pageX - elem_left;
            var tt_y = event.pageY - elem_top;
            $(svg_id).parent().find('#svg-scheme-tooltip').css({
                top : tt_y + 30,
                left : tt_x + 30
            });
        })
        
        for (var i = 0; i < g.length; i++) {
            current_g = g[i];
            
            g[i].node.onmouseover = function (current_g) {
                return function () {
                    current_g.selectAll("polygon,rect,path")[0].stop().animate({
                        fill : "#60A8D3"
                    }, 100);
                    current_g.select('text').attr({
                        fill : "#fff"
                    }); 
                    
                    var tt = tooltip(current_g.select('text').node.innerHTML.replace("Л.","Ложа ") , '10' , '1000');
                    $(svg_id).parent().append(tt);
                }
            }(current_g);
            
            g[i].node.onmouseout = function (current_g) {
                return function () {
                    current_g.selectAll("polygon,rect,path")[0].stop().animate({
                        fill : "#fff"
                    },100);
                    current_g.select('text').attr({
                        fill : "#000"
                    });
                    $(svg_id).parent().find('#svg-scheme-tooltip').remove()
                }
            }(current_g);
        }
    })
});