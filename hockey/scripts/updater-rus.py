#!c:/Python27/python
# -*- coding: utf8 -*-

# определение окружения
import os, sys, locale, codecs , argparse , django
sys.path.append('c:\\openserver\\domains\\hockey\\')
sys.path.append('c:\\openserver\\domains\\hockey\\hockey\\')
os.environ["DJANGO_SETTINGS_MODULE"] = "hockey.settings"
django.setup()

import json, pytils, datetime, pytz
from portal.models import TextPage, Team, Match, Player, Stadium, News
from django.utils import timezone
from django.conf import settings
from django.template.defaultfilters import slugify

def setup_console(sys_enc="utf-8"):
    reload(sys)
    try:
        # для win32 вызываем системную библиотечную функцию
        if sys.platform.startswith("win"):
            import ctypes
            enc = "cp%d" % ctypes.windll.kernel32.GetOEMCP() #TODO: проверить на win64/python64
        else:
            # для Linux всё, кажется, есть и так
            enc = (sys.stdout.encoding if sys.stdout.isatty() else
                        sys.stderr.encoding if sys.stderr.isatty() else
                            sys.getfilesystemencoding() or sys_enc)

        # кодировка для sys
        sys.setdefaultencoding(sys_enc)

        # переопределяем стандартные потоки вывода, если они не перенаправлены
        if sys.stdout.isatty() and sys.stdout.encoding != enc:
            sys.stdout = codecs.getwriter(enc)(sys.stdout, 'replace')

        if sys.stderr.isatty() and sys.stderr.encoding != enc:
            sys.stderr = codecs.getwriter(enc)(sys.stderr, 'replace')

    except:
        pass # Ошибка? Всё равно какая - работаем по-старому...

# создание парсера командной строки
def create_parser ():
    parser = argparse.ArgumentParser()
    parser.add_argument('type', nargs='?')
    parser.add_argument ('-save', action='store_const', const=True)
 
    return parser

# получить id объекта
def get_object_id( object , name ):
    try:
        return object.objects.get(name=name).id
    except object.DoesNotExist:
        return None

# получить id матча        
def get_match_id( object , match_datetime , match_stadium ):
    try:
        return object.objects.get(datetime=match_datetime,stadium=match_stadium).id
    except object.DoesNotExist:
        return None
        
# получить время в нужном формате
def get_datetime( str , format="%d.%m.%Y" ):
    try :
        return timezone.make_aware(datetime.datetime.strptime(str, format), timezone.get_current_timezone())
    except ValueError :
        print u"Дата не определена: " + str.encode('utf8')
        return timezone.make_aware(datetime.datetime.strptime("01.01.0001", "%d.%m.%Y"), timezone.get_current_timezone())
    
# задать Текстовую страницу        
def set_textpage( textpage ):
    if get_object_id( TextPage , textpage['name'] ) :
        print u"Обновлена страница:"
    else :
        print u"Добавлена страница:"
    return TextPage(
        id = get_object_id( TextPage , textpage['name'] ),
        name = textpage['name'],
        content = textpage['content'],
        seo_h1 = textpage['seo_h1'],
        seo_title = textpage['seo_title'],
        seo_description = textpage['seo_description'],     
        alias = slugify(pytils.translit.translify(textpage['name'])) if textpage['alias'] == '' else textpage['alias'],
        menutitle = textpage['menutitle'],
        menuposition = int(textpage['menuposition']),
        menushow = False if textpage['menushow'] == '0' else True,
        sitemap = False if textpage['sitemap'] == '0' else True,
    )

# задать Новость        
def set_news( news ):
    if get_object_id( News , news['name'] ) :
        print u"Обновлена новость:"
    else :
        print u"Добавлена новость:"
    return News(
        id = get_object_id( News , news['name'] ),
        name = news['name'],
        content = news['content'],
        seo_h1 = news['seo_h1'],
        seo_title = news['seo_title'],
        seo_description = news['seo_description'],     
        alias = slugify(pytils.translit.translify(news['name'])) if news['alias'] == '' else news['alias'],
        menutitle = news['menutitle'],
        menushow = False if news['menushow'] == '0' else True,
        sitemap = False if news['sitemap'] == '0' else True,
    )
    
# задать Стадион        
def set_stadium( stadium ):
    if get_object_id( Stadium , stadium['name'] ) :
        print u"Обновлен стадион:"
    else :
        print u"Добавлен стадион:"
    return Stadium(
        id = get_object_id( Stadium , stadium['name'] ),
        name = stadium['name'],
        image = settings.MEDIA_URL + "files/" + stadium['image'],
        alias = slugify(pytils.translit.translify(stadium['name'])),
    )
    
# задать Клуб        
def set_team( team ):
    if get_object_id( Team , team['name'] ) :
        print u"Обновлен клуб:"
    else :
        print u"Добавлен клуб:"
    return Team(
        id = get_object_id( Team , team['name'] ),
        name = team['name'],
        offsite = team['url'],
        group = team['group'],
        image = settings.MEDIA_URL + "files/" + team['image'],
        alias = slugify(pytils.translit.translify(team['name'])),
    )

# получить id игрока на основе его номера в команде    
def get_player_id( pl_number , fc_object ):
    try :
        return Player.objects.get(number=pl_number,team=fc_object).id
    except Team.DoesNotExist as detail:
        print u"В базе нет клуба: " + fc_name.encode('utf8') + u": ", detail
    except Player.DoesNotExist as detail:
        print u"В данном клубе и с таким номером игрок не найден: " + fc_object.name.encode('utf8') + u", " + pl_number.encode('utf8') + u": ", detail
    try :
        trans_player = Player.objects.get(transfer__contains=[[pl_number,fc_object.name]])
        print u"Игрок найден на основе данных трансферов: ", trans_player
        return trans_player
    except Player.DoesNotExist :
        print u"Данные в трансеферах не найдены"

# изменить номера игроков на id
def change_number_to_id( sostav , fc_name ):
    team = Team.objects.get(shortname=fc_name)
    
    for key in sostav :
        if key != "name":
            for player in sostav[key] :
                player_id = get_player_id( player[0].encode('utf-8') , team )
                player[0] = str(player_id)
        
    return sostav
        
# задать Матч   
def set_match( match ):
    try :
        # форматирование даты для записи в базу
        datetime_for_base = get_datetime( match['datetime'].strip() , format="%d.%m.%Y %H:%M" )
        stadium_object = Stadium.objects.get(name=match['stadium'])
        
        if get_match_id( Match , datetime_for_base , stadium_object) :
            print u"Обновлен матч:"
        else :
            #match['fc_first'] = change_number_to_id(match['fc_first'] , match['fc_first']['name'])
            #match['fc_second'] = change_number_to_id(match['fc_second'] , match['fc_second']['name'])

            print u"Добавлен матч:"
        
        return Match(
            id = get_match_id( Match , datetime_for_base , stadium_object) ,
            datetime = datetime_for_base ,
            stadium = stadium_object,
            group = match['group'],
            tour = match['tour'],
            command_first = Team.objects.get(name=match['fc_first']['name']),
            command_second = Team.objects.get(name=match['fc_second']['name']),               
            command_data = [match['fc_first'],match['fc_second']],
            history = match['history'] if 'history' in match else [],
            actions_data = match['actions'] if 'actions' in match else [],
            offace_data = match['offace'] if 'offace' in match else [],
            alias = slugify(pytils.translit.translify(datetime_for_base.strftime('%d-%m-%Y-%H-%M-')+match['stadium'])),
        )
    except Team.DoesNotExist as detail:
        print u"В базе нет одного из клубов: " + match['fc_first']['name'].encode('utf8') + u", " + match['fc_second']['name'].encode('utf8') + u": ", detail
    
# задать Игрока   
def set_player( player ):
    try :
        if get_object_id( Player , player['name'] ) :
            print u"Обновлен игрок:"
        else :
            print u"Добавлен игрок:"
        return Player(     
            id = get_object_id( Player , player['name'] ),
            name = player['name'],
            number = player['number'],
            birth = get_datetime(player['birth']),
            position = player['position'],
            team = Team.objects.get(name=player['team']),
            height = player['height'],
            weight = player['weight'],
            image = settings.MEDIA_URL + "files/" + player['image'],
            alias = slugify(pytils.translit.translify(player['name'])),
        )
    except Team.DoesNotExist as detail:
        print u"В базе нет клуба: " + player['team'].encode('utf8') + u": ", detail
    
# перезаписываем файл   
def rewrite_file( rewrite ):
    data = open('pr/'+object_name+'.pr' , 'w')
    data.write(rewrite)
    data.close()

#основные операции
def drive( object_name ):

    # разбор аргументов коммандной строки
    parser = create_parser()
    namespace = parser.parse_args()
    
    # если передан аргумент переопределяем тип данных
    if namespace.type :
        object_name = namespace.type
    
    data = open('pr/'+object_name+'.pr' , 'r')  # открытие файла
    go = False                            # начало записи в базу
    change = False                        # наличие изменений в файле
    rewrite = ''                          # перезаписанный файл
    
    for line in data :  
        # записываем в базу
        if go :
            if line.strip() == '': # пропуск пустых строк
                continue
                
            json_object = json.loads(line) # декодирование строки
            
            if object_name == 'other':
                object_name = json_object['type']
            
            #выбираем модель записи данных
            if object_name == 'team':                 # клуб
                object = set_team( json_object )
            elif object_name == 'match':            # матч
                object = set_match( json_object )
            elif object_name == 'player':           # игрок
                object = set_player( json_object )
            elif object_name == 'stadium':          #стадион
                object = set_stadium( json_object )
            elif object_name == 'textpage':          #страница
                object = set_textpage( json_object )
            elif object_name == 'news':          #страница
                object = set_news( json_object )

            if object :                             #проверка существования объекта
                if namespace.save:
                    object.save()
                #object.save()                          # сохранение объекта
                print object.alias
  
            rewrite += line  
            change = True 
        # не записываем
        else :
            # если встречаем break начинаем запись
            if line.strip() == 'break' :
                go = True
                
            else :
                rewrite += line    

    # если были изменения в файле делаем перенос строки        
    if change :
        rewrite += '\nbreak'
    else :
        rewrite += 'break'
        
    data.close()
    
    if namespace.save:
        print u"Данные добавлены в базу!"

    # перезаписываем файл
    #rewrite_file( rewrite )


if __name__ == '__main__':
    print "Content-Type: text/html; charset=UTF-8\r\n"
    print sys.getdefaultencoding()
    print locale.getpreferredencoding()
    print sys.stdout.encoding
    
    # object_name = "team" | "match" | "player" | "textpage" | "stadium" | "other" # в первой строке файла должно быть слово break
    object_name = 'other' 
        
    setup_console('utf-8')  # загрузка правильной кодировки
    drive( object_name )           

