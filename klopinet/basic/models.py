# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django import forms
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField

# Родительский класс для всех моделей - страниц сайта
class Page(models.Model):
    class Meta:
        abstract = True
    
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним" )
    menuposition = models.IntegerField( default=0 , verbose_name=u"Позиция в меню" )
    menushow = models.BooleanField( default=True , verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

# Модель обычной текстовой страницы
class Textpage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
    
    def get_absolute_url(self):
        if self.alias == "index" :
            return reverse('basic:index', kwargs={})
        else :
            return reverse('basic:textpage', kwargs={'textpage_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
# Модель услуги
class Usluga(Page):
    class Meta:
        verbose_name = u"Услуга"
        verbose_name_plural = u"Услуги"
    
    def get_absolute_url(self):
        return reverse('basic:textpage', kwargs={'textpage_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class ContactForm(forms.Form):
    name = forms.CharField(max_length=200,widget = forms.TextInput(attrs = {'placeholder': 'Имя' , 'class' : "form-control" , 'required' : True}))
    email = forms.EmailField(widget = forms.TextInput(attrs = {'placeholder': 'E-mail' , 'class' : "form-control"}),required = False)
    message = forms.CharField(widget=forms.Textarea(attrs = {'placeholder': 'Сообщение' , 'class' : "form-control" , 'required' : True}))

class Scripts(models.Model):
    class Meta:
        verbose_name = u"Скрипт"
        verbose_name_plural = u"Импортируемые скрипты"

    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    content = models.TextField( verbose_name=u"Содержимое" )

    def __unicode__(self):
        return self.name
