"""desbooks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from . import views
import sitemap

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'^catalog/', include('catalog.urls', namespace="catalog")),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^uslugi/(?P<textpage_alias>[0-9a-z\-]+)/$', views.usluga, name='uslugi'),
    url(r'^(?P<textpage_alias>[0-9a-z\-]+)/$', views.textpage, name='textpage'),
    
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^b8c10b2f0684\.html$', views.yandex_mail, name='yandex_mail'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap' , {'sitemaps': sitemap.get_map()}),
    url(r'^yandex_(?P<yandex_verification_number>[0-9a-z]+)\.html$', views.yandex_verification, name='yandex_verification'),
    url(r'^google(?P<google_verification_number>[0-9a-z]+)\.html$', views.google_verification, name='google_verification'),
    url(r'^wmail_(?P<mail_verification_number>[0-9a-z]+)\.html$', views.mail_verification, name='mail_verification'),
]
