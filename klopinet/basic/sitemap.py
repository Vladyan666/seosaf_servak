from django.contrib.sitemaps import Sitemap
from django.db.models import Q
from .models import Textpage, Usluga

class TextpageSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Textpage.objects.filter(sitemap=True)

class UslugaSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5

	def items(self):
		return Usluga.objects.filter(sitemap=True)

def get_map() :
    sitemaps = {
        'textpage': TextpageSitemap,
        'usluga' : UslugaSitemap,
    }
    return sitemaps
