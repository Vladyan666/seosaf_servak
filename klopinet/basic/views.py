# -*- coding: utf-8 -*-
import datetime
import re, sys, os
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.core.mail import send_mail, BadHeaderError
from django.template import RequestContext, loader
from django.db.models import Q

# импортируем модели
from .models import ContactForm, Textpage, Usluga,Scripts

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias

def default_context(request,alias,object):
    menu = Textpage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    scripts = Scripts.objects.all()
    uslugi = Usluga.objects.filter(menushow=True)

    data = get_object_or_404(object, alias=alias)
    
    context_object = {
        'menu' : menu,
        'uslugi' : uslugi,
        'data' : data,
        'scripts' : scripts,
    }
    
    return context_object

# Create your views here.
def index(request):
    template = loader.get_template('main/index.html')
    
    context_data = default_context( request , "index" , Textpage )    
    
    context_data.update({
        #'news' : news,
    })
    
    context = RequestContext(request, context_data)
#    context = RequestContext(request, {})
    
    return HttpResponse(template.render(context))
    

def textpage(request,textpage_alias):
    template = loader.get_template('main/textpage.html')
    
    context_data = default_context( request , textpage_alias , Textpage )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def usluga(request,textpage_alias):
    template = loader.get_template('main/textpage.html')
    
    context_data = default_context( request , textpage_alias , Usluga )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def contacts(request):
    template = loader.get_template('main/contacts.html')
    
    context_data = default_context( request , 'contacts' , Textpage )
    
    if request.method == 'POST':
        form = ContactForm(request.POST)
        error = ''
        success = ''
        
        if form.is_valid():
        
            name = form.cleaned_data['name']
            if form.cleaned_data['email'] :
                email = form.cleaned_data['email']
            else :
                email = "Не указан".decode('utf8')
            message = '<b>Имя отправителя:</b> '+name.encode('utf8')+'<br><b>E-mail: </b> '+email.encode('utf8')+'<p>'+form.cleaned_data['message'].encode('utf8')+'</p>'

            recipients = ['admin@klopinet.ru']

            try:
                send_mail("Сообщение с сайта klopinet.ru", message, 'admin@klopinet.ru', recipients, html_message=message)
            except BadHeaderError:
                error = 'Invalid header found'

            success = 'Сообщение успешно отправлено!'
        
            
        form = ContactForm()    
        context_data.update({
            'error' : error,
            'success' : success,
            'form' : form,
        })
        
    else :
        form = ContactForm()
        
        context_data.update({
           'form' : form,
        })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
    
def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")
    
def yandex_mail(request):
    template = loader.get_template('b8c10b2f0684.html')
    
    context = RequestContext(request,{})
    
    return HttpResponse(template.render(context))
    
def yandex_verification(request,yandex_verification_number):
    template = loader.get_template('yandex_verification.html')
    context = RequestContext(request,{
        'yandex_verification_number' : yandex_verification_number,
    })
    return HttpResponse(template.render(context))

def google_verification(request,google_verification_number):
    template = loader.get_template('google_verification.html')
    context = RequestContext(request,{
        'google_verification_number' : google_verification_number,
    })
    return HttpResponse(template.render(context))

def mail_verification(request,mail_verification_number):
    template = loader.get_template('mail_verification.html')
    context = RequestContext(request,{
        'mail_verification_number' : mail_verification_number,
    })
    return HttpResponse(template.render(context))

