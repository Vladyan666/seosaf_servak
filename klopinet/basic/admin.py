# -*- coding: utf-8 -*-
from django.contrib import admin

# импортируем модели
from .models import Textpage, Usluga, Scripts

# Поля модели Page
page_fields = [
    (u"Настройки страницы" , {'fields':['alias','menushow','menuposition','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')


#Страницы
class TextpageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(None,  {'fields':['name',]}),] + page_fields
#
admin.site.register(Textpage,TextpageAdmin)

#Страницы
class UslugaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(None,  {'fields':['name',]}),] + page_fields
#
admin.site.register(Usluga,UslugaAdmin)

class ScriptsAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','content',]}),]
#
admin.site.register(Scripts,ScriptsAdmin)