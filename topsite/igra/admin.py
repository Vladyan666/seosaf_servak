# -*- coding: utf-8 -*-
from django.contrib import admin
from igra.models import TextPage, Card, Nabor, Profile
# Register your models here.
#общие поля
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
#
admin.site.register(TextPage,TextPageAdmin)

class CardAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name']}),] + page_fields
#
admin.site.register(Card,CardAdmin)

class NaborAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name']}),] + page_fields
#
admin.site.register(Nabor,NaborAdmin)

class ProfileAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name','cards','nabors']}),] + page_fields
#
admin.site.register(Profile,ProfileAdmin)