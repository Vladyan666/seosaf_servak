import datetime
import re, sys, os
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.contrib.sitemaps import Sitemap
from django.core.mail import send_mail, BadHeaderError
from django.template import RequestContext, loader
from django.db.models import Q

# Create your views here.
from .models import TextPage,Profile, Card, Nabor
from base.models import Player

class TextPageSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return TextPage.objects.all()
		
def index(request):
    template = loader.get_template('index.html')
    context = RequestContext(request , "index" , TextPage )
    return HttpResponse(template.render(context))
		

def profile(request, alias):
    template = loader.get_template('profile.html')
    
    profile = Profile.objects.all()
    player = Player.objects.order_by('?')[:11]
    context_data = {
    'profile' : profile,
    'player' : player,
    }

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))
