# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField

class Page(models.Model):
    class Meta:
        abstract = True

    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )
    
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страницы"
        verbose_name_plural = u"Страницы"

    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню" )

    def get_absolute_url(self):
        if self.alias == "index" :
            return reverse('igra:index', kwargs={})
        else :
            return reverse('igra:textpage', kwargs={'textpage_alias': self.alias})

    def __unicode__(self):
        return self.name

class Card(Page):
    class Meta:
        verbose_name = u"Карточка"
        verbose_name_plural = u"Карточки"

    name = models.CharField(max_length=200, verbose_name=u"Фамилия Имя")
    position = models.CharField(max_length=100, verbose_name=u"Позиция" , null=True , blank=True )
    number = models.CharField(max_length=3, verbose_name=u"Номер в команде" , null=True , blank=True )
    birth = models.DateField(verbose_name=u"День рождения" , null=True , blank=True )
    team = models.CharField(max_length=200, verbose_name=u"Команда" )
    country = models.CharField(max_length=100, verbose_name=u"Гражданство" , null=True , blank=True )
    height = models.CharField(max_length=100, verbose_name=u"Рост" , null=True , blank=True )
    weight = models.CharField(max_length=100, verbose_name=u"Вес" , null=True , blank=True )
    image = models.CharField(max_length=100,verbose_name=u"Фотография" , null=True , blank=True )
    exp = models.IntegerField(default='0',verbose_name=u"Опыт")
    points = models.IntegerField(default='0',verbose_name=u"Очки")

    def get_absolute_url(self):
        return reverse('igra:card', kwargs={'card_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Nabor(Page):
    class Meta:
        verbose_name = u"Набор"
        verbose_name_plural = u"Наборы"

    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    cards = models.ManyToManyField(Card, verbose_name=u"Карточки")
    price = models.IntegerField(verbose_name=u"Цена", null=True , blank=True)

    def __unicode__(self):
        return self.name
        
class Profile(Page):
    class Meta:
        verbose_name = u"Профиль игрока"
        verbose_name_plural = u"Профили"

    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    cards = models.ManyToManyField(Card, verbose_name=u"Карточки")
    nabors = models.ManyToManyField(Nabor, verbose_name=u"Наборы")
    
    def get_absolute_url(self):
        return reverse('igra:profile', kwargs={'profile_alias': self.alias})

    def __unicode__(self):
        return self.name