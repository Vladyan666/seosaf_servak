from __future__ import unicode_literals

from django.apps import AppConfig


class IgraConfig(AppConfig):
    name = 'igra'
