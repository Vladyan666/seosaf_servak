from django.conf.urls import url
import django
from django.contrib import admin
from . import views
from .views import TextPageSitemap

sitemaps = {
	'page': TextPageSitemap,
}

urlpatterns = [
	url(r'^$', views.index, name='index'),
    url(r'^(?P<alias>[0-9a-z\-]+)/$', views.profile, name='profile'),
]

handler404 = 'base.views.error_404'