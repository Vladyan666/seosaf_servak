# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-28 18:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0025_player_eng_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='parse',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='\u041e\u0442\u043a\u0435\u0443\u0434\u0430 \u043f\u0430\u0440\u0441\u0438\u043c'),
        ),
    ]
