# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-25 12:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0019_scripts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stadium',
            name='city',
            field=models.CharField(default=1, max_length=200, verbose_name='\u0413\u043e\u0440\u043e\u0434'),
            preserve_default=False,
        ),
    ]
