# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-07 22:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20160804_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='zayavka',
            name='number',
            field=models.CharField(blank=True, max_length=3, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432 \u0441\u0435\u0437\u043e\u043d\u0435'),
        ),
        migrations.AlterField(
            model_name='player',
            name='number',
            field=models.CharField(blank=True, max_length=3, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432 \u043a\u043e\u043c\u0430\u043d\u0434\u0435'),
        ),
    ]
