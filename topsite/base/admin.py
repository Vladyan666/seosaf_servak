# -*- coding: utf-8 -*-
from django.contrib import admin
from base.models import TextPage, Team, Player, Match, Stadium, News, Champ, Group, Zayavka, City, Teamchamp, Scripts

# Register your models here.
#общие поля
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
#
admin.site.register(TextPage,TextPageAdmin)

#Игроки
class PlayerAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [
        (u'Личная информация',  {'fields':['name', 'eng_name', 'fioname', 'birth','image','weight','height']}),
        (u'Профессиональные данные',   {'fields':['number','position','team','country']}),
    ] + page_fields
    list_display = ('name','image','alias') + page_list
    search_fields = ['name']
    list_filter = ['team__name','position','club']
#добавление игрока на странице клуба
class PlayerInline(admin.StackedInline):
    model = Player
    extra = 1
    fieldsets = [
        (u'Личная информация',  {'fields':['name','birth','']}),
        (u'Профессиональные данные',   {'fields':['number','position','team']}),
    ]
#
admin.site.register(Player,PlayerAdmin)

#Города
class CityAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'image',]}),] + page_fields
#
admin.site.register(City,CityAdmin)

#Стадионы
class StadiumAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','country','city','traffic') + page_list
    fieldsets = [
        (u'Информация о стадионе',  {'fields':['name','image','city','country','traffic']}),
    ] + page_fields
    search_fields = ['name']
    list_filter = ['country','city']
#
admin.site.register(Stadium,StadiumAdmin)

#Группы
class GroupAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name',) + page_list
    fieldsets = [
        (u'Информация о группе',  {'fields':['name','champ','teams','qualifying']}),
    ] + page_fields
    filter_vertical = ('teams',)
    list_filter = ['champ',]
#
admin.site.register(Group,GroupAdmin)

#Заявки
class ZayavkaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("champ","team")}
    list_display = ('team', 'champ') + page_list
    fieldsets = [
        (u'Информация о заявке',  {'fields':['champ','team','players',]}),
    ] + page_fields
    search_fields = ['team__name']
    list_filter = ['champ','team']
    filter_vertical = ('players',)
#
admin.site.register(Zayavka,ZayavkaAdmin)

#Чемпионаты
class ChampAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','year') + page_list
    fieldsets = [
        (u'Информация о чемпионате',  {'fields':['name','image','year','teams','web','status']}),
    ] + page_fields
    search_fields = ['name']
    list_filter = ['year']
    filter_vertical = ('teams',)
#
admin.site.register(Champ,ChampAdmin)

#Команды
class TeamAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    #inlines = [PlayerInline] #добавлять игрока на странице клуба
    list_display = ('name','city','country','stadium')
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','shortname','founded','offsite','city','country','stadium','logo','flag','photo','forma','techteam']}),
    ] + page_fields
    list_filter = ['country']
    search_fields = ['name']
#
admin.site.register(Team,TeamAdmin)

class TeamchampAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    #inlines = [PlayerInline] #добавлять игрока на странице клуба
    list_display = ('name','champ','points','win','draw','lose','goals','vgoals','mesto')
    fieldsets = [
        (u'Информация о клубе',  {'fields':['name','shortname','stadium','champ','logo','photo','forma','techteam','points','goals','vgoals','win','lose','draw','mesto']}),
    ] + page_fields
    list_filter = ['champ']
    search_fields = ['name']
#
admin.site.register(Teamchamp,TeamchampAdmin)

#Матчи
class MatchAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("datetime","stadium")}
    list_display = ('id','name','parse','score','tour','datetime',"stadium","status") + page_list
    
    def name(self, obj):
        return ("%s - %s" % (obj.command_first, obj.command_second)).upper()
    name.short_description = u'Матч'
    
    fieldsets = [
        (u'Информация о матче',  {'fields':['command_first','command_second','datetime','parse','score','status','stadium','champ','tour']}),
    ] + page_fields
    search_fields = ['command_first__name','command_second__name']
    list_filter = ['champ','stadium','datetime']
    
admin.site.register(Match,MatchAdmin)


#Новости
class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Новость',  {'fields':['name','date','img','category',]}),] + page_fields
#
admin.site.register(News,NewsAdmin)

#Импортируемые скрипты
class ScriptsAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','content',]}),]
#
admin.site.register(Scripts,ScriptsAdmin)


