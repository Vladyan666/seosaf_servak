# -*- coding: utf-8 -*-
import datetime
import re, sys, os
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.contrib.sitemaps import Sitemap
from django.core.mail import send_mail, BadHeaderError
from django.template import RequestContext, loader
from django.db.models import Q


# импортируем модели
from .models import TextPage, Team, Champ, Group, City, Stadium, Zayavka, Match, Player, Teamchamp, News, Scripts

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias

def default_context(request,alias,object):
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    data = get_object_or_404(object, alias=alias)
    scripts = Scripts.objects.all()
    active_url = active_menu(request)
    datee = datetime.datetime.now()
    zone = timezone.now()
    
    context_object = {
        'menu' : menu,
        'data' : data,
        'active_url' : active_url,
        'scripts' : scripts,
        'datee' : datee,
        'zone' : zone,
    }
    
    return context_object

#Описание модели  
def index(request):
    template = loader.get_template('index.html')

    context_data = default_context( request , "index" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    livematches = Match.objects.order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    news = News.objects.order_by('date')
    
    context_data.update({
        'matches' : matches,
        'livematches' : livematches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'news' : news,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def index2(request):
    template = loader.get_template('index2.html')

    context_data = default_context( request , "newindex" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    livematches = Match.objects.order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    news = News.objects.order_by('date')
    
    context_data.update({
        'matches' : matches,
        'livematches' : livematches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'news' : news,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def match2(request):
    template = loader.get_template('newmatch.html')

    context_data = default_context( request , "newindex" , TextPage )
    m = Match.objects.filter(status="Матч окончен").order_by('?')[0]
    teamchamp = Teamchamp.objects.order_by('mesto')
    news = News.objects.order_by('date')
    
    context_data.update({
        'm' : m,
        'teamchamp' : teamchamp,
        'news' : news,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def team2(request):
    template = loader.get_template('newteam.html')

    context_data = default_context( request , "newindex" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    livematches = Match.objects.order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    news = News.objects.order_by('date')
    
    context_data.update({
        'matches' : matches,
        'livematches' : livematches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'news' : news,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def champ2(request):
    template = loader.get_template('newchamp.html')

    context_data = default_context( request , "newindex" , TextPage )
    matches = Match.objects.order_by('datetime')
    champ_alias = 'champ'
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    teamchamp = Teamchamp.objects.order_by('name')
    group = Group.objects.filter(champ__name=u"Лига чемпионов 2016/2017 групповой этап")
    group_table = []
    for group_item in group :
        teams = Group.objects.filter(champ__name=u"Лига чемпионов 2016/2017 групповой этап")
        temp_teams = []
        for team in teams :
            temp_teams.append(team.teams)
        temp_teams = list(set(temp_teams))
        group_table.append([group_item,temp_teams])
#        group_table = list(set(group_table))
        
    context_data.update({
        'matches' : matches,
        'teams' : teams,
        'champ_alias' : champ_alias,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'teamchamp' : teamchamp,
        'group' : group,
        'group_table' : group_table,
    })
    
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))
    
def textpage(request,textpage_alias):
    template = loader.get_template('textpage.html')
    
    context_data = default_context( request , textpage_alias , TextPage )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def teams(request):
    template = loader.get_template('teams.html')

    context_data = default_context( request , "teams" , TextPage )
    teams = Team.objects.filter(techteam=False).order_by('name')
    context_data.update({
        'teams' : teams,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))

def team(request,team_alias):
    template = loader.get_template('team.html')

    context_data = default_context( request , team_alias , Team )

    teams_alias = TextPage.objects.get(alias='teams')
    #teams_alias = get_host_vs_url( request.get_full_path() , 'teams' )
    #parent_textpage = TextPage.objects.get(alias=teams_alias,host=context_data['host_name'])

    #teams_alias = get_alias_related('teams')
    # if context_data['host_name'] == 'confcuptickets.ru' :
        # teams_alias = 'comands'

    groups = Group.objects.filter(teams=context_data['data'])
    matches = Match.objects.filter(Q(command_first=context_data['data']) | Q(command_second=context_data['data'])).order_by('datetime')
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    players = Player.objects.filter(team=context_data['data'])
    coaches = Player.objects.filter(Q(position="тренер") | Q(position="Тренер"), team=context_data['data'])
    goalkeepers = Player.objects.filter(Q(position="вратарь") | Q(position="Вратарь"), team=context_data['data'])
    defenders = Player.objects.filter(Q(position="защитник") | Q(position="Защитник"), team=context_data['data'])
    midfielders = Player.objects.filter(Q(position="полузащитник") | Q(position="Полузащитник"), team=context_data['data'])
    attacks = Player.objects.filter(Q(position="нападающий") | Q(position="Нападающий"), team=context_data['data'])
    teamchamp = Teamchamp.objects.order_by('name')
    news = News.objects.order_by('date')[:5]

    context_data.update({
        'groups' : groups,
        'matches' : matches,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'players' : players,
        'coaches' : coaches,
        'goalkeepers' : goalkeepers,
        'defenders' : defenders,
        'midfielders' : midfielders,
        'attacks' : attacks,
        'teamchamp' : teamchamp,
        'news' : news,
        
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))

def matches(request):
    template = loader.get_template('matches.html')

    context_data = default_context( request , "matches" , TextPage )
    matches = Match.objects.order_by('datetime')
    stadiums = Stadium.objects.order_by('name')
    teamchamp = Teamchamp.objects.order_by('name')

    context_data.update({
       'matches' : matches,
       'stadiums' : stadiums,
       'teamchamp' : teamchamp,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))

def match(request, match_alias):
    template = loader.get_template('match.html')

    context_data = default_context( request , match_alias , Match )
    
    #matches_alias = TextPage.objects.get(alias='matches')
    matches_alias = 'matches'
    # if context_data['host_name'] == 'confcuptickets.ru' :
        # matches_alias = 'matchi'

    breadcrumbs = [TextPage.objects.get(alias=matches_alias)]
    # for text in context_data['data'].text :
        # try :
            # try :
                # player = str(Player.objects.filter(Q(team=context_data['data'].command_first)|Q(team=context_data['data'].command_second)).get(name=text['player']).id)
                # text['player'] = player
            # except KeyError :
                # pass
        # except Player.DoesNotExist:
            # pass

        #


    try:
        next = context_data['data'].get_next_by_datetime()
    except Match.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_datetime()
    except Match.DoesNotExist:
        prev = None

    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'next' : next,
        'prev' : prev,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))
    

def updater(request):
    try :
        object_name = re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',request.GET['type']).strip()
    except KeyError :
        return HttpResponse(u'Модель не определена!')

    sys.path.append(os.path.join(settings.BASE_DIR, 'scripts'))
    import updater

    result = updater.init( object_name )
    return HttpResponse(result)

def get_birth( data ):
    if data.birth.year == 1 :
        return u"Нет данных"
    else :
        return data.birth

def player(request, player_alias):
    template = loader.get_template('player.html')

    context_data = default_context( request , player_alias , Player )

    teams_alias = 'teams'
    # if context_data['host_name'] == 'confcuptickets.ru' :
        # teams_alias = 'comands'

    breadcrumbs = [TextPage.objects.get(alias=teams_alias),context_data['data'].team]
    birth = get_birth( context_data['data'] )

    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'birth' : birth,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))

def champs(request):
    template = loader.get_template('champs.html')
    context_data = default_context( request , "champs" , TextPage )
    champ = Champ.objects.order_by('name')
    
    context_data.update({
        'champ' : champ,
    })
    
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))

def champ(request, champ_alias):
    template = loader.get_template('champ-season.html')

    context_data = default_context( request , champ_alias , Champ )
    matches = Match.objects.order_by('datetime')
    champ_alias = 'champ'
    new_matches = matches.filter(datetime__gt=timezone.now())
    old_matches = matches.filter(datetime__lte=timezone.now())
    teamchamp = Teamchamp.objects.order_by('name')
    group = Group.objects.filter(champ__name=u"Лига чемпионов 2016/2017 групповой этап")
    group_table = []
    for group_item in group :
        teams = Group.objects.filter(champ__name=u"Лига чемпионов 2016/2017 групповой этап")
        temp_teams = []
        for team in teams :
            temp_teams.append(team.teams)
        temp_teams = list(set(temp_teams))
        group_table.append([group_item,temp_teams])
#        group_table = list(set(group_table))
        

    context_data.update({
        'matches' : matches,
        'teams' : teams,
        'champ_alias' : champ_alias,
        'new_matches' : new_matches,
        'old_matches' : old_matches,
        'teamchamp' : teamchamp,
        'group' : group,
        'group_table' : group_table,
    })
    
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))
    
def allnews(request):
    template = loader.get_template('news.html')
    context_data = default_context( request, "news", TextPage )
    news = News.objects.order_by('date')[:50]

    context_data.update({
        'news' : news,
    })
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))

def news(request, news_alias):
    template = loader.get_template('news_item.html')
    context_data = default_context( request , news_alias , News )
    news = News.objects.order_by('date')[:50]
    context = RequestContext(request, context_data)
    try:
        next = context_data['data'].get_next_by_date()
    except News.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_date()
    except News.DoesNotExist:
        prev = None
    
    context_data.update({
        'next' : next,
        'prev' : prev,
        'news' : news,
    })
    return HttpResponse(template.render(context))
    
def teamchamp(request,teamchamp_alias):
    template = loader.get_template('champteam.html')

    context_data = default_context( request , teamchamp_alias , Teamchamp )

#    teamchamp_alias = TextPage.objects.get(alias='seasons')
    #teams_alias = get_host_vs_url( request.get_full_path() , 'teams' )
    #parent_textpage = TextPage.objects.get(alias=teams_alias,host=context_data['host_name'])

    #teams_alias = get_alias_related('teams')
    # if context_data['host_name'] == 'confcuptickets.ru' :
        # teams_alias = 'comands'
	
    breadcrumbs = [TextPage.objects.get(alias=teamchamp_alias)]
    champ = Champ.objects.get(champ = champ.name)
    groups = Group.objects.filter(teams=context_data['data'].team)
    matches = Match.objects.filter(Q(command_first=context_data['data'].team) | Q(command_second=context_data['data'].team))
    players = Player.objects.filter(team=context_data['data'].team)
    coaches = Player.objects.filter(Q(position="тренер") | Q(position="Тренер"), team=context_data['data'].team)
    goalkeepers = Player.objects.filter(Q(position="вратарь") | Q(position="Вратарь"), team=context_data['data'].team)
    defenders = Player.objects.filter(Q(position="защитник") | Q(position="Защитник"), team=context_data['data'].team)
    midfielders = Player.objects.filter(Q(position="полузащитник") | Q(position="Полузащитник"), team=context_data['data'].team)
    attacks = Player.objects.filter(Q(position="нападающий") | Q(position="Нападающий"), team=context_data['data'].team)

    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'groups' : groups,
        'matches' : matches,
        'players' : players,
        'coaches' : coaches,
        'goalkeepers' : goalkeepers,
        'defenders' : defenders,
        'midfielders' : midfielders,
        'attacks' : attacks,
        'champ' : champ,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))
    
def tablica(request):
    template = loader.get_template('table.html')

    context_data = default_context( request , "scoretable" , TextPage )
    teamchamp = Teamchamp.objects.order_by('mesto')

    context_data.update({
       'teamchamp' : teamchamp,
    })

    context = RequestContext(request, context_data)

    return HttpResponse(template.render(context))
    
def matches_today(request):
    template = loader.get_template('matches_today.html')

    context_data = default_context( request , "matches-today" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    livematches = Match.objects.order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    
    context_data.update({
        'matches' : matches,
        'livematches' : livematches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def matches_pars(request):
    template = loader.get_template('matches_pars.html')

    context_data = default_context( request , "matches-pars" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    livematches = Match.objects.order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    
    context_data.update({
        'matches' : matches,
        'livematches' : livematches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def seo_yadro(request):
    template = loader.get_template('seo_yadro.html')

    context_data = default_context( request , "seo-yadro" , TextPage )
    matches = Match.objects.filter(champ__year="2016").order_by('datetime')
    teamchamp = Teamchamp.objects.order_by('mesto')
    new_matches = matches.filter(datetime__gt=timezone.now())
    
    context_data.update({
        'matches' : matches,
        'teamchamp' : teamchamp,
        'new_matches' : new_matches,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
  
    
@csrf_exempt 
def postupdater(request): 
    try :
        object_name_m = re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',request.POST['type']).strip()
    except KeyError :
        return HttpResponse(u'Модель не определена!')
     
    sys.path.append(os.path.join(settings.BASE_DIR, 'scripts'))
    import updater
    result = ''
    
    object_name_arr = object_name_m.split(',')
    write_arr = request.POST['write'].encode('utf-8').split('@@')
    
    i = 0
    for object_name in object_name_arr :
    
        write = 'break\n'
        
        for wr in write_arr[i].split('::') :
            write += wr+'\n'
            
        data = open(os.path.join(settings.BASE_DIR, 'scripts')+'/pr/'+object_name+'.pr' , 'w')
        data.write(write)
        data.close()
        
        result += updater.init( object_name )
        
        i += 1
    return HttpResponse(result)    
    
  
def get_field_value( object , field ):
    from django.db.models.fields import IntegerField, DateField, DateTimeField
    from django.db.models.fields.files import ImageField
    from django.contrib.postgres.fields.jsonb import JSONField
    import json

    value = eval("object."+field)
    field_type = "other"
    
    if type(value) == bool :
        field_type = "bool"
        if value :
            value = "1"
        else :
            value = "0"
        
    elif isinstance( object._meta.get_field(field) , IntegerField ) : 
        field_type = "int"
        if value :
            value = str( value )
        else :
            value = "0"
            
    elif isinstance( object._meta.get_field(field) , DateField ) : 
        field_type = "date"
        if value :
            value = str( value.strftime("%d.%m.%Y,%H:%M") )
        else :
            value = "01.01.1900"
            
    elif isinstance( object._meta.get_field(field) , DateTimeField ) : 
        field_type = "datetime"
        if value :
            value = str( value.strftime("%d.%m.%Y,%H:%M") )
        else :
            value = "01.01.1900,00:00"
            
    elif isinstance( object._meta.get_field(field) , ImageField ) : 
        field_type = "image"
        if value :
            value = str( value ).split("/")[-1]
        else :
            value = ""
    
    elif isinstance( object._meta.get_field(field) , JSONField ) :
        field_type = "json"
        value = json.dumps(value , ensure_ascii = False , encoding='utf8')
    
    elif type(value) == unicode or type(value) == str :
        field_type = "str"
        value = value.encode('utf8').replace('"', '\\"')
    
    else :
        if value :
            value = str( value ).replace('\n', ' ')
        else :
            value = ""
        
    return [field , value , field_type]

def export(request,export_type):
    template = loader.get_template('export.html')
    if export_type == 'team' :
        object = Team
    elif export_type == 'match' :
        object = Match
    elif export_type == 'player' :
        object = Player
    elif export_type == 'stadium' :
        object = Stadium
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'fed' :
        object = Fed
    elif export_type == 'group' :
        object = Group
    elif export_type == 'textpage' :
        object = TextPage
    else :
        object = Team
    fields = [field.name for field in object._meta.fields]
    
    objects = object.objects.all()

    data = []
    for object in objects :
        item = []
        for field in fields :
            if field != 'id' and field != 'content' :
                field_param = get_field_value( object , field ) # [field , value , type]
                item.append(field_param)
        data.append(item)
            
    context = RequestContext(request, {
        'objects' : objects,
        'fields' : fields,
        'data' : data,
    })
    
    return HttpResponse(template.render(context) , content_type="text/plain;charset=utf-8")

class StadiumSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Stadium.objects.all()
        
class GroupSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Group.objects.all()
        
class FedSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Fed.objects.all()
        
class ZayavkaSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Zayavka.objects.all()

class ChampSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Champ.objects.all()
    
class PlayerSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Player.objects.all()
    
class MatchSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Match.objects.all()
    
class TextPageSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return TextPage.objects.all()    
    
class TeamSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Team.objects.all()
        
class NewsSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return News.objects.all()
    
def error_404(request):
    template = loader.get_template('404.html')
    context_data = default_context( request , match_alias , TextPage )
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    
    context_data.update({
        'menu' : menu,
    })
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")
    
def mywot(request):
    return render_to_response('mywotf3631b47ebba1ef5a0c0.html', content_type="text/plain")

def yandex_verification(request,yandex_verification_number):
    template = loader.get_template('yandex_verification.html')
    context = RequestContext(request,{
        'yandex_verification_number' : yandex_verification_number,
    })
    return HttpResponse(template.render(context))
    
def google_verification(request,google_verification_number):
    template = loader.get_template('google_verification.html')
    context = RequestContext(request,{
        'google_verification_number' : google_verification_number,
    })
    return HttpResponse(template.render(context))

def mail_verification(request,mail_verification_number):
    template = loader.get_template('mail_verification.html')
    context = RequestContext(request,{
        'mail_verification_number' : mail_verification_number,
    })
    return HttpResponse(template.render(context))
    
def ajaxcontroller(request):
    if request.is_ajax():
        new_min = 'ajax:no-method'
        if request.POST['type'] == 'mins':
            #match = Match.objects.get(alias='match1')
            #this_url = 'match1'
            #this_url = request.POST['id']
            #request.POST['id'] = 'match1'
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.minute
            
        elif request.POST['type'] == 'score':
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.score
            
        elif request.POST['type'] == 'content':
            if request.user.is_authenticated():
                match = Match.objects.get(alias=request.POST['id'])
                match.content = request.POST['this_cont']
                match.save()
                MyValue = match.content
            else:
                MyValue = u'Неавторизован'
                
        return HttpResponse(MyValue)
    else:
        raise Http404
        