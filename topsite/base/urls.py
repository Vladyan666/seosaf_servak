from django.conf.urls import url
import django
from django.contrib import admin
from . import views
from .views import TeamSitemap, NewsSitemap, TextPageSitemap, MatchSitemap, StadiumSitemap, PlayerSitemap, ChampSitemap, GroupSitemap, ZayavkaSitemap

sitemaps = {
	'team': TeamSitemap,
	'news': NewsSitemap,
	'textpage': TextPageSitemap,
	'match': MatchSitemap,
#	'stadium': StadiumSitemap,
	'player': PlayerSitemap,
	'champ': ChampSitemap,
#	'group': GroupSitemap,
#	'zayavka': ZayavkaSitemap,
}

urlpatterns = [
    url(r'^ajax/$', views.ajaxcontroller, name='ajax'),
    url(r'^$', views.index, name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^teams/$', views.teams, name='teams'),
    url(r'^news/$', views.allnews, name='news'),
    url(r'^teams/(?P<team_alias>[0-9a-z\-]+)/$', views.team, name="team"),
    url(r'^seasons/(?P<teamchamp_alias>[0-9a-z\-]+)/$', views.teamchamp, name="teamchamp"),
    url(r'^updater/$', views.updater, name='updater'),
    url(r'^postupdater/$', views.postupdater, name='postupdater'),
#    url(r'^fc/$', views.fc, name='fc'),
#    url(r'^contacts/$', views.contacts, name='contacts'),
#    url(r'^kontacty/$', views.contacts, name='contacts'),
#    url(r'^teamsjson/$', views.teamsjson, name='teamsjson'),
    url(r'^matches/$', views.matches, name='matches'),
    url(r'^scoretable/$', views.tablica, name='scoretable'),
    url(r'^matches-today/$', views.matches_today, name='matches-today'),
    url(r'^matches-pars/$', views.matches_pars, name='matches-pars'),
    url(r'^seo-yadro/$', views.seo_yadro, name='seo-yadro'),
    url(r'^newindex/$', views.index2, name='newindex'),
    url(r'^newmatch/$', views.match2, name='newmatch'),
    url(r'^newteam/$', views.team2, name='newteam'),
    url(r'^newchamp/$', views.champ2, name='newchamp'),
    url(r'^champs/$', views.champs, name='champs'),
#    url(r'^stadiums/$', views.stadiums, name='stadiums'),
#    url(r'^ajaxcontroller/$', views.ajax, name='ajax'),
    url(r'^export/(?P<export_type>[0-9a-z\-]+)/$', views.export, name='export'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap' , {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^mywotf3631b47ebba1ef5a0c0\.html$', views.mywot, name='mywot'),
    url(r'^yandex_(?P<yandex_verification_number>[0-9a-z]+)\.html$', views.yandex_verification, name='yandex_verification'),
    url(r'^google(?P<google_verification_number>[0-9a-z]+)\.html$', views.google_verification, name='google_verification'),
    url(r'^wmail_(?P<mail_verification_number>[0-9a-z]+)\.html$', views.mail_verification, name='mail_verification'),
    url(r'^(?P<textpage_alias>[0-9a-z\-]+)/$', views.textpage, name='textpage'),
    url(r'^news/(?P<news_alias>[0-9a-z\-]+)/$', views.news, name="news"),
    url(r'^teams/(?P<team_alias>[0-9a-z\-]+)/$', views.team, name="team"),
    url(r'^matches/(?P<match_alias>[0-9a-z\-]+)/$', views.match, name="match"),
    url(r'^players/(?P<player_alias>[0-9a-z\-]+)/$', views.player, name="player"),
#    url(r'^stadiums/(?P<stadium_alias>[0-9a-z\-]+)/$', views.stadium, name="stadium"),
    url(r'^champ/(?P<champ_alias>[0-9a-z\-]+)/$', views.champ, name="champ"),
#    url(r'^group/(?P<group_alias>[0-9a-z\-]+)/$', views.group, name="group"),
#   url(r'^zayavka/(?P<zayavka_alias>[0-9a-z\-]+)/$', views.zayavka, name="zayavka"),
]

handler404 = 'base.views.error_404'