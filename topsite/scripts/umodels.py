# -*- coding: utf8 -*-
############################################################################
import pytils, datetime, pytz
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.conf import settings

# импортируем нужные модели
from base.models import Team, Champ, Group, City, Stadium, Zayavka, Player, Match, Teamchamp

# log записи 
def log_locale(key,locale='en'):
    log_string = {
        "no_object_in_db" : {"ru" : "Объект в базе не найден: ", "en" : "DB does not have this object: "},
    }
    return log_string[key][locale].encode('utf8')
    
# получить время в нужном формате
def get_datetime( str , format="%d.%m.%Y" ):
    try :
        return timezone.make_aware(datetime.datetime.strptime(str.strip(), format), timezone.get_current_timezone())
    except ValueError :
        print "Date is not set: " + str.strip().encode('utf8')
        return timezone.make_aware(datetime.datetime.strptime("01.01.1990", "%d.%m.%Y"), timezone.get_current_timezone())

# получаем объект, если существует
def get_object( object , field_id , value  ):
    try:
        return eval("object.objects.get("+field_id+"=value)")
    except object.DoesNotExist as detail:
        print log_locale('no_object_in_db') + value.encode('utf8') , detail
        return None
        
def get_field_value( object , field ):
    from django.db.models.fields import IntegerField, DateField, DateTimeField
    from django.db.models.fields.files import ImageField

    value = eval("object."+field)
    
    if type(value) == bool :
        if value :
            value = "1"
        else :
            value = "0"
        
    elif isinstance( object._meta.get_field(field) , IntegerField ) : 
        if value :
            value = str( value )
        else :
            value = "0"
            
    elif isinstance( object._meta.get_field(field) , DateField ) : 
        if value :
            value = value.strftime("%d.%m.%Y")
        else :
            value = "01.01.1900"
            
    elif isinstance( object._meta.get_field(field) , DateTimeField ) : 
        if value :
            value = value.strftime("%d.%m.%Y,%H:%M")
        else :
            value = "01.01.1900,00:00"
            
    elif isinstance( object._meta.get_field(field) , ImageField ) : 
        if value :
            value = str( value ).split("/")[-1]
        else :
            value = ""
    
    elif type(value) == unicode or type(value) == str :
        value = value.encode('utf8').replace('"', '\\"')
    
    else :
        if value :
            value = value.name.encode('utf8')
        else :
            value = ""
        
    return value
        
def get_foreign_key(  json_data , error , field , name_field, object  ):
    try :
        temp = get_object( object , name_field , json_data[field] )
        if temp :
            json_data[field] = temp
        else :
            error = True
    except KeyError :
        pass
# get_foreign_key( json_data , error , 'mail' , 'mail', Mails  )
    
def set_sostav_player( json_data , field_name ):
    try :
        sostav = json_data[field_name]
        for player in sostav :
            temp = get_object( Player , 'name' , player['player'].strip() )
            if temp :
                player['alias'] = temp.alias
                player['id'] = temp.id
                player['image'] = str(temp.image)
                player['weight'] = temp.weight
                player['height'] = temp.height
    except KeyError :
        pass        
        
        
############################################################################

def get_teamsnames_table( key ):
    tables = {
        "name" : [
            ["Кот-д'Ивуар","Кот-д’Ивуар (Берег Слоновой Кости)"],
            ["Рубен Рочина","РубенРочина"],
            ["Корея","Южная Корея"],
            ["Чили","Чили, о. Пасхи"],
            ["Нидерланды","Нидерланды (Голландия)","Голландия"],
            ["Эквадор","Эквадор, Галапагосы",],
            ["Сербия","Сербия и Черногория"],
			["Пауль Агиляр", "Пауль Агилар"],
			["Хесус Корона", "Хосе де Хесус Корона"],
			["Ирвинг Лосано", "Ирвин Лосана"],
			["Мигель Лайюн", "Мигель Лаюн"],
			["Хорхе Торрес", "Хорхе Торрес Нило"],
			["Джейсон Джерия", "Джейсон Герия"],
			["Алекс Герсбах", "Александер Герсбах"],
			["Тим Кэйхилл", "Тим Кэхилл"],
			["Райан Макгоуэн", "Райан Макгован"],
			["Мэтью Лекки", "Мэтью Леки"],
			["Мэттью Райан", "Мэтью Райан"],
			["Том Рогич", "Томас Рогич"],
			["Марк Миллиган", "Марк Миллигэн"],
			["Трент Сэйнсбери", "Трент Сэйнсбари"],
        ],
        "eng_name" : [
            ["Cote d'Ivoire (Ivory Coast)","Côte d'Ivoire", "Cote d'Ivoire"],
            ["Macedonia","FYR Macedonia"],
            ["Democratic Republic of the Congo","Congo DR"],
            ["Cape Verde","Cape Verde Islands"],
            ["China","China PR"],
            ["Taiwan (Taipei)","Chinese Taipei"],
            ["Curacao","Curaçao"],
            ["The Turks and Caicos Islands","Turks and Caicos Islands"],
            ["Reunion","Réunion"],
            ["Saint Kitts and Nevis","St. Kitts and Nevis"],
            ["Ireland","Republic of Ireland"],
            ["Sao Tome and Principe","São Tomé e Príncipe","Sao Tome e Principe"],
            ["French Polynesia","Tahiti"],
            ["Republic of South Africa","South Africa"],
            ["Palestinian Territories","Palestine"],
            ["Qatar","Quatar"],
            ["Myanmar (Burma)","Myanmar"],
            ["Macau","Macao"],
            ["Korea","Korea Republic"],
            ["Republic of the Congo","Congo"],
            ["United States Virgin Islands","US Virgin Islands"],
            ["East Timor","Timor-Leste"],
            ["Saint Lucia","St. Lucia"],
            ["Saint Vincent and the Grenadines","St. Vincent and the Grenadines", "San Vicente and Granadines"],
            ["North Korea","Korea DPR"],
			["Thomas Muller","Thomas Muller"],
			["Mesut Ozil","Mesut Ozil"],
			["Mario Gotze","Mario Gotze"],
			["Leroy Sane","Leroy Sane"],
			["Mario Gomez","Mario Gomez"],
			["Andre Schurrle","Andre Schurrle"],
			["Benedikt Howedes","Benedikt Howedes"],
			["Jerome Boateng","Jerome Boateng"],
			["Marc-Andre ter Stegen","Marc-Andre ter Stegen"],

        ],
        "code" : [
            ['FRO','FAR'],
            ['LVA','LAT'],
            ['MNE','MGR'],
            ['ROU','ROM'],
            ['SRB','SCG'],
            ['EQG','GNQ','GEQ'],
            ['GNB','GBS'],
            ['LBY','LBA'],
            ['NGA','NGR'],
            ['SDN','SUD'],
            ['BHR','BRN'],
            ['IDN','INA'],
            ['IRN','IRI'],
            ['SVN','SLO'],
            ['BFA','BUR'],
            ['BRB','BAR'],
            ['BLZ','BIZ'],
            ['VGB','IVB'],
            ['SLV','ESA'],
            ['MSR','MNT'],
            ['TCA','TKS'],
            ['VIR','ISV'],
        ],
    }
    if key in tables :
        return tables[key]
    else :
        return []
        
def get_bool_value( json_data , field ):
    try :
        if json_data[field] and json_data[field] != "0" :
            json_data[field] = True
        else :
            json_data[field] = False
    except KeyError :
        json_data[field] = False
        pass
# проверка в таблице соответствия        
def get_true_name( json_data , object ,  main_field , field_in_object ):    
    if not get_object( object , field_in_object , json_data[main_field] ) :
        new_name = ""
        name_replace = get_teamsnames_table( field_in_object )
        for name in name_replace :
            try :
                it = name.index( json_data[main_field].encode('utf8') )
                json_data[main_field] = name[0]
            except ValueError :
                pass
                
# добавление в коллекцию каринок
def set_new_image( json_data , object , main_field , field ): 
    try :
        if get_object( object , main_field , json_data[main_field] ) :
            if eval("get_object( object , main_field , json_data[main_field] )."+field) :
                images = eval("get_object( object , main_field , json_data[main_field] )."+field+".split(',')")
                if not json_data[field] in images :
                    json_data[field] = eval("get_object( object , main_field , json_data[main_field] )."+field)+','+json_data[field]
                else :
                    json_data[field] = eval("get_object( object , main_field , json_data[main_field] )."+field)
        else :
            json_data[field] = ""
    except KeyError :
        if not get_object( object , main_field , json_data[main_field] ) :
            json_data[field] = ""
        pass

##########################  Модели  ##################################################

def set_default( json_data ):
    get_bool_value( json_data , "menushow" )
    get_bool_value( json_data , "sitemap" )


# задать Запрос
def set_query( json_data , object ):
    if json_data['pub_date'] :
        format_datetime = get_datetime( json_data['pub_date'] , format="%m/%d/%Y %H:%M:%S" )        

    error = False
    # форматирование полей        
    try :
        format_datetime = get_datetime( json_data['pub_date'] , format="%m/%d/%Y %H:%M:%S" ) 
        json_data["pub_date"] = format_datetime
        json_data["alias"] = slugify(pytils.translit.translify(format_datetime.strftime('%d-%m-%Y-%H-%M-')+json_data['query']))
    except KeyError :
        pass
        
    try :
        temp = get_object( Project , 'project' , json_data['project'] )
        if temp :
            json_data["project"] = temp
        else :
            error = True
    except KeyError :
        pass
     
    # обновление или запись нового объекта
    if error :
        object_update_or_create = None
    else :
        object_update_or_create = object.objects.update_or_create(
            query=json_data['query'] , 
            defaults=json_data
        )
    
    return object_update_or_create

# задать Стадион        
def set_stadium( json_data , object ):
    # форматирование полей
    try :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
    except KeyError :
        pass
        
    try :
        json_data["image"] = settings.MEDIA_URL + "stadiums/" + json_data['image']
    except KeyError :
        pass
        
    object_update_or_create = object.objects.update_or_create(
        name = json_data['name'] ,
        defaults=json_data
    )
    return object_update_or_create
    
def set_balance( json_data , object ):
    # форматирование полей
    try :
        format_datetime = get_datetime( json_data['pub_date'] , format="%m/%d/%Y %H:%M:%S" )
        json_data['pub_date'] = format_datetime
    except KeyError :
        pass
    json_data['phone'] = get_object( Phones , 'phone' , json_data['phone'] )
    if json_data['phone'] :
        object_update_or_create = object.objects.update_or_create(
            phone = json_data['phone'] ,
            pub_date = json_data['pub_date'] ,
            defaults =json_data
        )
    else :
        object_update_or_create = None
        
    return object_update_or_create
    
    
def get_team( json_data , object ):
    team = get_object( object , 'name' , json_data['name'] )
    fields = [field.name for field in object._meta.fields]
    
    result = None
    
    if team :
        result = "{ "
        num = 0
        for field in fields :
            if num > 0 :
                if num > 1 :
                    result += " , "

                field_value = get_field_value( team , field )
                result += '"'+field+'"' + ' : ' + '"'+field_value+'"'
            num += 1
        
        result += " }"
    
    return result

    
# задать Команду        
def set_team( json_data , object ):
    main_field = 'name'
    error = False
    # форматирование полей
    set_default( json_data )
    
    try :
        json_data["code"] = json_data["code"].upper()
    except KeyError :
        pass
            
    try :
        temp = get_object( Stadium , 'name' , json_data['stadium'] )
        # if temp :
        json_data["stadium"] = temp
        # else :
            # error = True
    except KeyError :
        pass
        
    # техническая команда
    get_bool_value( json_data , "techteam" )
    # try :
        # if json_data["techteam"] and json_data["techteam"] != "0" :
            # json_data["techteam"] = True
        # else :
            # json_data["techteam"] = False
    # except KeyError :
        # json_data["techteam"] = False
        # pass
        
      
    # проверка в таблице соответствия
    try :
        get_true_name( json_data , object , main_field , main_field )
    except KeyError :
        pass  
        
    try :
        if not get_object( object , main_field , json_data[main_field] ) :
            json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
    except KeyError :
        pass
    
    # коллекции картинок
    set_new_image( json_data , object , main_field , 'logo' )
        
    set_new_image( json_data , object , main_field , 'flag' )
        
    set_new_image( json_data , object , main_field , 'photo' )
        
    set_new_image( json_data , object , main_field , 'forma' )
        
    if error :
        object_update_or_create = None
    else :  
        object_update_or_create = eval("object.objects.update_or_create( "+main_field+" = json_data[main_field] , defaults=json_data )")
    return object_update_or_create
    
# задать Текстовую страницу        
def set_textpage( json_data , object ):
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    object_update_or_create = object.objects.update_or_create(
        name = json_data['name'] ,
        defaults=json_data
    )
    return object_update_or_create
    
# задать Новость        
def set_news( json_data , object ):
    del json_data["type"]
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    try :
        format_datetime = get_datetime( json_data['date'] , format="%d.%m.%Y" )
        json_data['date'] = format_datetime
    except KeyError :
        pass
        
    object_update_or_create = object.objects.update_or_create(
        name = json_data['name'] ,
        defaults=json_data
    )
    return object_update_or_create
    
# задать Федерацию       
def set_fed( json_data , object ):
    error = False
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    try :
        json_data["image"] = settings.MEDIA_URL + "feds/" + json_data['image']
    except KeyError :
        pass
    
    if error :
        object_update_or_create = None
    else :     
        object_update_or_create = object.objects.update_or_create(
            name = json_data['acronym'] ,
            defaults=json_data
        )
    return object_update_or_create
    
# задать Чемпионат       
def set_champ( json_data , object ):
    error = False
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    try :
        temp = get_object( Fed , 'acronym' , json_data['fed'] )
        if temp :
            json_data["fed"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    try :
        json_data["image"] = settings.MEDIA_URL + "champs/" + json_data['image']
    except KeyError :
        pass    
    
    if error :
        object_update_or_create = None
    else :     
        object_update_or_create = object.objects.update_or_create(
            name = json_data['name'] ,
            defaults=json_data
        )
    return object_update_or_create
    
# задать Группу       
def set_group( json_data , object ):
    error = False
    #del json_data["teams"]
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    try :
        if json_data["qualifying"] :
            json_data["qualifying"] = True
        else :
            json_data["qualifying"] = False
    except KeyError :
        json_data["qualifying"] = False
        pass
        
    try :
        temp = get_object( Champ , 'name' , json_data['champ'] )
        if temp :
            json_data["champ"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    if error :
        object_update_or_create = None
    else :
        object_update_or_create = object.objects.update_or_create(
            name = json_data['name'] ,
            champ = json_data["champ"] ,
            qualifying = json_data["qualifying"] ,
        )
    
        # сделать добавление manytomany
        try :   
            teams = json_data['teams'].split(',')
            for team in teams :
                temp = get_object( Team , 'name' , team )
                if temp :
                    json_data["group"] = temp
                    object_update_or_create[0].teams.add(temp)
                else :
                    print log_locale('no_object_in_db') + ' Team(' + team.encode('utf8') + ')'
                
        except KeyError :
            pass
    
    return object_update_or_create
    
def get_player( json_data , object ):
    teams = Champ.objects.get(name="Чемпионат России 2016/2017").teams.all()
    fields = [field.name for field in object._meta.fields]
    result = ""
    for team in teams :
        players = object.objects.filter(team=team)
        
        for player in players :
            #team = get_object( object , 'name' , json_data['name'] , json_data['host'] )

            
            
            if player :
                result += "{ "
                num = 0
                for field in fields :
                    if num > 0 :
                        if num > 1 :
                            result += " , "

                        field_value = get_field_value( player , field )
                        result += '"'+field+'"' + ' : ' + '"'+field_value+'"'
                    num += 1
                
                result += " }\n"
    
    return result 
    
def set_player( json_data , object ):
    error = False
    main_field = "name"
    # форматирование полей
    set_default( json_data )
    #del json_data["name_eng"]
    
    try :
        format_datetime = get_datetime( json_data['birth'] , format="%d.%m.%Y" )
        json_data['birth'] = format_datetime
    except KeyError :
        pass
        
    try :
        json_data["position"] = json_data["position"].lower()
    except KeyError :
        pass
        
    try :
        json_data["image"] = settings.MEDIA_URL + "players/" + json_data['image']
    except KeyError :
        pass
        
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.slugify(json_data['name']))

    # проверка в таблице соответствия
    try :
        get_true_name( json_data , object , main_field , main_field )
    except KeyError :
        pass 
    try :
        get_true_name( json_data , Team , "team" , "name" )
    except KeyError :
        pass    
        
    try :
        temp = get_object( Team , 'name' , json_data['team'] )
        if temp :
            json_data["team"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    # запись заявки   
    zayavka = False
    try :
        temp = get_object( Champ , 'name' , json_data['champ'] )
        if temp :
            json_data["champ"] = temp
            
            zayavka_data = {'alias' : slugify(pytils.translit.slugify(json_data['champ'].name+'-'+json_data['team'].name)) }
            
            zayavka_object_update_or_create = Zayavka.objects.update_or_create(
                champ = json_data['champ'], 
                team = json_data["team"],
                defaults=zayavka_data
            )
            zayavka = zayavka_object_update_or_create
        del json_data["champ"]
            
    except KeyError :
        pass
     
    # обновление или запись нового объекта
    if error :
        object_update_or_create = None
    else :
        #print json_data
        object_update_or_create = object.objects.update_or_create(
            name = json_data[main_field], 
            team = json_data["team"],
            defaults=json_data
        )
        
        if zayavka :
            zayavka[0].players.add(object_update_or_create[0])
        
    return object_update_or_create
    
    
# заявка для структуры     { "team" : "Южная Корея" , "champ" : "Чемпионат мира 2014" , "players" : [{ "name" : "Майл Единак", "position" : "полузащитник", "birth" : "03.08.1984", "team" : "Австралия", "height" : "189", "weight" : "81", "image" : "majl-edinak.jpg" },{ "name" : "Тим Кэхилл", "position" : "нападающий", "birth" : "06.12.1979", "team" : "Австралия", "height" : "178", "weight" : "69", "image" : "tim-kehill.jpg" }]}

# def set_zayavka( json_data , object ):
    # error = False
    # # форматирование полей   
    # try :
        # json_data["alias"] = json_data["alias"]
    # except KeyError :
        # json_data["alias"] = slugify(pytils.translit.slugify(json_data['champ']+'-'+json_data['team']))
        
    # try :
        # temp = get_object( Champ , 'name' , json_data['champ'] )
        # if temp :
            # json_data["champ"] = temp
        # else :
            # error = True
    # except KeyError :
        # pass
        
    # try :
        # temp = get_object( Team , 'name' , json_data['team'] , json_data['host'] )
        # if temp :
            # json_data["team"] = temp
        # else :
            # error = True
    # except KeyError :
        # pass
        
    # # обновление или запись нового объекта
    # if error :
        # object_update_or_create = None
    # else :
        # #print json_data
        # object_update_or_create = object.objects.update_or_create(
            # champ = json_data['champ'], 
            # team = json_data["team"],
        # )
        
        # object_update_or_create.players.all().delete()
        
        # # сделать добавление manytomany
        # try :   
            # players = json_data['players']
            # for player in players :
                # temp = set_player( player , Player )
                # if temp :
                    # object_update_or_create[0].players.add(temp)
                # else :
                    # print log_locale('no_object_in_db') + ' Player(' + team.encode('utf8') + ')'
                
        # except KeyError :
            # pass
        
    # return object_update_or_create
    
def set_team_mesto( champ ):
    teams = Teamchamp.objects.filter(champ=champ).order_by('points').order_by('win')
    
    
    
    
def set_data_to_teamchamp( json_data ):
    error = False
    try :
        teamchamp = Teamchamp.objects.get(name=json_data['command_first'],champ=json_data["champ"])
        teamchamp2 = Teamchamp.objects.get(name=json_data['command_second'],champ=json_data["champ"])
    except Teamchamp.DoesNotExist :
        error = True
        
    if json_data['status'].encode('utf-8') != 'Матч окончен' :
        error = True
        
    score_first = int(json_data['score'].split(':')[0])
    score_second = int(json_data['score'].split(':')[1])
    
    if score_first == score_second :
        draw = 1
        p_first = 1
        p_second = 1
    elif score_first > score_second :
        draw = 0
        p_first = 3
        p_second = 0
    else :
        draw = 0
        p_first = 0
        p_second = 3
    
    if not error :
        # first_command
        if teamchamp.points :
            teamchamp.points = teamchamp.points + p_first
        else :
            teamchamp.points = p_first
            
        if teamchamp.goals :
            teamchamp.goals = teamchamp.goals + score_first
        else :
            teamchamp.goals = score_first
            
        if teamchamp.vgoals :
            teamchamp.vgoals = teamchamp.vgoals + score_second
        else :
            teamchamp.vgoals = score_second
            
        if teamchamp.win :
            teamchamp.win = teamchamp.win + p_first//3
        else :
            teamchamp.win = p_first//3
            
        if teamchamp.lose :
            teamchamp.lose = teamchamp.lose + p_second//3
        else :
            teamchamp.lose = p_second//3
            
        if teamchamp.draw :
            teamchamp.draw = teamchamp.draw + draw
        else :
            teamchamp.draw = draw

        teamchamp.save()
        
        # first_second
        if teamchamp2.points :
            teamchamp2.points = teamchamp2.points + p_second
        else :
            teamchamp2.points = p_second
            
        if teamchamp2.goals :
            teamchamp2.goals = teamchamp2.goals + score_second
        else :
            teamchamp2.goals = score_second
            
        if teamchamp2.vgoals :
            teamchamp2.vgoals = teamchamp2.vgoals + score_first
        else :
            teamchamp2.vgoals = score_first
            
        if teamchamp2.win :
            teamchamp2.win = teamchamp2.win + p_second//3
        else :
            teamchamp2.win = p_second//3
            
        if teamchamp2.lose :
            teamchamp2.lose = teamchamp2.lose + p_first//3
        else :
            teamchamp2.lose = p_first//3
            
        if teamchamp2.draw :
            teamchamp2.draw = teamchamp2.draw + draw
        else :
            teamchamp2.draw = draw

        teamchamp2.save()
        
        
        # teamchamp.mesto = 1
        # teamchamp2.mesto = 1

        
       
    
def set_match( json_data , object ):
    error = False
    # форматирование полей   
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.slugify(json_data['command_first']+'-'+json_data['command_second']+'_'+json_data['champ']))
        
        
    try :
        format_datetime = get_datetime( json_data['datetime'] , format="%d.%m.%Y,%H:%M" )
        json_data['datetime'] = format_datetime
    except KeyError :
        pass
    

    # запись группы
    try :
        if json_data["qualifying"] :
            json_data["qualifying"] = True
        else :
            json_data["qualifying"] = False
    except KeyError :
        json_data["qualifying"] = False
        pass
        
    try :
        group = {
            "name" : json_data['group'] ,
            "champ" : json_data['champ'] ,
            "qualifying" : json_data['qualifying'] ,
            "teams" : json_data["command_first"] +','+ json_data["command_second"] ,
        }
        temp = set_group( group , Group )
        if temp :
            json_data["group"] = temp[0]
        else :
            error = True
        del json_data["qualifying"]
    except KeyError :
        del json_data["qualifying"]
        pass
        
    ###
        
    try :
        temp = get_object( Champ , 'name' , json_data['champ'] )
        if temp :
            json_data["champ"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    try :
        temp = get_object( Stadium , 'name' , json_data['stadium'] )
        if temp :
            json_data["stadium"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    # проверка в таблице соответствия
    try :
        get_true_name( json_data , Team , "command_first" , "name" )
    except KeyError :
        pass 
        
    try :
        get_true_name( json_data , Team , "command_second" , "name" )
    except KeyError :
        pass 
#	try :
#        get_true_name( json_data , Player , "name" , "name" )
#    except KeyError :
#        pass 
    try :
        temp = get_object( Team , 'name' , json_data['command_first'] )
        if temp :
            json_data["command_first"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    try :
        temp = get_object( Team , 'name' , json_data['command_second'] )
        if temp :
            json_data["command_second"] = temp
        else :
            error = True
    except KeyError :
        pass
        
    set_sostav_player( json_data , 'osnova1' )
    set_sostav_player( json_data , 'osnova2' )
    set_sostav_player( json_data , 'zamena1' )
    set_sostav_player( json_data , 'zamena2' )
    
    try :
        if json_data['teamchamp'] :
            set_data_to_teamchamp( json_data )
            del json_data['teamchamp']
    except KeyError :
        pass
    
     
    # обновление или запись нового объекта
    if error :
        object_update_or_create = None
    else :
        #print json_data
        object_update_or_create = object.objects.update_or_create(
            command_first = json_data['command_first'], 
            command_second = json_data["command_second"],
            datetime = json_data["datetime"],
            defaults=json_data
        )
    return object_update_or_create
    