﻿$(document).ready(function(){
    //Карточки за нарушения
    $('b:contains("2yellow")').html('<div class="yell crd double"><div class="dbl card"></div>');
    $('b:contains("yellow")').html('<div class="yell crd"></div>');
    $('b:contains("red")').html('<div class="red crd"></div>');
    //Идёт Игра
    $('.status p:contains("2-й тайм")').addClass('active');
    $('.status p:contains("1-й тайм")').addClass('active');
    //Замены
    $('span.zamena:contains("out:")').css('color','red').addClass('out');
    $('span.zamena:contains("in:")').css('color','green').addClass('in');
    $('span.zamena').each(function(){
        abc = $(this).text().split(':');
        if ($(this).val().length == false ){
            if( $(this).hasClass('out')){
                $(this).html('<i class="fa fa-long-arrow-down" aria-hidden="true"></i>' + $(abc)[1]);
            }
            else if( $(this).hasClass('in')){
                $(this).html('<i class="fa fa-long-arrow-up"></i>' + $(abc)[1]);
            }
        }
    })
    //Голы
    $('.goals-bl .gl_min, .text_min, .foal_min').each(function(){
        if( $(this).text().length > 3 ){
            aa = $(this).text().replace(' ',' +')
            $(this).text(aa);
        }
    })
    //Карточки игроков
    $('.pos-hidden:contains("нп")').css('background','#f44336')
    $('.pos-hidden:contains("вр")').css({'background':'green','color':'#fff'})
    $('.pos-hidden:contains("зщ")').css('background','#ffc107')
    $('.pos-hidden:contains("пз")').css('background','orange')
    
    mins = parseInt($('.match_minute').text().split(':')[0])
    secs = parseInt($('.match_minute').text().split(':')[1])
    
/*     timer = setInterval(function(){
        if( parseInt(secs) <= 58 ){
            secs = secs + 1
            $('.match_minute').text( mins +':' + (secs < 10 ? "0" : "") + secs )
        }
        else if ( secs == 59 && mins == 44 ){
            $('.match_minute').text('45:00+')
            clearInterval(timer)
        }
        else if ( secs == 59 && mins == 89 ){
            $('.match_minute').text('90:00+')
            clearInterval(timer)
        }
        else if ( secs == 59 ){
            secs = 0;
            mins = mins + 1
            $('.match_minute').text( mins + ':' + (secs == 0 ? "0" : "" ) + secs )
        }
    }, 1000) */
    
     refreshScore = function () {
        var score = 'ничего';
        var this_url = document.location.pathname.split('/')[2];
        
        $.ajax({
            url: '/ajax/',
            type: "POST",
            data: { type : 'score' , id : this_url },
            
            success: function (data) { 
                $('.game_info .score').html(data);
                console.log(data)
            },
        })
        setTimeout(function(){
            refreshScore()
            
        }, 5000)
    }
    refreshScore()
    
    refreshMins = function () {
        
        var mins = 'ничего';
        var this_url = document.location.pathname.split('/')[2];
        
        $.ajax({
            url: '/ajax/',
            type: "POST",
            data: { type : 'mins' , id : this_url },
            
            success: function (data) { 
                $('.progress-bar').attr('aria-valuenow', data);
                $('.curr_min span').html(data+"'")
            },
        })
        
        $('.progress-bar').css('width', 100/90*(parseInt($('.progress-bar').attr('aria-valuenow')))+'%');
        setTimeout(function(){
                refreshMins()
        },5000)
    }
    refreshMins()
    
    
    
    $('article').dblclick(function(){
       $('article').attr('contenteditable','true');
    })
    
    $(window).keypress(function(e){
        var this_url = document.location.pathname.split('/')[2];
        
        if( $('article').attr('contenteditable') == "true" ){
            if(e.keyCode==13){
               $('article').attr('contenteditable','false')
               new_html = $('article').html();
               $.ajax({
                    url: '/ajax/',
                    type: "POST",
                    data: { type : 'content' , id : this_url, this_cont : new_html },            
                    success: function (data) {
                        console.log(data);
                    },
                })
            }
        }
        
    });
})