﻿$(document).ready(function(){
    //переключение вкладок на главной
    $($('.tabs .tab')[0]).addClass('active')
    $($('.matches-table-list')[2]).addClass('active')
    $('.tabs .tab').click(function(){
        $(this).addClass('active')
        $(this).siblings().removeClass('active')
        a = $(this).attr('val')
        $('.matches-table-list').each(function(){
            if($(this).hasClass(a)){
                $(this).removeClass('active')
                $(this).addClass('active')
            }
            else{
                $(this).removeClass('active')
            }
        })
    })
    //переключение вкладок на матче
    $('.match-tabs #stat').addClass('active')
    $('.content-tabs #stat').addClass('active')
    $('.match-tabs .match-tab').click(function(){
        $(this).addClass('active')
        $(this).siblings().removeClass('active')
        a = $(this).attr('id')
        $('.content-tab').each(function(){
            if($(this).attr('id') == a){
                $(this).removeClass('active')
                $(this).addClass('active')
            }
            else{
                $(this).removeClass('active')
            }
        })
    })
})