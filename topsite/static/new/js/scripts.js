﻿$(document).ready(function(){
    //переключение вкладок на главной
    $($('.tabs .tab')[0]).addClass('active')
    $($('.matches-table-list')[0]).addClass('active')
    $('.tabs .tab').click(function(){
        $(this).addClass('active')
        $(this).siblings().removeClass('active')
        a = $(this).attr('val')
        $('.matches-table-list').each(function(){
            if($(this).hasClass(a)){
                $(this).removeClass('active')
                $(this).addClass('active')
            }
            else{
                $(this).removeClass('active')
            }
        })
    })
    //переключение вкладок на матче и команде
    $($('.match-tabs .match-tab')[0]).addClass('active')
    $($('.content-tabs .content-tab')[0]).addClass('active')
    $('.match-tabs .match-tab').click(function(){
        $(this).addClass('active')
        $(this).siblings().removeClass('active')
        a = $(this).attr('id')
        $('.content-tab').each(function(){
            if($(this).attr('id') == a){
                $(this).removeClass('active')
                $(this).addClass('active')
                if( a == 'txt'){
                    go()
                }
            }
            else{
                $(this).removeClass('active')
            }
        })
    })
    $('.osnova .table tr').hover(function(){
        $(this).find('.hidden-card').show()
    },function(){
        $(this).find('.hidden-card').hide()
    })
    //Добавление вкладок(текстовая трансляция)
    go = function(){
        $('.content-tab.active').prepend('<div class="new-line"><div class="txt-line yellow fresh"><div class="min"><span>60<div class="after"></div></span></div><div class="text">Желтая карточка: Ерохин Александр отправляется отдыхать раньше запланированного, Денисов получает по ногам, а для автора голевой передачи горчичник стал вторым.</div></div></div>')
        $('.new-line').animate({'max-height':'600px'},1400)
        $('.txt-line.fresh').animate({'opacity':'1'},1400)
    }
    $('.ekshn-list .red .icon , .ekshn-list .yellow .icon , .ekshn-list .blue .icon').click(function(){
        $(this).find('img').animate({'width':'30px'},500)
        $(this).find('img').delay(3000).animate({'width':'24px'},500)
    })
    $('.ekshn-list .green .icon').click(function(){
        $(this).find('img').addClass('active')
    })
  /*              setTimeout(
                $(this).find('img').removeClass('active')
                $(this).find('img').animate({'width':'24px'}, 700)
            , 1500)*/
})