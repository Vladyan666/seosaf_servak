#!c:/Python27/python
# -*- coding: utf8 -*-

# определение окружения
import os, sys, locale, codecs , argparse , django
sys.path.append('e:\\openserver\\domains\\football_first\\')
sys.path.append('e:\\openserver\\domains\\football_first\\football_first\\')
os.environ["DJANGO_SETTINGS_MODULE"] = "football_first.settings"
django.setup()

import json, pytils, datetime, pytz
from portal.models import TextPage, Team, Match, Player, Stadium, News
from django.utils import timezone
from django.conf import settings
from django.template.defaultfilters import slugify

def log_locale(key,locale='en'):
    log_string = {
        "add_news" : {"ru" : "Добавлена новость:","en" : "News added:"},
        "update_news" : {"ru" : "Обновлена новость:","en" : "News update:"},
        "add_textpage" : {"ru" : "Добавлена страница:","en" : "TextPage added:"},
        "update_textpage" : {"ru" : "Обновлена страница:","en" : "TextPage update:"},
        "add_player" : {"ru" : "Добавлен игрок:","en" : "Player added:"},
        "update_player" : {"ru" : "Обновлен игрок:","en" : "Player update:"},
        "add_match" : {"ru" : "Добавлен матч:","en" : "Match added:"},
        "update_match" : {"ru" : "Обновлен матч:","en" : "Match update:"},
        "add_team" : {"ru" : "Добавлена команда:","en" : "Team added:"},
        "update_team" : {"ru" : "Обновлена команда:","en" : "Team update:"},
        "add_stadium" : {"ru" : "Добавлен стадион:","en" : "Stadium added:"},
        "update_stadium" : {"ru" : "Обновлен стадион:","en" : "Stadium update:"},
        "error_date_not_set" : {"ru" : "Дата не определена: ","en" : "Date is not set: "},
        "error_db_has_not_team" : {"ru" : "В базе нет команды: ","en" : "DB does not have this team: "},
        "error_db_has_not_teams" : {"ru" : "В базе нет одной из команд: ","en" : "DB does not have this teams: "},
        "error_transfers_has_not_data" : {"ru" : "Данные в трансеферах не найдены","en" : "Data is not found in the in transfers"},
        "error_db_has_not_team_and_num" : {"ru" : "В данном клубе и с таким номером игрок не найден: ","en" : "The player in this team and with this number does not have in DB: "},
        "info_player_in_transfers" : {"ru" : "Игрок найден на основе данных трансферов: ","en" : "Player found in the in transfers: "},
        "info_data_add_to_db" : {"ru" : "Данные добавлены в базу!","en" : "Data added to the db!"},
    }
    return log_string[key][locale]

def setup_console(sys_enc="utf-8"):
    reload(sys)
    try:
        # для win32 вызываем системную библиотечную функцию
        if sys.platform.startswith("win"):
            import ctypes
            enc = "cp%d" % ctypes.windll.kernel32.GetOEMCP() #TODO: проверить на win64/python64
        else:
            # для Linux всё, кажется, есть и так
            enc = (sys.stdout.encoding if sys.stdout.isatty() else
                        sys.stderr.encoding if sys.stderr.isatty() else
                            sys.getfilesystemencoding() or sys_enc)

        # кодировка для sys
        sys.setdefaultencoding(sys_enc)

        # переопределяем стандартные потоки вывода, если они не перенаправлены
        if sys.stdout.isatty() and sys.stdout.encoding != enc:
            sys.stdout = codecs.getwriter(enc)(sys.stdout, 'replace')

        if sys.stderr.isatty() and sys.stderr.encoding != enc:
            sys.stderr = codecs.getwriter(enc)(sys.stderr, 'replace')

    except:
        pass # Ошибка? Всё равно какая - работаем по-старому...

# создание парсера командной строки
def create_parser ():
    parser = argparse.ArgumentParser()
    parser.add_argument('type', nargs='?')
    parser.add_argument ('-save', action='store_const', const=True)
 
    return parser

# получить id объекта
def get_object_id( object , name ):
    try:
        return object.objects.get(name=name).id
    except object.DoesNotExist:
        return None

# получить id матча        
def get_match_id( object , match_datetime , match_stadium ):
    try:
        return object.objects.get(datetime=match_datetime,stadium=match_stadium).id
    except object.DoesNotExist:
        return None
        
# получить время в нужном формате
def get_datetime( str , format="%d.%m.%Y" ):
    try :
        return timezone.make_aware(datetime.datetime.strptime(str, format), timezone.get_current_timezone())
    except ValueError :
        print "Date is not set: " + str.encode('utf8')
        return timezone.make_aware(datetime.datetime.strptime("01.01.0001", "%d.%m.%Y"), timezone.get_current_timezone())
    
# задать Текстовую страницу        
def set_textpage( textpage ):
    if get_object_id( TextPage , textpage['name'] ) :
        print log_locale('update_textpage')
    else :
        print log_locale('add_textpage')
    return TextPage(
        id = get_object_id( TextPage , textpage['name'] ),
        name = textpage['name'],  
        alias = slugify(pytils.translit.translify(textpage['name'])) if textpage['alias'] == '' else textpage['alias'],
        menuposition = int(textpage['menuposition']) if 'menuposition' in textpage else 0,
        
        content = textpage['content'] if 'content' in textpage else '',
        seo_h1 = textpage['seo_h1'] if 'seo_h1' in textpage else '',
        seo_title = textpage['seo_title'] if 'seo_title' in textpage else '',
        seo_description = textpage['seo_description'] if 'seo_description' in textpage else '', 
        seo_keywords = textpage['seo_keywords'] if 'seo_keywords' in textpage else '', 
        menutitle = textpage['menutitle'] if 'menutitle' in textpage else '',
        menushow = True if 'menushow' in textpage and textpage['menushow'] != "0" else False,
        sitemap = True if 'sitemap' in textpage and textpage['sitemap'] != "0" else False,
    )

# задать Новость        
def set_news( news ):
    if get_object_id( News , news['name'] ) :
        print log_locale('update_news')
    else :
        print log_locale('add_news')
    return News(
        id = get_object_id( News , news['name'] ),
        name = news['name'],
        date = get_datetime(news['date']) if 'date' in news else datetime.date.today,   
        alias = slugify(pytils.translit.translify(news['name'])) if news['alias'] == '' else news['alias'],
        
        content = news['content'] if 'content' in news else '',
        seo_h1 = news['seo_h1'] if 'seo_h1' in news else '',
        seo_title = news['seo_title'] if 'seo_title' in news else '',
        seo_description = news['seo_description'] if 'seo_description' in news else '', 
        seo_keywords = news['seo_keywords'] if 'seo_keywords' in news else '', 
        menutitle = news['menutitle'] if 'menutitle' in news else '',
        menushow = True if 'menushow' in news and news['menushow'] != "0" else False,
        sitemap = True if 'sitemap' in news and news['sitemap'] != "0" else False,
    )
    
# задать Стадион        
def set_stadium( stadium ):
    if get_object_id( Stadium , stadium['name'] ) :
        print log_locale('update_stadium')
    else :
        print log_locale('add_stadium')
    return Stadium(
        id = get_object_id( Stadium , stadium['name'] ),
        name = stadium['name'],
        image = settings.MEDIA_URL + "files/" + stadium['image'] if 'image' in stadium else '',
        alias = slugify(pytils.translit.translify(stadium['name'])),

        content = stadium['content'] if 'content' in stadium else '',
        seo_h1 = stadium['seo_h1'] if 'seo_h1' in stadium else '',
        seo_title = stadium['seo_title'] if 'seo_title' in stadium else '',
        seo_description = stadium['seo_description'] if 'seo_description' in stadium else '', 
        seo_keywords = stadium['seo_keywords'] if 'seo_keywords' in stadium else '', 
        menutitle = stadium['menutitle'] if 'menutitle' in stadium else '',
        menushow = True if 'menushow' in stadium and stadium['menushow'] != "0" else False,
        sitemap = True if 'sitemap' in stadium and stadium['sitemap'] != "0" else False,
    )
    
# задать Клуб        
def set_team( team ):
    if get_object_id( Team , team['name'] ) :
        print log_locale('update_team')
    else :
        print log_locale('add_team')
    return Team(
        id = get_object_id( Team , team['name'] ),
        name = team['name'],
        offsite = team['url'] if 'url' in team else '',
        group = team['group'] if 'group' in team else '',
        image = settings.MEDIA_URL + "files/" + team['image'] if 'image' in team else '',
        
        alias = slugify(pytils.translit.translify(team['name'])),
        
        content = team['content'] if 'content' in team else '',
        seo_h1 = team['seo_h1'] if 'seo_h1' in team else '',
        seo_title = team['seo_title'] if 'seo_title' in team else '',
        seo_description = team['seo_description'] if 'seo_description' in team else '',
        seo_keywords = team['seo_keywords'] if 'seo_keywords' in team else '',         
        menutitle = team['menutitle'] if 'menutitle' in team else '',
        menushow = True if 'menushow' in team and team['menushow'] != "0" else False,
        sitemap = True if 'sitemap' in team and team['sitemap'] != "0" else False,
    )

# получить id игрока на основе его номера в команде    
def get_player_id( pl_number , fc_object ):
    try :
        return Player.objects.get(number=pl_number,team=fc_object).id
    except Team.DoesNotExist as detail:
        print log_locale('error_db_has_not_team') + fc_name.encode('utf8') + ": ", detail
    except Player.DoesNotExist as detail:
        print log_locale('error_db_has_not_team_and_num') + fc_object.name.encode('utf8') + ", " + pl_number.encode('utf8') + ": ", detail
    try :
        trans_player = Player.objects.get(transfer__contains=[[pl_number,fc_object.name]])
        print log_locale('info_player_in_transfers'), trans_player
        return trans_player
    except Player.DoesNotExist :
        print log_locale('error_transfers_has_not_data')

# изменить номера игроков на id
def change_number_to_id( sostav , fc_name ):
    team = Team.objects.get(shortname=fc_name)
    
    for key in sostav :
        if key != "name":
            for player in sostav[key] :
                player_id = get_player_id( player[0].encode('utf-8') , team )
                player[0] = str(player_id)
        
    return sostav
        
# задать Матч   
def set_match( match ):
    try :
        # форматирование даты для записи в базу
        datetime_for_base = get_datetime( match['datetime'].strip() , format="%d.%m.%Y %H:%M" )
        stadium_object = Stadium.objects.get(name=match['stadium'])
        champ_object = Champ.objects.get(name=match['champ'])
        
        if get_match_id( Match , datetime_for_base , stadium_object) :
            print log_locale('update_match')
        else :
            #match['fc_first'] = change_number_to_id(match['fc_first'] , match['fc_first']['name'])
            #match['fc_second'] = change_number_to_id(match['fc_second'] , match['fc_second']['name'])

            print log_locale('add_match')
        
        return Match(
            id = get_match_id( Match , datetime_for_base , stadium_object) ,
            datetime = datetime_for_base ,
            stadium = stadium_object,
            champ = champ_object,
            group = match['group'] if 'group' in match else '',
            tour = match['tour'] if 'tour' in match else '',
            command_first = Team.objects.get(name=match['fc_first']['name']),
            command_second = Team.objects.get(name=match['fc_second']['name']),               
            command_data = [match['fc_first'],match['fc_second']],
            history = match['history'] if 'history' in match else [],
            actions_data = match['actions'] if 'actions' in match else [],
            offace_data = match['offace'] if 'offace' in match else [],
            alias = slugify(pytils.translit.translify(datetime_for_base.strftime('%d-%m-%Y-%H-%M-')+match['stadium'])),
            
            content = match['content'] if 'content' in match else '',
            seo_h1 = match['seo_h1'] if 'seo_h1' in match else '',
            seo_title = match['seo_title'] if 'seo_title' in match else '',
            seo_description = match['seo_description'] if 'seo_description' in match else '',   
            seo_keywords = match['seo_keywords'] if 'seo_keywords' in match else '', 
            menutitle = match['menutitle'] if 'menutitle' in match else '',
            menushow = True if 'menushow' in match and match['menushow'] != "0" else False,
            sitemap = True if 'sitemap' in match and match['sitemap'] != "0" else False,
        )
    except Team.DoesNotExist as detail:
        print "DB does not have this teams: " + match['fc_first']['name'].encode('utf8') + ", " + match['fc_second']['name'].encode('utf8') + ": ", detail
    
# задать Игрока   
def set_player( player ):
    try :
        if get_object_id( Player , player['name'] ) :
            print log_locale('update_player')
        else :
            print log_locale('add_player')
        return Player(     
            id = get_object_id( Player , player['name'] ),
            name = player['name'],
            number = player['number'] if 'number' in player else '',
            birth = get_datetime(player['birth']) if 'birth' in player else '',
            position = player['position'] if 'position' in player else '',
            team = Team.objects.get(name=player['team']) if 'team' in player else '',
            height = player['height'] if 'height' in player else '',
            weight = player['weight'] if 'weight' in player else '',
            image = settings.MEDIA_URL + "files/" + player['image'] if 'image' in player else '',
            
            alias = slugify(pytils.translit.translify(player['name'].replace("′",""))),
            
            content = player['content'] if 'content' in player else '',
            seo_h1 = player['seo_h1'] if 'seo_h1' in player else '',
            seo_title = player['seo_title'] if 'seo_title' in player else '',
            seo_description = player['seo_description'] if 'seo_description' in player else '',  
            seo_keywords = player['seo_keywords'] if 'seo_keywords' in player else '',  
            menutitle = player['menutitle'] if 'menutitle' in player else '',
            menushow = True if 'menushow' in player and player['menushow'] != "0" else False,
            sitemap = True if 'sitemap' in player and player['sitemap'] != "0" else False,
        )
    except Team.DoesNotExist as detail:
        print "DB does not have this team: " + player['team'].encode('utf8') + ": ", detail
    
# перезаписываем файл   
def rewrite_file( rewrite ):
    data = open('pr/'+object_name+'.pr' , 'w')
    data.write(rewrite)
    data.close()

#основные операции
def drive( object_name ):

    # разбор аргументов коммандной строки
    parser = create_parser()
    namespace = parser.parse_args()
    
    # если передан аргумент переопределяем тип данных
    if namespace.type :
        object_name = namespace.type
    
    data = open('pr/'+object_name+'.pr' , 'r')  # открытие файла
    go = False                            # начало записи в базу
    change = False                        # наличие изменений в файле
    rewrite = ''                          # перезаписанный файл
    
    for line in data :  
        # записываем в базу
        if go :
            if line.strip() == '': # пропуск пустых строк
                continue
                
            json_object = json.loads(line) # декодирование строки
            
            if object_name == 'other':
                object_name = json_object['type']
            
            #выбираем модель записи данных
            if object_name == 'team':                 # клуб
                object = set_team( json_object )
            elif object_name == 'match':            # матч
                object = set_match( json_object )
            elif object_name == 'player':           # игрок
                object = set_player( json_object )
            elif object_name == 'stadium':          #стадион
                object = set_stadium( json_object )
            elif object_name == 'textpage':          #страница
                object = set_textpage( json_object )
            elif object_name == 'news':          #страница
                object = set_news( json_object )

            if object :                             #проверка существования объекта
                if namespace.save:
                    object.save()
                #object.save()                          # сохранение объекта
                print object.alias
  
            rewrite += line  
            change = True 
        # не записываем
        else :
            # если встречаем break начинаем запись
            if line.strip() == 'break' :
                go = True
                
            else :
                rewrite += line    

    # если были изменения в файле делаем перенос строки        
    if change :
        rewrite += '\nbreak'
    else :
        rewrite += 'break'
        
    data.close()
    
    if namespace.save:
        print log_locale('info_data_add_to_db')

    # перезаписываем файл
    #rewrite_file( rewrite )


if __name__ == '__main__':
    print "Content-Type: text/html; charset=UTF-8\r\n"
    
    # object_name = "team" | "match" | "player" | "textpage" | "stadium" | "other" # в первой строке файла должно быть слово break
    object_name = 'textpage' 
        
    setup_console('utf-8')  # загрузка правильной кодировки
    drive( object_name )           

