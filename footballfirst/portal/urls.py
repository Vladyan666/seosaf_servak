from django.conf.urls import url
import django

from . import views
from views import TeamSitemap, NewsSitemap, TextPageSitemap, MatchSitemap, StadiumSitemap, PlayerSitemap, ChampSitemap, GroupSitemap, ZayavkaSitemap, FedSitemap

sitemaps = {
	'team': TeamSitemap,
	'news': NewsSitemap,
	'textpage': TextPageSitemap,
	'match': MatchSitemap,
	'stadium': StadiumSitemap,
	'player': PlayerSitemap,
	'champ': ChampSitemap,
	'group': GroupSitemap,
	'zayavka': ZayavkaSitemap,
	'fed': FedSitemap,
}

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^updater/$', views.updater, name='updater'),
    url(r'^fc/$', views.fc, name='fc'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^kontacty/$', views.contacts, name='contacts'),
    url(r'^teams/$', views.teams, name='teams'),
    url(r'^teamsjson/$', views.teamsjson, name='teamsjson'),
    url(r'^matches/$', views.matches, name='matches'),
    url(r'^champs/$', views.champs, name='champs'),
    url(r'^stadiums/$', views.stadiums, name='stadiums'),
    url(r'^ajaxcontroller/$', views.ajax, name='ajax'),
    url(r'^export/(?P<export_type>[0-9a-z\-]+)/$', views.export, name='export'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap' , {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^yandex_(?P<yandex_verification_number>[0-9a-z]+)\.html$', views.yandex_verification, name='yandex_verification'),
    url(r'^(?P<textpage_alias>[0-9a-z\-]+)/$', views.textpage, name='textpage'),
    url(r'^news/(?P<news_alias>[0-9a-z\-]+)/$', views.news, name="news"),
    url(r'^teams/(?P<team_alias>[0-9a-z\-]+)/$', views.team, name="team"),
    url(r'^matches/(?P<match_alias>[0-9a-z\-]+)/$', views.match, name="match"),
    url(r'^players/(?P<player_alias>[0-9a-z\-]+)/$', views.player, name="player"),
    url(r'^stadiums/(?P<stadium_alias>[0-9a-z\-]+)/$', views.stadium, name="stadium"),
    url(r'^champ/(?P<champ_alias>[0-9a-z\-]+)/$', views.champ, name="champ"),
    url(r'^group/(?P<group_alias>[0-9a-z\-]+)/$', views.group, name="group"),
    url(r'^zayavka/(?P<zayavka_alias>[0-9a-z\-]+)/$', views.zayavka, name="zayavka"),
    url(r'^fed/(?P<fed_alias>[0-9a-z\-]+)/$', views.fed, name="fed"),
]

handler404 = 'portal.views.error_404'