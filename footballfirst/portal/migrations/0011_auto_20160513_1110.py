# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-13 11:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0010_team_fed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='qualifying',
        ),
        migrations.AlterField(
            model_name='group',
            name='teams',
            field=models.ManyToManyField(to='portal.Team', verbose_name='\u041a\u043e\u043c\u0430\u043d\u0434\u044b \u0432 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u0435'),
        ),
        migrations.AlterField(
            model_name='team',
            name='fed',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='portal.Fed', verbose_name='\u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='team',
            name='logo',
            field=models.ImageField(upload_to=b'', verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f'),
        ),
    ]
