# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-19 16:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0018_auto_20160519_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='code',
            field=models.CharField(blank=True, max_length=5, null=True, verbose_name='\u041a\u043e\u0434 \u0441\u0442\u0440\u0430\u043d\u044b'),
        ),
    ]
