# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-20 12:34
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Champ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('teams', models.TextField(blank=True, null=True, verbose_name='\u041a\u043e\u043c\u0430\u043d\u0434\u044b \u0432 \u0447\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u0435')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u041b\u043e\u0433\u043e')),
            ],
            options={
                'verbose_name': '\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442',
                'verbose_name_plural': '\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('champ', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='portal.Champ', verbose_name='\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442')),
            ],
            options={
                'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u044b',
            },
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('datetime', models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u043d\u0430\u0447\u0430\u043b\u0430 \u043c\u0430\u0442\u0447\u0430')),
                ('tour', models.CharField(max_length=100, verbose_name='\u0422\u0443\u0440')),
                ('history', models.TextField(verbose_name='\u0418\u0441\u0442\u043e\u0440\u0438\u044f \u0432\u0441\u0442\u0440\u0435\u0447')),
                ('champ', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='portal.Champ', verbose_name='\u0427\u0435\u043c\u043f\u0438\u043e\u043d\u0430\u0442')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0442\u0447',
                'verbose_name_plural': '\u041c\u0430\u0442\u0447\u0438',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u0418\u043c\u044f')),
                ('position', models.CharField(max_length=100, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('number', models.CharField(max_length=100, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0432 \u043a\u043e\u043c\u0430\u043d\u0434\u0435')),
                ('birth', models.DateField(verbose_name='\u0414\u0435\u043d\u044c \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f')),
                ('club', models.CharField(max_length=100, verbose_name='\u041a\u043b\u0443\u0431')),
                ('height', models.CharField(max_length=100, verbose_name='\u0420\u043e\u0441\u0442')),
                ('weight', models.CharField(max_length=100, verbose_name='\u0412\u0435\u0441')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u0418\u0433\u0440\u043e\u043a',
                'verbose_name_plural': '\u0425\u043e\u043a\u043a\u0435\u0438\u0441\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Stadium',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0421\u0445\u0435\u043c\u0430')),
            ],
            options={
                'verbose_name': '\u0421\u0442\u0430\u0434\u0438\u043e\u043d',
                'verbose_name_plural': '\u0421\u0442\u0430\u0434\u0438\u043e\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('offsite', models.CharField(max_length=200, verbose_name='\u041e\u0444\u0438\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0439 \u0441\u0430\u0439\u0442')),
                ('group', models.CharField(max_length=100, verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
                ('photo', models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('image_min', models.ImageField(upload_to=b'', verbose_name='\u041c\u0438\u043d\u0438\u0430\u0442\u044e\u0440\u0430')),
                ('widget', models.TextField(verbose_name='\u0412\u0438\u0434\u0436\u0435\u0442')),
            ],
            options={
                'verbose_name': '\u0421\u0431\u043e\u0440\u043d\u0430\u044f',
                'verbose_name_plural': '\u0421\u0431\u043e\u0440\u043d\u044b\u0435',
            },
        ),
        migrations.CreateModel(
            name='TextPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_h1', models.CharField(blank=True, max_length=200, null=True, verbose_name='H1')),
                ('seo_title', models.CharField(blank=True, max_length=200, null=True, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=500, null=True, verbose_name='Keywords')),
                ('alias', models.SlugField(blank=True, max_length=200, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c')),
                ('menutitle', models.CharField(blank=True, max_length=200, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u043c\u0435\u043d\u044e')),
                ('menushow', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043c\u0435\u043d\u044e')),
                ('sitemap', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u043a\u0430\u0440\u0442\u0435 \u0441\u0430\u0439\u0442\u0430')),
                ('content', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f')),
                ('name', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('menuposition', models.IntegerField(verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f \u0432 \u043c\u0435\u043d\u044e')),
            ],
            options={
                'verbose_name': '\u0422\u0435\u043a\u0441\u0442\u043e\u0432\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
        ),
        migrations.AddField(
            model_name='player',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='portal.Team', verbose_name='\u0421\u0431\u043e\u0440\u043d\u0430\u044f'),
        ),
        migrations.AddField(
            model_name='match',
            name='command_first',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_first', to='portal.Team', verbose_name='\u041f\u0435\u0440\u0432\u0430\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u0430'),
        ),
        migrations.AddField(
            model_name='match',
            name='command_second',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_second', to='portal.Team', verbose_name='\u0412\u0442\u043e\u0440\u0430\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u0430'),
        ),
        migrations.AddField(
            model_name='match',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='portal.Group', verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430'),
        ),
        migrations.AddField(
            model_name='match',
            name='stadium',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='portal.Stadium', verbose_name='\u0421\u0442\u0430\u0434\u0438\u043e\u043d'),
        ),
    ]
