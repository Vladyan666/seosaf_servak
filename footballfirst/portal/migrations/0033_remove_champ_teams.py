# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-09 16:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0032_auto_20160609_1618'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='champ',
            name='teams',
        ),
    ]
