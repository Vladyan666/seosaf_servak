# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField

# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True
        
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню" )
    
    def get_absolute_url(self):
        if self.alias == "index" :
            return reverse('portal:index', kwargs={})
        else :
            return reverse('portal:textpage', kwargs={'textpage_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Fed(Page):
    class Meta:
        verbose_name = u"Федерация"
        verbose_name_plural = u"Федерации"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    acronym = models.CharField(max_length=10 , verbose_name=u"Аббревиатура")
    image = models.ImageField(verbose_name=u"Лого")
    
    def get_absolute_url(self):
        return reverse('portal:fed', kwargs={'fed_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Stadium(Page):
    class Meta:
        verbose_name = u"Стадион"
        verbose_name_plural = u"Стадионы"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Схема" , null=True , blank=True)
    city = models.CharField( max_length=200 , verbose_name=u"Город" , null=True , blank=True)
    country = models.CharField( max_length=200 , verbose_name=u"Страна" , null=True , blank=True)
    traffic = models.IntegerField( verbose_name=u"Вместимость" , null=True , blank=True )
    
    def get_absolute_url(self):
        return reverse('portal:stadium', kwargs={'stadium_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Team(Page):
    class Meta:
        verbose_name = u"Сборная"
        verbose_name_plural = u"Сборные"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название") 
    eng_name = models.CharField( max_length=200 , verbose_name=u"Название на английском") 
    code = models.CharField( max_length=5 , verbose_name=u"Код страны" , null=True , blank=True ) 
    fed = models.ForeignKey(Fed, verbose_name=u"Федерация" , null=True , blank=True )
    stadium = models.ForeignKey(Stadium, verbose_name=u"Домашний стадион" , null=True , blank=True)
    offsite = models.CharField( max_length=200 , verbose_name=u"Официальный сайт" , null=True , blank=True )
    
    logo = models.CharField( max_length=3000 ,verbose_name=u"Логотип" , null=True , blank=True )
    flag = models.CharField( max_length=3000 ,verbose_name=u"Флаг" , null=True , blank=True )
    photo = models.CharField( max_length=3000 ,verbose_name=u"Фотография команды" , null=True , blank=True )
    forma = models.CharField( max_length=3000 ,verbose_name=u"Фотография формы" , null=True , blank=True )
    
    widget = models.TextField(verbose_name=u"Виджет" , null=True , blank=True )   
    address = models.CharField( max_length=200 , verbose_name=u"Адрес" , null=True , blank=True )
    email = models.CharField( max_length=200 , verbose_name=u"Email" , null=True , blank=True )
    phone = models.CharField( max_length=200 , verbose_name=u"Телефон" , null=True , blank=True )
    fax = models.CharField( max_length=200 , verbose_name=u"Fax" , null=True , blank=True )
    fansite = models.CharField( max_length=2000 , verbose_name=u"Неофициальные сайты" , null=True , blank=True )
    founded = models.IntegerField( verbose_name=u"Год основания" , null=True , blank=True )
    country_fed = models.CharField( max_length=200 , verbose_name=u"Наименование федерации" , null=True , blank=True )
    
    techteam = models.BooleanField( default=False, verbose_name=u"Техническая команда" )
    
    def get_logo_list(self):
        return self.logo.split(',')
        
    def get_flag_list(self):
        return self.flag.split(',')
        
    def get_photo_list(self):
        return self.photo.split(',')
    
    def get_absolute_url(self):
        return reverse('portal:team', kwargs={'team_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Champ(Page):
    class Meta:
        verbose_name = u"Чемпионат"
        verbose_name_plural = u"Чемпионаты"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")    
    fed = models.ForeignKey(Fed, verbose_name=u"Федерация чемпионата" , null=True , blank=True )
    image = models.ImageField(verbose_name=u"Лого"  , null=True , blank=True )
    year = models.IntegerField( verbose_name=u"Год проведения" , null=True , blank=True )
    
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в чемпионате" , blank=True )
    
    web = models.TextField(verbose_name=u"Турнирная сетка" , null=True , blank=True )
    
    def get_absolute_url(self):
        return reverse('portal:champ', kwargs={'champ_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Group(Page):
    class Meta:
        verbose_name = u"Группа"
        verbose_name_plural = u"Группы"
    
    name = models.CharField( max_length=200 , verbose_name=u"Название")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат" , null=True , blank=True )
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в группе" , blank=True )
    qualifying = models.BooleanField( default=False , verbose_name=u"Отброчная группа" )
    
    def get_absolute_url(self):
        return reverse('portal:group', kwargs={'group_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Player(Page):
    class Meta:
        verbose_name = u"Игрок"
        verbose_name_plural = u"Футболисты"
        
    name = models.CharField(max_length=200, verbose_name=u"Имя")
    position = models.CharField(max_length=100, verbose_name=u"Позиция")
    number = models.CharField(max_length=100, verbose_name=u"Номер в команде")
    birth = models.DateField(verbose_name=u"День рождения")
    team = models.ForeignKey(Team, verbose_name=u"Сборная")
    club = models.CharField(max_length=100, verbose_name=u"Клуб")
    height = models.CharField(max_length=100, verbose_name=u"Рост")
    weight = models.CharField(max_length=100, verbose_name=u"Вес")
    image = models.ImageField(verbose_name=u"Фотография")
    
    def get_absolute_url(self):
        return reverse('portal:player', kwargs={'player_alias': self.alias})
    
    def __unicode__(self):
        return self.name
        
class Zayavka(Page):
    class Meta:
        verbose_name = u"Заявка на чемпионат"
        verbose_name_plural = u"Заявки на чемпионаты"
      
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат")
    team = models.ForeignKey(Team, verbose_name=u"Сборная")
    players = models.ManyToManyField(Player, verbose_name=u"Заявленные игроки")
    
    def get_absolute_url(self):
        return reverse('portal:zayavka', kwargs={'zayavka_alias': self.alias})
    
    def __unicode__(self):
        return self.champ.name+' - '+self.team.name
        
class Match(Page):
    class Meta:
        verbose_name = u"Матч"
        verbose_name_plural = u"Матчи"
         
    datetime = models.DateTimeField(verbose_name=u"Время начала матча")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат" , null=True , blank=True )
    group = models.ForeignKey(Group, verbose_name=u"Группа" , null=True , blank=True )
    stadium = models.ForeignKey(Stadium, verbose_name=u"Стадион" , null=True , blank=True )
    tour = models.CharField(max_length=100, verbose_name=u"Тур" , null=True , blank=True )
    command_first = models.ForeignKey(Team, verbose_name=u"Первая команда", related_name="command_first")
    command_second = models.ForeignKey(Team, verbose_name=u"Вторая команда", related_name="command_second") 
    
    #f_goals = models.IntegerField( verbose_name=u"Голы" , null=True , blank=True )
    
    #command_data = JSONField()
    #history = JSONField()
    #actions_data = JSONField()
    #offace_data = JSONField()
    status = models.CharField(max_length=100, verbose_name=u"Статус матча" , null=True , blank=True )
    goals = JSONField( verbose_name=u"Голы" , null=True , blank=True )
    foals = JSONField( verbose_name=u"Нарушения" , null=True , blank=True )
    people = models.IntegerField( verbose_name=u"Посещаемость" , null=True , blank=True )
    penalty = JSONField( verbose_name=u"Наказания" , null=True , blank=True )
    osnova1 = JSONField( verbose_name=u"Основной состав 1 команды" , null=True , blank=True )
    osnova2 = JSONField( verbose_name=u"Основной состав 2 команды" , null=True , blank=True )
    zamena1 = JSONField( verbose_name=u"Запасной состав 1 команды" , null=True , blank=True )
    zamena2 = JSONField( verbose_name=u"Запасной состав 2 команды" , null=True , blank=True )
    trener1 = JSONField( verbose_name=u"Тренер 1 команды" , null=True , blank=True )
    trener2 = JSONField( verbose_name=u"Тренер 2 команды" , null=True , blank=True )
    referee = JSONField( verbose_name=u"Главный судья" , null=True , blank=True )
    assistent1 = JSONField( verbose_name=u"1 Боковой судья" , null=True , blank=True )
    assistent2 = JSONField( verbose_name=u"2 Боковой судья" , null=True , blank=True )
    reserve = JSONField( verbose_name=u"Резрвный судья" , null=True , blank=True )
    text = JSONField( verbose_name=u"Текстовая трансляция" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('portal:match', kwargs={'match_alias': self.alias})
    
    def __unicode__(self):
       return self.command_first.name + ' - ' + self.command_second.name + ' ' + str(self.datetime)
       
class News(Page):
    class Meta:
        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"
        
    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    date = models.DateField( default=datetime.date.today , verbose_name=u"Время публикации" )
    
    def get_absolute_url(self):
        return reverse('portal:news', kwargs={'news_alias': self.alias})
    
    def __unicode__(self):
        return self.name