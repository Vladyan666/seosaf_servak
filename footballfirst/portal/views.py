# -*- coding: utf-8 -*-
import datetime
import ajaxcontroller
import re, sys, os
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpRequest
from django.http.response import Http404
from django.contrib.sitemaps import Sitemap
from django.template import RequestContext, loader
from django.db.models import Q
from .models import TextPage, News, Team, Player, Match, Stadium, Champ, Group, Fed, Zayavka

site_two = "8011"

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias
    
def choose_template(request , page):
    template_name = 'main'
    if request.get_port() == "8011": 
        template_name = "khl"
    if request.get_port() == "8012": 
        template_name = "template1"
    if request.get_port() == "8013": 
        template_name = "template2"
    if request.get_port() == "8014": 
        template_name = "template3"
    if request.get_port() == "8015": 
        template_name = "template4"
    if request.get_port() == "8016": 
        template_name = "template5"
    if request.get_port() == "8017": 
        template_name = "template6"
    if request.get_port() == "8018": 
        template_name = "template7"
    if request.get_port() == "8019": 
        template_name = "template8"
    if request.get_port() == "8020": 
        template_name = "template9"
    if request.get_port() == "8021": 
        template_name = "template10"
    if request.get_port() == "8022": 
        template_name = "template11"
    if request.get_port() == "8023": 
        template_name = "template12"
    if request.get_port() == "8024": 
        template_name = "template13"
    
    template = loader.get_template(template_name+'/'+page+'.html')
        
    return template
    
def get_host_vs_url( host_name , url ):
    vs = [
        ["8018" , "kontacty" , "contacts"],
        ["8020" , "kontactys" , "contacts"],
    ]
    url_first = url.split('/')[1]
    a = []
    
    for item in vs :
        a.append(item)
        try :
            it = item.index( url_first.encode('utf8') )
            if item[0] != host_name.encode('utf8') or it == 2 :
                raise Http404
        except ValueError :
            pass
    return url_first.encode('utf8')

def default_context(request,alias,object):
    host = active_menu(request)
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    
    xt = get_host_vs_url( request.get_port() , request.get_full_path() )
    
    #russia = Team.objects.get(name="Россия")
    #ru_matches = Match.objects.filter(Q(command_first=russia) | Q(command_second=russia)).order_by('datetime')
    last_news = News.objects.order_by('date')[:4]
    last_matches = Match.objects.filter(champ=Champ.objects.get(name="Кубок конфедераций 2017")).order_by('datetime')[:5]
    last_matches_two = Match.objects.filter(champ=Champ.objects.get(name="Кубок конфедераций 2017")).order_by('datetime')[5:10]
    #teams = Team.objects.filter(fed=Fed.objects.get(name="УЕФА"),techteam=False).order_by('name')
    champ_teams = Champ.objects.get(name="Кубок конфедераций 2017").teams.all()
    teams = champ_teams
    teams_group = []
    groups = Group.objects.filter(champ=Champ.objects.get(name="Кубок конфедераций 2017"),name__contains="Группа").order_by('name')
    for group in groups :
        matches = Match.objects.filter(group=group)
        temp_teams = []
        for match in matches :
            temp_teams.append(match.command_first)
        temp_teams = list(set(temp_teams))
        teams_group.append([group,temp_teams])
    try:
        data = object.objects.get(alias=alias)
    except object.DoesNotExist:
        raise Http404
    
    context_object = {
        'host' : host,
        'menu' : menu,
        #'ru_matches' : ru_matches,
        'last_news' : last_news,
        'last_matches' : last_matches,
        'last_matches_two' : last_matches_two,
        'teams' : teams,
        'teamsAB' : teams_group,
        'champ_teams' : champ_teams,
        'data' : data,
        'xt' : xt,
    }
    
    return context_object

# Create your views here.
def index(request):
    template = choose_template(request , "index")
    
    context_data = default_context( request , "index" , TextPage )    
    news = News.objects.order_by('date')[:5]
    stadiums = Stadium.objects.order_by('id');
    champ = Champ.objects.get(name="Кубок конфедераций 2017")

    ru_players = Player.objects.filter(team=Team.objects.get(name="Россия"))
    
    context_data.update({
        'news' : news,
        'stadiums' : stadiums,
        'ru_players' : ru_players,
        'champ' : champ,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    

def textpage(request,textpage_alias):
    template = choose_template(request , "textpage")
    
    context_data = default_context( request , textpage_alias , TextPage )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def contacts(request):
    template = choose_template(request , "contacts")
    
    context_data = default_context( request , 'contacts' , TextPage )
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def news(request,news_alias):
    template = choose_template(request , "news_item")
    
    context_data = default_context( request , news_alias , News )
    try:
        next = context_data['data'].get_next_by_date()
    except News.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_date()
    except News.DoesNotExist:
        prev = None
    
    context_data.update({
        'next' : next,
        'prev' : prev,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def stadiums(request):
    template = choose_template(request , "stadiums")
    
    context_data = default_context( request , "stadiums" , TextPage )
    stadiums = Stadium.objects.all()
    
    context_data.update({
        'stadiums' : stadiums,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def champs(request):
    template = choose_template(request , "champs")
    
    context_data = default_context( request , "champs" , TextPage )
    champs = Champ.objects.all()
    
    context_data.update({
        'champs' : champs,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def teams(request):
    template = choose_template(request , "teams")
    
    context_data = default_context( request , "teams" , TextPage )
    # teams = Team.objects.filter(techteam=False).order_by('name')
    # context_data.update({
        # 'teams' : teams,
    # })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))

def teamsjson(request):
    template = loader.get_template("main/teamsjson.html")

    name = teamsjson.objects.all()

    context = RequestContext(request, {
        'name' : name,
    })

    return HttpResponse(template.render(context) , content_type="text/plain;charset=utf-8")
    
def matches(request):
    template = choose_template(request , "matches")
    
    context_data = default_context( request , "matches" , TextPage )
    matches = Match.objects.filter(champ=Champ.objects.get(name="Кубок конфедераций 2017")).order_by('datetime')
    #matches = Match.objects.order_by('datetime')
    stadiums = Stadium.objects.all()
    
    context_data.update({
        'matches' : matches,
        'stadiums' : stadiums,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def team(request,team_alias):
    template = choose_template(request , "team")
    
    context_data = default_context( request , team_alias , Team )

    breadcrumbs = [TextPage.objects.get(alias='teams')]
    groups = Group.objects.filter(teams=context_data['data'])
    matches = Match.objects.filter(Q(command_first=context_data['data']) | Q(command_second=context_data['data']))
    players = Player.objects.filter(team=context_data['data'])
    coaches = Player.objects.filter(position="тренер", team=context_data['data'])
    goalkeepers = Player.objects.filter(position="вратарь", team=context_data['data'])
    defenders = Player.objects.filter(position="защитник", team=context_data['data'])
    midfielders = Player.objects.filter(position="полузащитник", team=context_data['data'])
    attacks = Player.objects.filter(position="нападающий", team=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'groups' : groups,
        'matches' : matches,
        'players' : players,
        'coaches' : coaches,
        'goalkeepers' : goalkeepers,
        'defenders' : defenders,
        'midfielders' : midfielders,
        'attacks' : attacks,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def get_birth( data ):
    if data.birth.year == 1 :
        return u"Нет данных"
    else :
        return data.birth
    
def player(request, player_alias):
    template = choose_template(request , "player")
    
    context_data = default_context( request , player_alias , Player )
    breadcrumbs = [TextPage.objects.get(alias='teams'),context_data['data'].team]
    birth = get_birth( context_data['data'] )
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'birth' : birth,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def match(request, match_alias):
    template = choose_template(request , "match")

    context_data = default_context( request , match_alias , Match )
    breadcrumbs = [TextPage.objects.get(alias='matches')]
    # for text in context_data['data'].text :
        # try :
            # try :
                # player = str(Player.objects.filter(Q(team=context_data['data'].command_first)|Q(team=context_data['data'].command_second)).get(name=text['player']).id)
                # text['player'] = player
            # except KeyError :
                # pass
        # except Player.DoesNotExist:
            # pass
            
        #
        
    
    try:
        next = context_data['data'].get_next_by_datetime()
    except Match.DoesNotExist:
        next = None
    try:
        prev = context_data['data'].get_previous_by_datetime()
    except Match.DoesNotExist:
        prev = None
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'next' : next,
        'prev' : prev,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def stadium(request, stadium_alias):
    template = choose_template(request , "stadium")

    context_data = default_context( request , stadium_alias , Stadium )
    breadcrumbs = [TextPage.objects.get(alias='stadiums')]
    matches = Match.objects.filter(stadium=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'matches' : matches
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def fed(request, fed_alias):
    template = choose_template(request , "fed")

    context_data = default_context( request , champ_alias , Fed )
    breadcrumbs = [TextPage.objects.get(alias='feds')]
    champs = Champ.objects.filter(fed=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'champs' : champs
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def zayavka(request, zayavka_alias):
    template = choose_template(request , "zayavka")

    context_data = default_context( request , champ_alias , Fed )
    breadcrumbs = [TextPage.objects.get(alias='zayavki')]
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def champ(request, champ_alias):
    template = choose_template(request , "champ")

    context_data = default_context( request , champ_alias , Champ )
    breadcrumbs = [TextPage.objects.get(alias='champs')]
    matches = Match.objects.filter(champ=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'matches' : matches
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def group(request, group_alias):
    template = choose_template(request , "group")

    context_data = default_context( request , group_alias , Group )
    breadcrumbs = [Group.objects.get(alias=group_alias).champ]
    matches = Match.objects.filter(group=context_data['data'])
    
    context_data.update({
        'breadcrumbs' : breadcrumbs,
        'matches' : matches
    })
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def get_field_value( object , field ):
    from django.db.models.fields import IntegerField, DateField, DateTimeField
    from django.db.models.fields.files import ImageField
    from django.contrib.postgres.fields.jsonb import JSONField
    import json

    value = eval("object."+field)
    field_type = "other"
    
    if type(value) == bool :
        field_type = "bool"
        if value :
            value = "1"
        else :
            value = "0"
        
    elif isinstance( object._meta.get_field(field) , IntegerField ) : 
        field_type = "int"
        if value :
            value = str( value )
        else :
            value = "0"
            
    elif isinstance( object._meta.get_field(field) , DateField ) : 
        field_type = "date"
        if value :
            value = str( value.strftime("%d.%m.%Y") )
        else :
            value = "01.01.1900"
            
    elif isinstance( object._meta.get_field(field) , DateTimeField ) : 
        field_type = "datetime"
        if value :
            value = str( value.strftime("%d.%m.%Y,%H:%M") )
        else :
            value = "01.01.1900,00:00"
            
    elif isinstance( object._meta.get_field(field) , ImageField ) : 
        field_type = "image"
        if value :
            value = str( value ).split("/")[-1]
        else :
            value = ""
    
    elif isinstance( object._meta.get_field(field) , JSONField ) :
        field_type = "json"
        value = json.dumps(value , ensure_ascii = False , encoding='utf8')
    
    elif type(value) == unicode or type(value) == str :
        field_type = "str"
        value = value.encode('utf8').replace('"', '\\"')
    
    else :
        if value :
            value = str( value )
        else :
            value = ""
        
    return [field , value , field_type]
    
def export(request,export_type):
    template = loader.get_template('export.html')
    if export_type == 'team' :
        object = Team
    elif export_type == 'match' :
        object = Match
    elif export_type == 'player' :
        object = Player
    elif export_type == 'stadium' :
        object = Stadium
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'champ' :
        object = Champ
    elif export_type == 'fed' :
        object = Fed
    elif export_type == 'group' :
        object = Group
    elif export_type == 'textpage' :
        object = TextPage
    else :
        object = Team
    fields = [field.name for field in object._meta.fields]
    
    objects = object.objects.all()

    data = []
    for object in objects :
        item = []
        for field in fields :
            if field != 'id':
                field_param = get_field_value( object , field ) # [field , value , type]
                item.append(field_param)
        data.append(item)
            
    context = RequestContext(request, {
        'objects' : objects,
        'fields' : fields,
        'data' : data,
    })
    
    return HttpResponse(template.render(context) , content_type="text/plain;charset=utf-8")
    
def ajax(request):
    if request.is_ajax():
        message = ajaxcontroller.get_svg()
        return HttpResponse(message)
    else:
        message = "Hello"
        
def fc(request):
    template = loader.get_template('fc/index.html')
    
    context_data = default_context( request , 'index' , TextPage )
    
    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
def updater(request): 
    try :
        object_name = re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',request.GET['type']).strip()
    except KeyError :
        return HttpResponse(u'Модель не определена!')
        
    sys.path.append(os.path.join(settings.BASE_DIR, 'scripts'))
    import updater_
    
    result = updater_.init( object_name )
    return HttpResponse(result)

# не работает(    
def error_404(request):
    template = loader.get_template('main/404.html')
    
    context_data = default_context( request , match_alias , TextPage )

    context = RequestContext(request, context_data)
    
    return HttpResponse(template.render(context))
    
# Карта сайта
class StadiumSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Stadium.objects.all()
        
class GroupSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Group.objects.all()
        
class FedSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Fed.objects.all()
        
class ZayavkaSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Zayavka.objects.all()

class ChampSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Champ.objects.all()
    
class PlayerSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Player.objects.all()
    
class MatchSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Match.objects.all()
    
class TextPageSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return TextPage.objects.all()    
    
class TeamSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return Team.objects.all()
        
class NewsSitemap(Sitemap):
	changefreq = "weekly"
	priority = 0.5
    
	def items(self):
		return News.objects.all()
        
def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")
    
def yandex_verification(request,yandex_verification_number):
    template = loader.get_template('yandex_verification.html')
    context = RequestContext(request,{
        'yandex_verification_number' : yandex_verification_number,
    })
    return HttpResponse(template.render(context))