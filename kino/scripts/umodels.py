# -*- coding: utf8 -*-
############################################################################
import pytils, datetime, pytz
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.conf import settings

# импортируем нужные модели
from base.models import TextPage

# log записи 
def log_locale(key,locale='en'):
    log_string = {
        "no_object_in_db" : {"ru" : "Объект в базе не найден: ", "en" : "DB does not have this object: "},
    }
    return log_string[key][locale].encode('utf8')
    
# получить время в нужном формате
def get_datetime( str , format="%d.%m.%Y" ):
    try :
        return timezone.make_aware(datetime.datetime.strptime(str.strip(), format), timezone.get_current_timezone())
    except ValueError :
        print "Date is not set: " + str.strip().encode('utf8')
        return timezone.make_aware(datetime.datetime.strptime("01.01.1990", "%d.%m.%Y"), timezone.get_current_timezone())

# получаем объект, если существует
def get_object( object , field_id , value  ):
    try:
        return eval("object.objects.get("+field_id+"=value)")
    except object.DoesNotExist as detail:
        print log_locale('no_object_in_db') + value.encode('utf8') , detail
        return None
        
def get_field_value( object , field ):
    from django.db.models.fields import IntegerField, DateField, DateTimeField
    from django.db.models.fields.files import ImageField

    value = eval("object."+field)
    
    if type(value) == bool :
        if value :
            value = "1"
        else :
            value = "0"
        
    elif isinstance( object._meta.get_field(field) , IntegerField ) : 
        if value :
            value = str( value )
        else :
            value = "0"
            
    elif isinstance( object._meta.get_field(field) , DateField ) : 
        if value :
            value = value.strftime("%d.%m.%Y")
        else :
            value = "01.01.1900"
            
    elif isinstance( object._meta.get_field(field) , DateTimeField ) : 
        if value :
            value = value.strftime("%d.%m.%Y,%H:%M")
        else :
            value = "01.01.1900,00:00"
            
    elif isinstance( object._meta.get_field(field) , ImageField ) : 
        if value :
            value = str( value ).split("/")[-1]
        else :
            value = ""
    
    elif type(value) == unicode or type(value) == str :
        value = value.encode('utf8').replace('"', '\\"')
    
    else :
        if value :
            value = value.name.encode('utf8')
        else :
            value = ""
        
    return value
        
def get_foreign_key(  json_data , error , field , name_field, object  ):
    try :
        temp = get_object( object , name_field , json_data[field] )
        if temp :
            json_data[field] = temp
        else :
            error = True
    except KeyError :
        pass
# get_foreign_key( json_data , error , 'mail' , 'mail', Mails  )
    
def set_sostav_player( json_data , field_name ):
    try :
        sostav = json_data[field_name]
        for player in sostav :
            temp = get_object( Player , 'name' , player['player'].strip() )
            if temp :
                player['alias'] = temp.alias
                player['id'] = temp.id
                player['image'] = str(temp.image)
                player['weight'] = temp.weight
                player['height'] = temp.height
    except KeyError :
        pass        
        
        
############################################################################

def get_teamsnames_table( key ):
    tables = {
        "name" : [
            ["Кот-д'Ивуар","Кот-д’Ивуар (Берег Слоновой Кости)"],
            ["Рубен Рочина","РубенРочина"],
            ["Корея","Южная Корея"],
            ["Чили","Чили, о. Пасхи"],
            ["Нидерланды","Нидерланды (Голландия)","Голландия"],
            ["Эквадор","Эквадор, Галапагосы",],
            ["Сербия","Сербия и Черногория"],
			["Пауль Агиляр", "Пауль Агилар"],
			["Хесус Корона", "Хосе де Хесус Корона"],
			["Ирвинг Лосано", "Ирвин Лосана"],
			["Мигель Лайюн", "Мигель Лаюн"],
			["Хорхе Торрес", "Хорхе Торрес Нило"],
			["Джейсон Джерия", "Джейсон Герия"],
			["Алекс Герсбах", "Александер Герсбах"],
			["Тим Кэйхилл", "Тим Кэхилл"],
			["Райан Макгоуэн", "Райан Макгован"],
			["Мэтью Лекки", "Мэтью Леки"],
			["Мэттью Райан", "Мэтью Райан"],
			["Том Рогич", "Томас Рогич"],
			["Марк Миллиган", "Марк Миллигэн"],
			["Трент Сэйнсбери", "Трент Сэйнсбари"],
        ],
        "eng_name" : [
            ["Cote d'Ivoire (Ivory Coast)","Côte d'Ivoire", "Cote d'Ivoire"],
            ["Macedonia","FYR Macedonia"],
            ["Democratic Republic of the Congo","Congo DR"],
            ["Cape Verde","Cape Verde Islands"],
            ["China","China PR"],
            ["Taiwan (Taipei)","Chinese Taipei"],
            ["Curacao","Curaçao"],
            ["The Turks and Caicos Islands","Turks and Caicos Islands"],
            ["Reunion","Réunion"],
            ["Saint Kitts and Nevis","St. Kitts and Nevis"],
            ["Ireland","Republic of Ireland"],
            ["Sao Tome and Principe","São Tomé e Príncipe","Sao Tome e Principe"],
            ["French Polynesia","Tahiti"],
            ["Republic of South Africa","South Africa"],
            ["Palestinian Territories","Palestine"],
            ["Qatar","Quatar"],
            ["Myanmar (Burma)","Myanmar"],
            ["Macau","Macao"],
            ["Korea","Korea Republic"],
            ["Republic of the Congo","Congo"],
            ["United States Virgin Islands","US Virgin Islands"],
            ["East Timor","Timor-Leste"],
            ["Saint Lucia","St. Lucia"],
            ["Saint Vincent and the Grenadines","St. Vincent and the Grenadines", "San Vicente and Granadines"],
            ["North Korea","Korea DPR"],
			["Thomas Muller","Thomas Muller"],
			["Mesut Ozil","Mesut Ozil"],
			["Mario Gotze","Mario Gotze"],
			["Leroy Sane","Leroy Sane"],
			["Mario Gomez","Mario Gomez"],
			["Andre Schurrle","Andre Schurrle"],
			["Benedikt Howedes","Benedikt Howedes"],
			["Jerome Boateng","Jerome Boateng"],
			["Marc-Andre ter Stegen","Marc-Andre ter Stegen"],

        ],
        "code" : [
            ['FRO','FAR'],
            ['LVA','LAT'],
            ['MNE','MGR'],
            ['ROU','ROM'],
            ['SRB','SCG'],
            ['EQG','GNQ','GEQ'],
            ['GNB','GBS'],
            ['LBY','LBA'],
            ['NGA','NGR'],
            ['SDN','SUD'],
            ['BHR','BRN'],
            ['IDN','INA'],
            ['IRN','IRI'],
            ['SVN','SLO'],
            ['BFA','BUR'],
            ['BRB','BAR'],
            ['BLZ','BIZ'],
            ['VGB','IVB'],
            ['SLV','ESA'],
            ['MSR','MNT'],
            ['TCA','TKS'],
            ['VIR','ISV'],
        ],
    }
    if key in tables :
        return tables[key]
    else :
        return []
        
def get_bool_value( json_data , field ):
    try :
        if json_data[field] and json_data[field] != "0" :
            json_data[field] = True
        else :
            json_data[field] = False
    except KeyError :
        json_data[field] = False
        pass
# проверка в таблице соответствия        
def get_true_name( json_data , object ,  main_field , field_in_object ):    
    if not get_object( object , field_in_object , json_data[main_field] ) :
        new_name = ""
        name_replace = get_teamsnames_table( field_in_object )
        for name in name_replace :
            try :
                it = name.index( json_data[main_field].encode('utf8') )
                json_data[main_field] = name[0]
            except ValueError :
                pass
                
# добавление в коллекцию каринок
def set_new_image( json_data , object , main_field , field ): 
    try :
        if get_object( object , main_field , json_data[main_field] ) :
            if eval("get_object( object , main_field , json_data[main_field] )."+field) :
                images = eval("get_object( object , main_field , json_data[main_field] )."+field+".split(',')")
                if not json_data[field] in images :
                    json_data[field] = eval("get_object( object , main_field , json_data[main_field] )."+field)+','+json_data[field]
                else :
                    json_data[field] = eval("get_object( object , main_field , json_data[main_field] )."+field)
        else :
            json_data[field] = ""
    except KeyError :
        if not get_object( object , main_field , json_data[main_field] ) :
            json_data[field] = ""
        pass

##########################  Модели  ##################################################

def set_default( json_data ):
    get_bool_value( json_data , "menushow" )
    get_bool_value( json_data , "sitemap" )

# задать Текстовую страницу        
def set_textpage( json_data , object ):
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    object_update_or_create = object.objects.update_or_create(
        name = json_data['name'] ,
        defaults=json_data
    )
    return object_update_or_create

#Фильм    
def set_film( json_data , object ):
    # форматирование полей
    try :
        json_data["alias"] = json_data["alias"]
    except KeyError :
        json_data["alias"] = slugify(pytils.translit.translify(json_data['name']))
        
    object_update_or_create = object.objects.update_or_create(
        name = json_data['name'] ,
        defaults=json_data
    )
    return object_update_or_create