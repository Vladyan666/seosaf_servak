# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-28 01:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0014_film_poster'),
    ]

    operations = [
        migrations.AlterField(
            model_name='film',
            name='year',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0413\u043e\u0434 \u0432\u044b\u043f\u0443\u0441\u043a\u0430'),
        ),
    ]
