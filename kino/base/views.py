# -*- coding: utf8 -*-
from django.shortcuts import render, render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext, loader
from django.template.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group 
from .models import Film, TextPage ,Category
from django.db.models import Q
import sys, re, os
from django.conf import settings
import math, urllib, pytils

def default_context(request,alias,object):
    menu = TextPage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    data = get_object_or_404(object, alias=alias)
    
    context_object = {
        'menu' : menu,
        'data' : data,
    }
    
    return context_object
    
def index(request):
    template = loader.get_template('index.html')
    
    context = RequestContext(request)
    
    return HttpResponse(template.render(context))
    
def films(request):
    template = loader.get_template('films.html')
    context_data = default_context( request , "films" , TextPage )
    
    cinemas = Film.objects.all()
    
    context_data.update({
        'cinemas' : cinemas,
    })
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))
    
def check(request):
    return render_to_response('e7e00435857b50f5.txt', content_type="text/plain")

def film(request, alias):
    template = loader.get_template('film.html')
    context_data = default_context( request , alias , Film )
    cinema = Film.objects.get(alias=alias)

    context_data.update({
        'cinema' : cinema,
    })
    
    context = RequestContext(request, context_data)
     
    return HttpResponse(template.render(context))

def categories(request):
    template = loader.get_template('categories.html')
    context_data = default_context( request , "categories" , TextPage )
    categories = Category.objects.all()

    context_data.update({
        'categories' : categories,
    })
    
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))
    
def category(request, alias):
    template = loader.get_template('category.html')
    context_data = default_context( request , "category" , TextPage )
    
    category = Category.objects.all()
    
    context_data.update({
        'category' : category,
    })
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))

@csrf_exempt 
def postupdater(request): 
    try :
        object_name_m = re.sub(r'[^\w\s]+|[\d]+|[_]{2}', r'',request.POST['type']).strip()
    except KeyError :
        return HttpResponse(u'Модель не определена!')
     
    sys.path.append(os.path.join(settings.BASE_DIR, 'scripts'))
    import updater
    result = ''
    
    object_name_arr = object_name_m.split(',')
    write_arr = request.POST['write'].encode('utf-8').split('@@')
    
    i = 0
    for object_name in object_name_arr :
    
        write = 'break\n'
        
        for wr in write_arr[i].split('::') :
            write += wr+'\n'
            
        data = open(os.path.join(settings.BASE_DIR, 'scripts')+'/pr/'+object_name+'.pr' , 'w')
        data.write(write)
        data.close()
        
        result += updater.init( object_name )
        
        i += 1
    return HttpResponse(result)    
    
def ajaxcontroller(request):
    if request.is_ajax():
        new_min = 'ajax:no-method'
        if request.POST['type'] == 'mins':
            #match = Match.objects.get(alias='match1')
            #this_url = 'match1'
            #this_url = request.POST['id']
            #request.POST['id'] = 'match1'
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.minute
            
        elif request.POST['type'] == 'score':
            match = Match.objects.get(alias=request.POST['id'])
            MyValue = match.score
            
        elif request.POST['type'] == 'content':
            if request.user.is_authenticated():
                match = Match.objects.get(alias=request.POST['id'])
                match.content = request.POST['this_cont']
                match.save()
                MyValue = match.content
            else:
                MyValue = u'Неавторизован'
                
        return HttpResponse(MyValue)
    else:
        raise Http404
