# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.postgres.fields import JSONField
# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True

    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=500 , verbose_name="Keywords" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"

    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню" )

    def get_absolute_url(self):
        if self.alias == "index" :
            return reverse('base:index', kwargs={})
        else :
            return reverse('base:textpage', kwargs={'textpage_alias': self.alias})

    def __unicode__(self):
        return self.name

class Producer(Page):
    class Meta:
        verbose_name = u"Режиссер"
        verbose_name_plural = u"Режиссеры"
    
    name = models.CharField(max_length=200, verbose_name=u'Имя Режиссера')
    birth = models.DateField(verbose_name=u"День рождения" , null=True , blank=True )
    birthplace = models.CharField(max_length=200, verbose_name=u'Место рождения', null=True , blank=True)
    
    def get_absolute_url(self):
        return reverse('base:producer', kwargs={'producer_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Actor(Page):
    class Meta:
        verbose_name = u"Актер"
        verbose_name_plural = u"Актеры"
    
    name = models.CharField(max_length=200, verbose_name=u'Имя Актера')
    heigth = models.CharField(max_length=10, verbose_name=u'Рост', null=True , blank=True)
    birth = models.DateField(verbose_name=u"День рождения" , null=True , blank=True )
    birthplace = models.CharField(max_length=200, verbose_name=u'Место рождения', null=True , blank=True)
    
    def get_absolute_url(self):
        return reverse('base:actor', kwargs={'actor_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Category(Page):
    class Meta:
        verbose_name = u"Подборка"
        verbose_name_plural = u"Подборки"
        
    name = models.CharField(max_length=200, verbose_name=u'Подборка')
    
    def get_absolute_url(self):
        return reverse('base:category', kwargs={'category_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Genre(Page):
    class Meta:
        verbose_name = u"Жанр"
        verbose_name_plural = u"Жанры"
        
    name = models.CharField(max_length=200, verbose_name=u'Жанр')
    
    def get_absolute_url(self):
        return reverse('base:genre', kwargs={'genre_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Film(Page):
    class Meta:
        verbose_name = u"Фильм"
        verbose_name_plural = u"Фильмы"
        
    name = models.CharField(max_length=200, verbose_name=u'Название фильма')
    truename = models.CharField(max_length=200, verbose_name=u'Название сериала в оригинале', null=True , blank=True)
    genre = models.ManyToManyField(Genre, verbose_name=u'Жанр', blank=True)
    category = models.ManyToManyField(Category, verbose_name=u'Подборки', blank=True)
    actor = models.ManyToManyField(Actor, verbose_name=u'Актеры', blank=True)
    year = models.CharField(max_length=20, verbose_name=u"Год выпуска" , null=True , blank=True )
    QUALITY = (
        ('Не указан', 'Не указан'),
        ('CamRIP', 'CamRIP'),
        ('HD720', 'HD720'),

    )
    quality = models.CharField( max_length=20 ,verbose_name=u"Качество", choices=QUALITY, blank=True )
    country = models.CharField(max_length=50, verbose_name=u'Страна', null=True , blank=True)
    TRANSLATE = (
        ('Не указан', 'Не указан'),
        ('Дублированный', 'Дублированный'),
        ('Одноголосый', 'Одноголосый'),
    )
    translate = models.CharField(max_length=50, verbose_name=u'Перевод',choices=TRANSLATE, blank=True)
    duration = models.TimeField(verbose_name=u'Продолжительность', null=True , blank=True)
    producer = models.ManyToManyField(Producer, verbose_name=u'Режиссер', blank=True)
    firstdate = models.DateField(verbose_name=u'Дата премьеры', null=True , blank=True)
    filmlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на фильм', null=True , blank=True)
    trailerlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на трейлер', null=True , blank=True)
    poster = models.CharField(max_length=5000, verbose_name=u'Ссылка на Постер', null=True , blank=True)
    moviebg = models.CharField(max_length=5000, verbose_name=u'Ссылка на Бэкграунд', null=True , blank=True)
    
    def get_absolute_url(self):
        return reverse('base:film', kwargs={'film_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Serial(Page):
    class Meta:
        verbose_name = u"Сериал"
        verbose_name_plural = u"Сериалы"
        
    name = models.CharField(max_length=200, verbose_name=u'Название сериала')
    truename = models.CharField(max_length=200, verbose_name=u'Название сериала в оригинале', null=True , blank=True)
    genre = models.ManyToManyField(Genre, verbose_name=u'Жанр', blank=True)
    category = models.ManyToManyField(Category, verbose_name=u'Подборки', blank=True)
    actor = models.ManyToManyField(Actor, verbose_name=u'Актеры', blank=True)
    year = models.DateField(verbose_name=u"Год выпуска" , null=True , blank=True )
    QUALITY = (
        ('Не указан', 'Не указан'),
        ('CamRIP', 'CamRIP'),
        ('HD720', 'HD720'),

    )
    quality = models.CharField( max_length=20 ,verbose_name=u"Качество", choices=QUALITY, blank=True )
    country = models.CharField(max_length=50, verbose_name=u'Страна', null=True , blank=True)
    TRANSLATE = (
        ('Не указан', 'Не указан'),
        ('Дублированный', 'Дублированный'),
        ('Одноголосый', 'Одноголосый'),
    )
    translate = models.CharField(max_length=50, verbose_name=u'Перевод',choices=TRANSLATE, blank=True)
    duration = models.TimeField(verbose_name=u'Продолжительность', null=True , blank=True)
    producer = models.ManyToManyField(Producer, verbose_name=u'Режиссер', blank=True)
    firstdate = models.DateField(verbose_name=u'Дата премьеры', null=True , blank=True)
    filmlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на фильм', null=True , blank=True)
    trailerlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на трейлер', null=True , blank=True)
    
    def get_absolute_url(self):
        return reverse('base:serial', kwargs={'serial_alias': self.alias})

    def __unicode__(self):
        return self.name
        
class Serial_item(Page):
    class Meta:
        verbose_name = u"Серия"
        verbose_name_plural = u"Серии"
        
    name = models.ForeignKey(Serial, verbose_name=u'Название сериала')
    serie_name = models.CharField(max_length=5000, verbose_name=u'Название Серии', null=True , blank=True)
    season = models.IntegerField(default=1, verbose_name=u'Сезон' )
    serie = models.IntegerField(default=1, verbose_name=u'Серия' )
    filmlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на серию', null=True , blank=True)
    trailerlink = models.CharField(max_length=5000, verbose_name=u'Ссылка на трейлер', null=True , blank=True)
        
    def get_absolute_url(self):
        return reverse('base:serial_item', kwargs={'serial_item_alias': self.alias})

    def __unicode__(self):
        return self.name.name

            
class News(Page):
    class Meta:
        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"

    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    date = models.DateField(default=timezone.now, verbose_name=u"Время публикации" )
    img = models.CharField(max_length=3000 ,verbose_name=u"Превью новости" , null=True , blank=True )
    category = models.CharField(max_length=300 ,verbose_name=u"Категория" , null=True , blank=True )

    def get_absolute_url(self):
        return reverse('base:news', kwargs={'news_alias': self.alias})

    def __unicode__(self):
        return self.name