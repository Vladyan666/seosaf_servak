# -*- coding: utf-8 -*-
from django.contrib import admin
from base.models import TextPage, Actor, Genre, Category, Film, News, Producer, Serial, Serial_item

# Register your models here.
#общие поля
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
#
admin.site.register(TextPage,TextPageAdmin)

#Актеры
class ActorAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'heigth','birth','birthplace']}),] + page_fields
#
admin.site.register(Actor,ActorAdmin)

#Режиссеры
class ProducerAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'birth','birthplace']}),] + page_fields
#
admin.site.register(Producer,ProducerAdmin)

#Жанры
class GenreAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name']}),] + page_fields
#
admin.site.register(Genre,GenreAdmin)

#Категории
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name']}),] + page_fields
#
admin.site.register(Category,CategoryAdmin)

#Фильмы
class FilmAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','quality','translate','firstdate','duration','country')
    fieldsets = [(u'Основные',  {'fields':['name', 'truename', 'genre','category','producer','actor','quality','translate','country', 'duration','firstdate','filmlink','trailerlink','poster','moviebg']}),] + page_fields
    search_fields = ['name']
    list_filter = ['genre','category','producer','country','quality','translate']
#
admin.site.register(Film,FilmAdmin)

#Серии
class Serial_itemAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','serie_name','season','serie')
    fieldsets = [(u'Основные',  {'fields':['name', 'serie_name', 'season','serie','filmlink','trailerlink']}),] + page_fields
    search_fields = ['name','serie_name']
    list_filter = ['name','season','serie']
#
admin.site.register(Serial_item,Serial_itemAdmin)

#Сериалы
class SerialAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    list_display = ('name','quality','translate','firstdate','duration','country')
    fieldsets = [(u'Основные',  {'fields':['name', 'genre','category','producer','actor','quality','translate','country', 'duration','firstdate','filmlink','trailerlink']}),] + page_fields
    search_fields = ['name']
    list_filter = ['genre','category','producer','country','quality','translate']
#
admin.site.register(Serial,SerialAdmin)

#Новости
class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Новость',  {'fields':['name','date','img','category',]}),] + page_fields
#
admin.site.register(News,NewsAdmin)