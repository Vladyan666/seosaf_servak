from django.conf.urls import url
import django
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^ajax/$', views.ajaxcontroller, name='ajax'),
    url(r'^postupdater/$', views.postupdater, name='postupdater'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^films$', views.films, name='films'),
    url(r'^e7e00435857b50f5\.txt$', views.check, name='check'),
    url(r'^categories$', views.categories, name='categories'),
    url(r'^category/(?P<alias>[0-9a-z\-]+)/$', views.category, name='category'),
    url(r'^films/(?P<alias>[0-9a-z\-]+)/$', views.film, name="film"),
]